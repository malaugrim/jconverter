package pl.com.malaugrim.jconverter.objectconverters;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import pl.com.malaugrim.jconverter.core.accessor.Accessor;
import pl.com.malaugrim.jconverter.core.accessor.FieldAccessor;
import pl.com.malaugrim.jconverter.core.accessor.PropertyAccessor;
import pl.com.malaugrim.jconverter.core.copymode.*;
import pl.com.malaugrim.jconverter.core.copymode.copymodebuilder.ConversionModeVariant;
import pl.com.malaugrim.jconverter.core.copymode.copymodebuilder.CopyModeBuilder;
import pl.com.malaugrim.jconverter.core.copymode.copymodebuilder.From;

/**
 * A factory used to create instances of {@link StandardObjectConverter} for most use case scenarios. Each created standard object converter 
 * uses {@link ConversionModeWithPassingDstValueOnly} as conversion mode. If different conversion mode is needed, consider using constructor
 * {@link StandardObjectConverter#StandardObjectConverter(CopyMode, CopyMode, ConversionMode)}.
 *
 * @author Lukasz Tutka
 */
public class StandardObjectConverterFactory {


	/**
	 * Creates {@link StandardObjectConverter}, which sets fields in the destination object with values copied from corresponding fields in
	 * source object. Fields are accessed via reflection, their access modifiers are unrestricted.
	 */
	public <S, D> StandardObjectConverter<S, D> createFieldToFieldConverter() {
		return createConverter(FieldAccessor.class, FieldAccessor.class);
	}

	/**
	 * Creates {@link StandardObjectConverter}, which sets properties in the destination object with values copied from corresponding fields
	 * in source object. Fields are accessed via reflection, their access modifiers are unrestricted. Properties are set via
	 * public setter.
	 */
	public <S, D> StandardObjectConverter<S, D> createFieldToPropertyConverter() {
		return createConverter(FieldAccessor.class, PropertyAccessor.class);
	}

	/**
	 * Creates {@link StandardObjectConverter}, which sets fields in the destination object with values copied from corresponding properties
	 * in source object. Fields are accessed via reflection, their access modifiers are unrestricted. Properties in source object are
	 * retrieved via public getters.
	 */
	public <S, D> StandardObjectConverter<S, D> createPropertyToFieldConverter() {
		return createConverter(PropertyAccessor.class, FieldAccessor.class);
	}

	/**
	 * Creates {@link StandardObjectConverter}, which sets properties in the destination object with values copied from corresponding
	 * properties in source object. Properties in source object are retrieved via public getters, while properties in destination object are
	 * set with public setters.
	 */
	public <S, D> StandardObjectConverter<S, D> createPropertyToPropertyConverter() {
		return createConverter(PropertyAccessor.class, PropertyAccessor.class);
	}

	private <S, D> StandardObjectConverter<S, D> createConverter(Class<? extends Accessor> srcAccessorType, Class<? extends Accessor> dstAccessorType) {
		return new StandardObjectConverter<>(
				createCopyMode(ShallowCopyMode.class, srcAccessorType, dstAccessorType),
				createCopyMode(DeepCopyMode.class, srcAccessorType, dstAccessorType),
				createCopyMode(ConversionModeWithPassingDstValueOnly.class, srcAccessorType, dstAccessorType));
	}
	
	@SuppressWarnings("unchecked")
	private <C extends CopyMode> C createCopyMode(Class<C> copyModeType, Class<? extends Accessor> srcAccessor,
												  Class<? extends Accessor> dstAccessor) {
		CopyModeBuilder builder = new CopyModeBuilder();
		return selectCopyMode(copyModeType, builder).from(srcAccessor).to(dstAccessor).build();
	}

	private <C extends CopyMode> From selectCopyMode(Class<C> copyModeType, CopyModeBuilder builder) {
		if (copyModeType.equals(ShallowCopyMode.class)) {
			return builder.shallowCopy();
		} else if (copyModeType.equals(DeepCopyMode.class)) {
			return builder.deepCopy();
		} else {
			return builder.convert(ConversionModeVariant.TAKE_DST_TYPE_ONLY, null);
		}
	}
}
