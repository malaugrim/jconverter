package pl.com.malaugrim.jconverter.objectconverters;

import java.lang.reflect.AccessibleObject;

import pl.com.malaugrim.jconverter.core.accessor.Accessor;
import pl.com.malaugrim.jconverter.core.annotations.DeepCopy;
import pl.com.malaugrim.jconverter.core.copymode.CopyMode;
import pl.com.malaugrim.jconverter.core.copymode.copymodebuilder.CopyModeBuilder;

/**
 * @author Lukasz Tutka
 */
class DeepCopyModeSetter extends CopyModeSetter {

	DeepCopyModeSetter(FlexibleObjectConverter objectConverter) {
		super(objectConverter);
	}

	@Override
	String getPointedSourceFieldChain(AccessibleObject accessibleObject) {
		return accessibleObject.getAnnotation(DeepCopy.class).fromFieldChain();
	}

	@Override
	String getPointedSourcePropertyChain(AccessibleObject accessibleObject) {
		return accessibleObject.getAnnotation(DeepCopy.class).fromPropertyChain();
	}

	@Override
	CopyMode createCopyMode(Class<? extends Accessor> srcAccessorType, Class<? extends Accessor> dstAccessorType) {
		return new CopyModeBuilder().deepCopy().from(srcAccessorType).to(dstAccessorType).build();
	}

}
