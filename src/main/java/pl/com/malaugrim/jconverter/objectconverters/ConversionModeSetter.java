package pl.com.malaugrim.jconverter.objectconverters;

import pl.com.malaugrim.jconverter.core.accessor.Accessor;
import pl.com.malaugrim.jconverter.core.annotations.Convert;
import pl.com.malaugrim.jconverter.core.copymode.CopyMode;
import pl.com.malaugrim.jconverter.core.copymode.copymodebuilder.CopyModeBuilder;
import pl.com.malaugrim.jconverter.core.copymode.copymodebuilder.ConversionModeVariant;

import java.lang.reflect.AccessibleObject;

/**
 * @author Lukasz Tutka
 */
class ConversionModeSetter extends CopyModeSetter {

	ConversionModeSetter(FlexibleObjectConverter objectConverter) {
		super(objectConverter);
	}

	@Override
	String getPointedSourceFieldChain(AccessibleObject accessibleObject) {
		return accessibleObject.getAnnotation(Convert.class).fromFieldChain();
	}

	@Override
	String getPointedSourcePropertyChain(AccessibleObject accessibleObject) {
		return accessibleObject.getAnnotation(Convert.class).fromPropertyChain();
	}

	@Override
	CopyMode createCopyMode(Class<? extends Accessor> srcAccessorType, Class<? extends Accessor> dstAccessorType) {
		return new CopyModeBuilder().convert(ConversionModeVariant.TAKE_DST_VALUE_ONLY, typeConverter)
				.from(srcAccessorType).to(dstAccessorType).build();
	}

}
