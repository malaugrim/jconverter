package pl.com.malaugrim.jconverter.objectconverters;

import java.lang.reflect.AccessibleObject;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Objects;

import pl.com.malaugrim.jconverter.core.TypeConverter;
import pl.com.malaugrim.jconverter.core.accessor.Accessor;
import pl.com.malaugrim.jconverter.core.accessor.FieldAccessor;
import pl.com.malaugrim.jconverter.core.accessor.PropertyAccessor;
import pl.com.malaugrim.jconverter.core.copymode.CopyMode;

/**
 * @author Lukasz Tutka
 */
abstract class CopyModeSetter {

	final FlexibleObjectConverter flexibleObjectConverter;
	TypeConverter<?, ?> typeConverter;


	CopyModeSetter(FlexibleObjectConverter objectConverter) {
		flexibleObjectConverter = objectConverter;
	}

	public void set(String dstAccessibleName, AccessibleObject dstAccessibleObject) {
		String srcFieldChain = getPointedSourceFieldChain(dstAccessibleObject);
		String srcPropertyChain = getPointedSourcePropertyChain(dstAccessibleObject);
		
		CopyModeInfo info = determineCopyModeInfo(srcFieldChain, srcPropertyChain, dstAccessibleName, dstAccessibleObject);
		flexibleObjectConverter.setCopyMode(info.getSrcAccessibleChain(), dstAccessibleName, info.getCopyMode());
	}

	public void setTypeConverter(TypeConverter<?, ?> typeConverter) {
		this.typeConverter = typeConverter;
	}

	public TypeConverter<?, ?> getTypeConverter() {
		return typeConverter;
	}

	CopyModeInfo determineCopyModeInfo(String srcFieldChain, String srcPropertyChain, String dstAccessibleName, AccessibleObject dstAccessibleObject) {
		String srcAccessibleChain = determineSourceAccessibleChain(srcFieldChain, srcPropertyChain, dstAccessibleName);
		Class<? extends Accessor> srcAccessorType = determineSrcAccessorType(srcFieldChain, srcPropertyChain, dstAccessibleObject);
		Class<? extends Accessor> dstAccessorType = determineDstAccessorType(dstAccessibleObject);
		return new CopyModeInfo(srcAccessibleChain, createCopyMode(srcAccessorType, dstAccessorType));
	}

	abstract String getPointedSourceFieldChain(AccessibleObject accessibleObject);

	abstract String getPointedSourcePropertyChain(AccessibleObject accessibleObject);

	abstract CopyMode createCopyMode(Class<? extends Accessor> srcAccessorType, Class<? extends Accessor> dstAccessorType);

	private String determineSourceAccessibleChain(String srcField, String srcProperty, String dstAccessibleName) {
		if(!srcField.isEmpty()) {
			return srcField;
		} else if(!srcProperty.isEmpty()) {
			return srcProperty;
		} else {
			return dstAccessibleName;
		}
	}

	private Class<? extends Accessor> determineSrcAccessorType(String srcField, String srcProperty, AccessibleObject accessibleObject) {
		if(!srcField.isEmpty()) {
			return FieldAccessor.class;
		} else if(!srcProperty.isEmpty()) {
			return PropertyAccessor.class;
		} else if(accessibleObject instanceof Field) {
			return FieldAccessor.class;
		} else if(accessibleObject instanceof Method) {
			return PropertyAccessor.class;
		}

		throw new IllegalStateException("Unable to determine copy mode for accessible object: " + accessibleObject.getClass().getName());
	}

	private Class<? extends Accessor> determineDstAccessorType(AccessibleObject accessibleObject) {
		if(accessibleObject instanceof Field) {
			return FieldAccessor.class;
		} else if(accessibleObject instanceof Method) {
			return PropertyAccessor.class;
		}

		throw new IllegalStateException("Unable to determine copy mode for accessible object: " + accessibleObject.getClass().getName());
	}


	
	class CopyModeInfo {
		
		private final String srcAccessibleChain;
		private final CopyMode copyMode;

		public CopyModeInfo(String srcAccessibleChain, CopyMode copyMode) {
			this.srcAccessibleChain = srcAccessibleChain;
			this.copyMode = copyMode;
		}

		public CopyMode getCopyMode() {
			return copyMode;
		}

		public String getSrcAccessibleChain() {
			return srcAccessibleChain;
		}

		@Override
		public boolean equals(Object o) {
			if (this == o) return true;
			if (!(o instanceof ConversionModeSetter.CopyModeInfo)) return false;
			ConversionModeSetter.CopyModeInfo that = (ConversionModeSetter.CopyModeInfo) o;
			return Objects.equals(srcAccessibleChain, that.srcAccessibleChain) &&
					Objects.equals(copyMode, that.copyMode);
		}

		@Override
		public int hashCode() {
			return Objects.hash(srcAccessibleChain, copyMode);
		}
	}
}
