package pl.com.malaugrim.jconverter.objectconverters;

import java.lang.reflect.AccessibleObject;

import pl.com.malaugrim.jconverter.core.accessor.Accessor;
import pl.com.malaugrim.jconverter.core.annotations.ShallowCopy;
import pl.com.malaugrim.jconverter.core.copymode.CopyMode;
import pl.com.malaugrim.jconverter.core.copymode.copymodebuilder.CopyModeBuilder;

/**
 * @author Lukasz Tutka
 */
class ShallowCopyModeSetter extends CopyModeSetter {

	ShallowCopyModeSetter(FlexibleObjectConverter objectConverter) {
		super(objectConverter);
	}

	@Override
	String getPointedSourceFieldChain(AccessibleObject accessibleObject) {
		return accessibleObject.getAnnotation(ShallowCopy.class).fromFieldChain();
	}

	@Override
	String getPointedSourcePropertyChain(AccessibleObject accessibleObject) {
		return accessibleObject.getAnnotation(ShallowCopy.class).fromPropertyChain();
	}

	@Override
	CopyMode createCopyMode(Class<? extends Accessor> srcAccessorType, Class<? extends Accessor> dstAccessorType) {
		return new CopyModeBuilder().shallowCopy().from(srcAccessorType).to(dstAccessorType).build();
	}
}
