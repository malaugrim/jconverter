package pl.com.malaugrim.jconverter.objectconverters;

import java.util.*;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import pl.com.malaugrim.jconverter.core.ObjectConverter;
import pl.com.malaugrim.jconverter.core.ObjectInstantiator;
import pl.com.malaugrim.jconverter.core.accessor.AccessibleObjectWrapper;
import pl.com.malaugrim.jconverter.core.accessor.Accessor;
import pl.com.malaugrim.jconverter.core.accessor.FieldAccessor;
import pl.com.malaugrim.jconverter.core.accessor.PropertyAccessor;
import pl.com.malaugrim.jconverter.core.copymode.CopyMode;
import pl.com.malaugrim.jconverter.core.copymode.copymodebuilder.CopyModeBuilder;

/**
 * <p>{@code FlexibleObjectConverter} is the most advanced converter. It allows to define a {@link CopyMode copy mode} for each accessible
 * in the destination object. Converter performs conversion in two simple steps:</p>
 * <ol>
 *     <li>converts accessibles, for which copy mode has been specified with {@link FlexibleObjectConverter#setCopyMode(String, CopyMode)}
 *     or {@link FlexibleObjectConverter#setCopyMode(String, String, CopyMode)}. Copy mode is used to retrieve value from source object 
 *     and set it in the destination object.</li>
 *
 *     <li>converts all other remaining accessibles with {@link FlexibleObjectConverter#setDefaultCopyMode(CopyMode) default
 *     copy mode}. If default copy mode is not specified, this step does nothing.</li>
 * </ol>
 * 
 * <p>A {@link CopyModeBuilder} may be used for creating copy modes, however you are free to create them as you see fit.</p>
 * 
 * <p>When setting copy mode you may provide either name of the accessibles to copy or something that is called accessible chain. Both 
 * terms are explained below:</p>
 * 
 * <p><b>Accessible</b>: either field or property</p>
 * <p><b>Accessible chain</b> is a dot separated list of accessibles. Basically accessible chain represents a parent-child relationship 
 * between accessibles - an accessible before the dot is said to contain accessbile after the dot. How exactly the chain is used to get and 
 * set accessibles - is dependent on {@link Accessor accessor classes} used by provided {@link CopyMode copy modes}. For instance a 
 * {@link FieldAccessor} will use reflection to get/set correspondent fields, while {@link PropertyAccessor} will use public 
 * getters/setters to get or set a property.
 *
 * <p>Example: </p>
 * Given the following chain: someValue.otherValue:
 * <ul>
 *     <li>A source accessor of type {@link FieldAccessor} will use reflective calls to get field 'otherValue', which is contained in 
 *     object represented by field 'someValue', which in turn is contained in the source object</li>
 *     <li>A source accessor of type {@link PropertyAccessor} will use following method calls to get the value: 
 *     srcObject.getSomeValue().getOtherValue()</li>
 * </ul>
 * </p>
 *
 * @see CopyMode
 * @see CopyModeBuilder
 * @see Accessor
 * @see FieldAccessor
 * @see PropertyAccessor
 * 
 * @author Lukasz Tutka
 */
public class FlexibleObjectConverter<S, D> implements ObjectConverter<S, D> {

	private final Map<AccessibleSrcDstPair, CopyMode> copyModesForAccessibles = new HashMap<>();
	private final Set<String> toIgnore = new HashSet<>();
	private final ObjectInstantiator instantiator = new ObjectInstantiator();
	private final List<String> accessiblesCopied = new ArrayList<>();
	private CopyMode defaultCopyMode;


	/**
	 * Same as {@link FlexibleObjectConverter#setCopyMode(String, String, CopyMode) setCopyMode(accessibleChain, accessibleChain, copyMode)}.
	 */
	public void setCopyMode(String accessibleChain, CopyMode copyMode) {
		setCopyMode(accessibleChain, accessibleChain, copyMode);
	}

	/**
	 * Provided copy mode will be used to copy value extracted from {@code srcAccessibleChain} in source object and set it in accessible 
	 * extracted from {@code dstAccessibleChain} in the destination object.
	 */
	public void setCopyMode(String srcAccessibleChain, String dstAccessibleChain, CopyMode copyMode) {
		copyModesForAccessibles.put(new AccessibleSrcDstPair(srcAccessibleChain, dstAccessibleChain), copyMode);
	}

	@Override
	public D convert(S src, Class<? extends D> dstClass) {
		D dst = instantiator.createInstance(dstClass);
		return convert(src, dst);
	}

	@Override
	public D convert(S src, D dst) {
		convertUsingProvidedCopyModes(src, dst);
		convertUsingDefaultCopyModeIfPresent(src, dst);
		accessiblesCopied.clear();
		return dst;
	}

	@Override
	public <T> void addInstanceSupplier(Class<T> type, Supplier<T> supplier) {
		instantiator.addInstanceSupplier(type, supplier);
	}

	@Override
	public void ignore(String... toIgnore) {
		this.toIgnore.addAll(Arrays.asList(toIgnore));
	}

	/**
	 * Sets default copy mode, which will be used to convert all accessibles without specified copy mode.
	 */
	public void setDefaultCopyMode(CopyMode defaultCopyMode) {
		this.defaultCopyMode = defaultCopyMode;
	}

	private void convertUsingProvidedCopyModes(S src, D dst) {
		copyModesForAccessibles.entrySet().stream().filter(this::isNotIgnored).forEach(entry -> {
			AccessibleSrcDstPair accessibles = entry.getKey();
			CopyMode copyMode = entry.getValue();
			copyMode.setup(src, dst);
			copyMode.setObjectInstantiator(instantiator);
			copyMode.copy(accessibles.getSrc(), accessibles.getDst());
			accessiblesCopied.add(accessibles.getDst());
		});
	}

	private boolean isNotIgnored(Map.Entry<AccessibleSrcDstPair, CopyMode> entry) {
		String dstAccessibleChain = entry.getKey().getDst();
		String dstAccessible = dstAccessibleChain.split("\\.")[0];
		return !toIgnore.contains(dstAccessible);
	}

	private void convertUsingDefaultCopyModeIfPresent(S src, D dst) {
		if(defaultCopyMode != null) {
			defaultCopyMode.setup(src, dst);
			defaultCopyMode.setObjectInstantiator(instantiator);
			getCommonNotIgnoredNotCopiedBeforeAccessibleNames().forEach(accessible -> defaultCopyMode.copy(accessible, accessible));
		}
	}

	private Set<String> getCommonNotIgnoredNotCopiedBeforeAccessibleNames() {
		List<? extends AccessibleObjectWrapper> srcAccessibles = defaultCopyMode.getSrcAccessibles();
		List<? extends AccessibleObjectWrapper> dstAccessibles = defaultCopyMode.getDstAccessibles();
		return dstAccessibles.stream()
				.filter(accessible -> contains(srcAccessibles, accessible))
				.filter(this::wasNotCopiedBefore)
				.map(AccessibleObjectWrapper::getAccessibleObjectName)
				.filter(a -> !toIgnore.contains(a))
				.collect(Collectors.toSet());
	}

	private boolean wasNotCopiedBefore(AccessibleObjectWrapper accessible) {
		return !accessiblesCopied.contains(accessible.getAccessibleName());
	}

	private boolean contains(List<? extends AccessibleObjectWrapper> accessibleList, AccessibleObjectWrapper accessible) {
		return accessibleList.stream().filter(acc -> acc.getAccessibleName().equals(accessible.getAccessibleName()))
				.findFirst().isPresent();
	}
}
