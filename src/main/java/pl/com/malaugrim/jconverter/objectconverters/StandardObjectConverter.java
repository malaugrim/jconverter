package pl.com.malaugrim.jconverter.objectconverters;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import pl.com.malaugrim.jconverter.core.ObjectConverter;
import pl.com.malaugrim.jconverter.core.ObjectInstantiator;
import pl.com.malaugrim.jconverter.core.TypeConverter;
import pl.com.malaugrim.jconverter.core.accessor.AccessibleObjectWrapper;
import pl.com.malaugrim.jconverter.core.copymode.ConversionMode;
import pl.com.malaugrim.jconverter.core.copymode.CopyMode;

import java.util.*;
import java.util.function.Consumer;
import java.util.function.Supplier;
import java.util.stream.Collectors;

/**
 * <p>This object converter is sufficient for most use cases. It uses three predefined {@link CopyMode copy modes} to copy object internal
 * components: first to shallow copy, second to deep copy and third to convert
 * (see {@link StandardObjectConverter#StandardObjectConverter(CopyMode, CopyMode, ConversionMode)}). By default, all accessibles are
 * shallow copied in a way defined by first {@link CopyMode}. If one or more accessibles need to be deep copied, use
 * {@link StandardObjectConverter#deepCopy(Collection)} or {@link StandardObjectConverter#deepCopy(String...)}. The deep copy will be
 * performed by the second {@link CopyMode}.</p>
 *
 * <p>Third copy mode ({@link ConversionMode} to be more specific) is used to perform conversions. A conversion is needed when accessible
 * in source object is of different type than corresponding accessbile in the destination object. In order for {@link ConversionMode} to
 * work properly, a {@link TypeConverter} must be provided. This can be done in one of two ways:</p>
 * <ol>
 *     <li>by adding a type converter to convert concrete accessible:
 *     {@link StandardObjectConverter#addConverter(String, TypeConverter)}. This way only the indicated accessible will be converted with
 *     the use of the given type converter. Such type converter is called name based converter.</li>
 *
 *     <li>by adding a type converter to convert all accessibles of matching types:
 *     {@link StandardObjectConverter#addConverter(TypeConverter)}. Given type converter will be used to convert all accessibles, for which
 *     the following returns {@code true}: {@link TypeConverter#canConvert(Class, Class)}, where first argument is type of the accessbile
 *     in source object, while second argument is type of the corresponding accessbile in destinatin object. Such type converter is called
 *     type based converter.</li>
 * </ol>
 *
 * <p>{@code StandardObjectConverter} performs conversion in the following steps:
 * <ol>
 *     <li>converts accessbiles with the use of name based and type based converters. Note that name based converter takes precedence
 *     before type based converter. This means that type based converter will not be used to convert accessbile, which already has been
 *     converted by name base converter.</li>
 *
 *     <li>performs deep copy of all indicated accessbiles. However note that deep copy will not be applied to accessbiles converted in the
 *     previous step.</li>
 *
 *     <li>during the last step all remaining accessibles are shallow copied. Note that shallow copy will not be applied to accessibles
 *     converted or deep copied in previous steps.</li>
 * </ol>
 *
 * <p>{@code StandardObjectConverter} tries to copy all accessibles by default. In order to indicate an accessible, which should be
 * omitted, use {@link StandardObjectConverter#ignore(String...)} Such accessibles won't be shallow copied, deep copied nor
 * converted.</p>
 *
 * <p>During object conversion process there are situations, when there's a need to create a new instance of the destination accessible
 * (e.g. when performing deep copy). In such situations public default constructor is called on the accessbiles class. However if class
 * does not have such constructor, a instance supplier must be provided with
 * {@link StandardObjectConverter#addInstanceSupplier(Class, Supplier)}.</p>
 *
 * <p>It is advised to use {@link StandardObjectConverterFactory} for creating most commonly used standard object converters.</p>
 *
 * @author Lukasz Tutka
 */
public class StandardObjectConverter<S, D> implements ObjectConverter<S,D> {

	private final ObjectInstantiator instantiator = new ObjectInstantiator();
	private final Set<String> toIgnore = new HashSet<>();
	private final Set<String> toDeepCopy = new HashSet<>();
	private final Set<TypeConverter<?, ?>> typeBasedConverters = new HashSet<>();
	private final Map<String, TypeConverter<?, ?>> nameBasedConverters = new HashMap<>();
	
	private final CopyMode shallowCopyMode;
	private final CopyMode deepCopyMode;
	private final ConversionMode conversionMode;

	final Set<String> convertedAccessibles = new HashSet<>();
	boolean conversionModeSetUp;


	/**
	 * Creates StandardObjectConverter with specified copy modes for shallow copying, deep copying and conversions.
	 */
	public StandardObjectConverter(CopyMode shallowCopyMode, CopyMode deepCopyMode, ConversionMode conversionMode) {
		this.shallowCopyMode = shallowCopyMode;
		this.deepCopyMode = deepCopyMode;
		this.conversionMode = conversionMode;
	}

	/**
	 * @throws IllegalStateException - when object of dstClass doesn't have public default constructor and no instance supplier was provided.
	 *
	 * @see StandardObjectConverter#addInstanceSupplier(Class, Supplier)
	 */
	@Override
	public D convert(S src, Class<? extends D> dstClass) {
		D dst = instantiator.createInstance(dstClass);
		return convert(src, dst);
	}

	@Override
	public D convert(S src, D dst) {
		performConversion(src, dst);
		clearInternalState();
		return dst;
	}

	@Override
	public <T> void addInstanceSupplier(Class<T> type, Supplier<T> supplier) {
		instantiator.addInstanceSupplier(type, supplier);
	}

	/**
	 * Adds type based converter used to convert all accessibles, for which the following returns {@code true}:
	 * {@link TypeConverter#canConvert(Class, Class)}, where first argument is type of the accessbile in source object, while second
	 * argument is type of the corresponding accessbile in destinatin object
	 */
	public void addConverter(TypeConverter<?, ?> typeConverter) {
		typeBasedConverters.add(typeConverter);
	}

	/**
	 * Adds name based converter used to convert {@code accessibleName}.
	 */
	@SuppressWarnings("SameParameterValue")
	public void addConverter(String accessibleName, TypeConverter<?, ?> typeConverter) {
		nameBasedConverters.put(accessibleName, typeConverter);
	}

	@Override
	public void ignore(String... toIgnore) {
		this.toIgnore.addAll(Sets.newHashSet(toIgnore));
	}

	/**
	 * Indicates that the given {@code accessibles} should be deep copied.
	 */
	public void deepCopy(String... accessibles) {
		deepCopy(Lists.newArrayList(accessibles));
	}

	/**
	 * Indicates that the given {@code accessibles} should be deep copied.
	 */
	public void deepCopy(Collection<String> accessibles) {
		toDeepCopy.addAll(accessibles);
	}

	CopyMode getShallowCopyMode() {
		return shallowCopyMode;
	}

	CopyMode getDeepCopyMode() {
		return deepCopyMode;
	}

	CopyMode getConversionMode() {
		return conversionMode;
	}

	private void performConversion(S src, D dst) {
		setUpShallowAndDeepCopyModes(src, dst);
		SrcDstAccessiblesPair srcDstAccessiblesPair = new SrcDstAccessiblesPair();
		Set<AccessibleObjectWrapper> commonNotIgnoredAccessibles = getCommonNotIgnoredAccessibles(srcDstAccessiblesPair);

		performCopyUsingConverters(commonNotIgnoredAccessibles, new SrcDstPair(src, dst), srcDstAccessiblesPair);
		performDeepCopy();
		performShallowCopy(commonNotIgnoredAccessibles);
	}

	private void setUpShallowAndDeepCopyModes(S src, D dst) {
		shallowCopyMode.setup(src, dst);
		deepCopyMode.setup(src, dst);
		deepCopyMode.setObjectInstantiator(instantiator);
	}

	private Set<AccessibleObjectWrapper> getCommonNotIgnoredAccessibles(SrcDstAccessiblesPair srcDstAccessiblesPair) {
		return srcDstAccessiblesPair.dstAccessibles.stream()
				.filter(accessible -> contains(srcDstAccessiblesPair.srcAccessibles, accessible))
				.filter(this::isNotIgnored)
				.collect(Collectors.toSet());
	}

	private boolean contains(List<AccessibleObjectWrapper> accessibles, AccessibleObjectWrapper accessible) {
		return accessibles.stream().filter(acc -> acc.getAccessibleName().equals(accessible.getAccessibleName())).findFirst().isPresent();
	}

	private boolean isNotIgnored(AccessibleObjectWrapper accessible) {
		String accessibleName = accessible.getAccessibleName();
		return !toIgnore.contains(accessibleName);
	}

	private void performCopyUsingConverters(Set<AccessibleObjectWrapper> accessibles, SrcDstPair srcDstPair, 
											SrcDstAccessiblesPair srcDstAccessiblesPair) {
		accessibles.forEach(accessible -> {
			Optional<TypeConverter<?, ?>> optConverter = findConverter(srcDstAccessiblesPair, accessible);
			optConverter.ifPresent(copyUsingConversionMode(accessible, srcDstPair.src, srcDstPair.dst));
		});
	}

	private Optional<TypeConverter<?, ?>> findConverter(SrcDstAccessiblesPair srcDstAccessiblesPair, AccessibleObjectWrapper accessible) {
		String accessibleName = accessible.getAccessibleName();
		if(nameBasedConverters.containsKey(accessibleName)) {
			return Optional.of(nameBasedConverters.get(accessibleName));
		}

		return findTypeBasedConverter(srcDstAccessiblesPair, accessibleName);
	}

	private Optional<TypeConverter<?, ?>> findTypeBasedConverter(SrcDstAccessiblesPair srcDstAccessiblesPair, String accessibleName) {
		AccessibleObjectWrapper sacAccessible = getWrapperByName(srcDstAccessiblesPair.srcAccessibles, accessibleName);
		Class<?> srcType = sacAccessible.getType();
		AccessibleObjectWrapper dstAccessible = getWrapperByName(srcDstAccessiblesPair.dstAccessibles, accessibleName);
		Class<?> dstType = dstAccessible.getType();

		return typeBasedConverters.stream().filter(converter -> converter.canConvert(srcType, dstType)).findAny();
	}

	@SuppressWarnings("OptionalGetWithoutIsPresent")
	private AccessibleObjectWrapper getWrapperByName(List<AccessibleObjectWrapper> accessibles, String name) {
		return accessibles.stream().filter(accessible -> accessible.getAccessibleName().equals(name)).findAny().get();
	}

	private Consumer<TypeConverter<?, ?>> copyUsingConversionMode(AccessibleObjectWrapper toCopy, S srcObject, D dstObject) {
		return converter -> {
			setUpConversionMode(srcObject, dstObject, converter);
			String accessibleName = toCopy.getAccessibleName();
			conversionMode.copy(accessibleName, accessibleName);
			convertedAccessibles.add(accessibleName);
		};
	}

	private void setUpConversionMode(S srcObject, D dstObject, TypeConverter<?, ?> typeConverter) {
		if(!conversionModeSetUp) {
			conversionMode.setup(srcObject, dstObject);
			conversionModeSetUp = true;
		}

		conversionMode.setTypeConverter(typeConverter);
	}

	private void  performDeepCopy() {
		toDeepCopy.stream().filter(accessible -> !toIgnore.contains(accessible) && !convertedAccessibles.contains(accessible))
				.forEach(accessible -> deepCopyMode.copy(accessible, accessible));
	}

	private void performShallowCopy(Set<AccessibleObjectWrapper> accessibles) {
		accessibles.stream().map(AccessibleObjectWrapper::getAccessibleName)
				.filter(accessible -> !toDeepCopy.contains(accessible) && !convertedAccessibles.contains(accessible))
				.forEach(accessible -> shallowCopyMode.copy(accessible, accessible));
	}

	private void clearInternalState() {
		convertedAccessibles.clear();
		conversionModeSetUp = false;
	}
	
	
	private class SrcDstPair {
		public final S src;
		public final D dst;

		private SrcDstPair(S src, D dst) {
			this.src = src;
			this.dst = dst;
		}
	}
	
	private class SrcDstAccessiblesPair {
		public final List<AccessibleObjectWrapper> srcAccessibles;
		public final List<AccessibleObjectWrapper> dstAccessibles;

		private SrcDstAccessiblesPair() {
			CopyMode copyMode = shallowCopyMode;
			this.srcAccessibles = copyMode.getSrcAccessibles();
			this.dstAccessibles = copyMode.getDstAccessibles();
		}
	}
}
