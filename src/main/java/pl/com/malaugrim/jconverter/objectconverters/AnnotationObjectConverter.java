package pl.com.malaugrim.jconverter.objectconverters;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

import pl.com.malaugrim.jconverter.core.ObjectConverter;
import pl.com.malaugrim.jconverter.core.ObjectInstantiator;
import pl.com.malaugrim.jconverter.core.TypeConverter;
import pl.com.malaugrim.jconverter.core.accessor.AccessibleObjectWrapper;
import pl.com.malaugrim.jconverter.core.annotations.ProcessingResult;
import pl.com.malaugrim.jconverter.core.copymode.CopyMode;
import pl.com.malaugrim.jconverter.core.copymode.DeepCopyMode;
import pl.com.malaugrim.jconverter.core.copymode.ShallowCopyMode;
import pl.com.malaugrim.jconverter.core.copymode.copymodebuilder.CopyModeBuilder;
import pl.com.malaugrim.jconverter.core.annotations.AnnotationProcessor;
import pl.com.malaugrim.jconverter.core.copymode.ConversionMode;


/**
 * @author Lukasz Tutka
 */
public class AnnotationObjectConverter<S, D> implements ObjectConverter<S, D> {
	
	private static final CopyMode DEFAULT_COPY_MODE = getDefaultCopyMode();
	
	private final AnnotationProcessor annotationProcessor = new AnnotationProcessor();
	private final Map<Class<? extends CopyMode>, CopyModeSetter>  copyModeSetters = new HashMap<>();
	private ObjectInstantiator instantiator = new ObjectInstantiator();
	private FlexibleObjectConverter<S, D> flexibleObjectConverter = new FlexibleObjectConverter<>();


	
	private static CopyMode getDefaultCopyMode() {
		return new CopyModeBuilder().shallowCopy().fromField().toField().build();
	}

	public AnnotationObjectConverter() {
		flexibleObjectConverter.setDefaultCopyMode(DEFAULT_COPY_MODE);
		setCopyModeSetters();
	}

	@Override
	public D convert(S srcObj, Class<? extends D> dstClass) {
		return convert(srcObj, instantiator.createInstance(dstClass));
	}

	@Override
	public D convert(S srcObj, D dstObj) {
		ProcessingResult processingResult = annotationProcessor.process(dstObj);
		setUpFlexibleObjectConverter(processingResult);
		return flexibleObjectConverter.convert(srcObj, dstObj);
	}

	@Override
	public void ignore(String... toIgnore) {
		flexibleObjectConverter.ignore(toIgnore);
	}

	@Override
	public <T> void addInstanceSupplier(Class<T> type, Supplier<T> supplier) {
		instantiator.addInstanceSupplier(type, supplier);
		flexibleObjectConverter.addInstanceSupplier(type, supplier);
	}

	void setInstantiator(ObjectInstantiator instantiator) {
		this.instantiator = instantiator;
	}

	@SuppressWarnings("unchecked")
	void setFlexibleObjectConverter(FlexibleObjectConverter objectConverter) {
		flexibleObjectConverter = objectConverter;
		flexibleObjectConverter.setDefaultCopyMode(DEFAULT_COPY_MODE);
		setCopyModeSetters();
	}

	private void setUpFlexibleObjectConverter(ProcessingResult processingResult) {
		processingResult.getIgnored().stream().map(AccessibleObjectWrapper::getAccessibleName).forEach(this::ignore);
		processingResult.getToDeepCopy().forEach(this::setDeepCopyMode);
		processingResult.getToConvert().forEach(this::setConversionMode);
		processingResult.getToShallowCopy().forEach(this::setShallowCopyMode);
	}

	private void setCopyModeSetters() {
		copyModeSetters.put(ShallowCopyMode.class, new ShallowCopyModeSetter(flexibleObjectConverter));
		copyModeSetters.put(DeepCopyMode.class, new DeepCopyModeSetter(flexibleObjectConverter));
		copyModeSetters.put(ConversionMode.class, new ConversionModeSetter(flexibleObjectConverter));
	}

	private void setDeepCopyMode(AccessibleObjectWrapper<?> accessible) {
		CopyModeSetter copyModeSetter = copyModeSetters.get(DeepCopyMode.class);
		copyModeSetter.set(accessible.getAccessibleName(), accessible.getAccessibleObject());
	}

	private void setConversionMode(AccessibleObjectWrapper<?> accessible, Class<? extends TypeConverter> converterClass) {
		TypeConverter<?, ?> typeConverter = instantiator.createInstance(converterClass);
		CopyModeSetter conversionModeSetter = copyModeSetters.get(ConversionMode.class);
		conversionModeSetter.setTypeConverter(typeConverter);
		conversionModeSetter.set(accessible.getAccessibleName(), accessible.getAccessibleObject());
	}

	private void setShallowCopyMode(AccessibleObjectWrapper<?> accessible) {
		CopyModeSetter copyModeSetter = copyModeSetters.get(ShallowCopyMode.class);
		copyModeSetter.set(accessible.getAccessibleName(), accessible.getAccessibleObject());
	}
}
