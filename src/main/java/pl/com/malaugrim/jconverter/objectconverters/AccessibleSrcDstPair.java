package pl.com.malaugrim.jconverter.objectconverters;

import java.util.Objects;

import com.google.common.base.MoreObjects;

/**
 * @author Lukasz Tutka
 */
class AccessibleSrcDstPair {

	private final String src;
	private final String dst;

	AccessibleSrcDstPair(String src, String dst) {
		this.src = src;
		this.dst = dst;
	}

	public String getSrc() {
		return src;
	}

	public String getDst() {
		return dst;
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof AccessibleSrcDstPair)) return false;
		AccessibleSrcDstPair that = (AccessibleSrcDstPair) o;
		return Objects.equals(src, that.src) &&
				Objects.equals(dst, that.dst);
	}

	@Override
	public int hashCode() {
		return Objects.hash(src, dst);
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this)
				.add("src", src)
				.add("dst", dst)
				.toString();
	}
}
