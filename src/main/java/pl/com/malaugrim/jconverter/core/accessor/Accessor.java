package pl.com.malaugrim.jconverter.core.accessor;

import java.util.List;

/**
 * <p>An implementation of this class is able to access (get or set value) any accessible of any given object. It is required that each 
 * instance of {@code Accessor} is tied to one object, on which it operates.</p>
 * 
 * <p><b>Accessible</b>: field, property or other 'accessible' object</p>
 * 
 * @see AccessibleObjectWrapper
 * 
 * @author Lukasz Tutka
 */
public interface Accessor {

	/**
	 * Retrieves all accessibles of the given object.
	 * @see AccessibleObjectWrapper
	 */
	List<? extends AccessibleObjectWrapper> getAllAccessibleObjects();

	/**
	 * Returns associated object, on which this accessor is operating.
	 */
	Object getObject();

	/**
	 * Gets value of the desired accessible.
	 * @param accessibleName name of the accessible
	 */
	Object getValue(String accessibleName);

	/**
	 * Sets value of the desired accessible.
	 * @param accessibleName name of the accessible
	 * @param value value to set
	 *              
	 * @throws IllegalArgumentException when associated object does not contain accessible {@code accessibleName}
	 * @throws IllegalStateException when the assignment is illegal 
	 */
	void setValue(String accessibleName, Object value);

	/**
	 * Returns the type of the given accessible
	 */
	Class<?> getType(String accessibleName);
}
