package pl.com.malaugrim.jconverter.core.copymode;

import pl.com.malaugrim.jconverter.core.accessor.Accessor;
import pl.com.malaugrim.jconverter.core.accessor.FieldAccessor;
import pl.com.malaugrim.jconverter.core.accessor.PropertyAccessor;
import pl.com.malaugrim.jconverter.core.copymode.validators.AccessibleValidator;

/**
 * In this mode accessibles are shallow copied, which means that source value is just naively assigned to the destination accessible.
 * 
 * <p><b>Accessible</b>: either field or property</p>
 * <p><b>Accessible chain</b> is a dot separated list of accessibles. Basically accessible chain represents a parent-child relationship 
 * between accessibles - an accessible before the dot is said to contain accessbile after the dot. How exactly the chain is used to get and 
 * set accessibles - is dependent on provided {@link Accessor accessor classes}. For instance a {@link FieldAccessor} will use
 * reflection to get/set correspondent fields, while {@link PropertyAccessor} will use public getters/setters to get or set a property.
 *
 * <p>Example: </p>
 * Given the following chain: someValue.otherValue:
 * <ul>
 *     <li>A source accessor of type {@link FieldAccessor} will use reflective calls to get field 'otherValue', which is contained in 
 *     object represented by field 'someValue', which in turn is contained in the source object</li>
 *     <li>A source accessor of type {@link PropertyAccessor} will use following method calls to get the value: 
 *     srcObject.getSomeValue().getOtherValue()</li>
 * </ul>
 * </p>
 * 
 * @author Lukasz Tutka
 */
public class ShallowCopyMode<S extends Accessor, D extends Accessor> extends AbstractCopyMode<S, D> {

	/**
	 * Creates shallow copy mode.
	 *
	 * @param srcAccessorType type of the {@link Accessor}, which will be used to access accessibles in source object
	 * @param dstAccessorType type of the {@link Accessor}, which will be used to access accessibles in destination object
	 * @param srcValidator validator used for validating source accessible
	 * @param dstValidator validator used for validating destination accessible
	 */
	public ShallowCopyMode(Class<S> srcAccessorType, Class<D> dstAccessorType, AccessibleValidator srcValidator,
							  AccessibleValidator dstValidator) {
		super(srcAccessorType, dstAccessorType, srcValidator, dstValidator);
	}

	/**
	 * Creates shallow copy mode.
	 *
	 * @param srcAccessorType type of the {@link Accessor}, which will be used to access accessibles in source object
	 * @param dstAccessorType type of the {@link Accessor}, which will be used to access accessibles in destination object
	 */
	public ShallowCopyMode(Class<S> srcAccessorType, Class<D> dstAccessorType) {
		super(srcAccessorType, dstAccessorType);
	}

	@Override
	protected void performCopy(String srcAccessibleChain, String dstAccessibleChain) {
		Object value = getSrcValue(srcAccessibleChain);
		setValue(dstAccessibleChain, value);
	}
}
