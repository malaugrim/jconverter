package pl.com.malaugrim.jconverter.core.copymode.validators;

/**
 * @author Lukasz Tutka
 */
public abstract class AbstractAccessibleValidator implements AccessibleValidator {

	protected static final String SRC_PREFIX = "Source";
	protected static final String DST_PREFIX = "Destination";
	protected static final String FIELD_ACCESSIBLE_TYPE = "field";
	protected static final String PROPERTY_ACCESSIBLE_TYPE = "property";
	
	private static final String NO_SUCH_FIELD_PATTERN = "%s object does not contain %s '%s'";

	protected IllegalStateException createNoSuchAccessibleException(String prefix, String accessibleName, String accessibleType) {
		String message = createNoSuchAccessibleMessage(prefix, accessibleName, accessibleType);
		return new IllegalStateException(message);
	}

	private String createNoSuchAccessibleMessage(String prefix, String accessibleName, String accessibleType) {
		return String.format(NO_SUCH_FIELD_PATTERN, prefix, accessibleType, accessibleName);
	}
}
