package pl.com.malaugrim.jconverter.core.copymode.copymodebuilder;

import pl.com.malaugrim.jconverter.core.TypeConverter;
import pl.com.malaugrim.jconverter.core.accessor.Accessor;
import pl.com.malaugrim.jconverter.core.copymode.ConversionMode;
import pl.com.malaugrim.jconverter.core.copymode.validators.AccessibleValidator;

/**
 * Supplier of a concrete implementation of {@link ConversionMode}.
 */
public interface ConversionModeSupplier {

	/**
	 * Creates a concrete implementation of {@link ConversionMode}.
	 */
	ConversionMode get(Class<? extends Accessor> srcAccessorType, Class<? extends Accessor> dstAccessorType,
					   AccessibleValidator srcValidator, AccessibleValidator dstValidator, TypeConverter typeConverter);
}
