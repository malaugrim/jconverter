package pl.com.malaugrim.jconverter.core.accessor;

import com.google.common.base.Defaults;
import com.google.common.base.Preconditions;
import pl.com.malaugrim.jconverter.core.typepromotion.TypePromotor;

import java.util.HashMap;
import java.util.Map;

/**
 * Base accessor containing common functionality, like promoting java types and determining, if value assignment is legal or not.
 * 
 * @author Lukasz Tutka
 */
abstract class AbstractAccessor implements Accessor {

	private static final String ILLEGAL_OBJECT_MSG_PATTERN = "Illegal object type: %s, expected: %s";
	private static final String OBJECT_NOT_SET_MSG = "Object needs to be set before using accessor";
	private static final Map<Class<?>, Class<?>> ALLOWED_ASSIGNMENTS = new HashMap<>();

	protected final Class<?> objectType;
	protected Object object;
	private TypePromotor typePromotor;


	protected AbstractAccessor(Class<?> objectType) {
		this.objectType = objectType;
	}

	public void setObject(Object object) {
		throwExceptionWhenObjectIsIllegal(object);
		this.object = object;
	}

	@Override
	public Object getObject() {
		return object;
	}

	/**
	 * Sets {@link TypePromotor} for promoting types before assignment.
	 *
	 * @see TypePromotor
	 */
	public void setTypePromotor(TypePromotor typePromotor) {
		this.typePromotor = typePromotor;
	}

	/**
	 * Tests, whether {@code srcType} is assignable to {@code dstType}. If necessary, the {@link TypePromotor} is used to check, if
	 * assignment would be legal after type promotion.
	 *
	 * @return {@code true} only when following assignment is legal in java language:
	 * {@code dstType = srcType}
	 */
	protected boolean isAssignable(Class<?> dstType, Class<?> srcType) {
		return dstType.isAssignableFrom(srcType) || canBeBoxedOrUnboxed(dstType, srcType) || typePromotor.canPromote(srcType, dstType);

	}

	/**
	 * Returns default value for type {@code dstType} when src value is null and {@code dstType} is a primitive.
	 */
	protected Object getDefaultIfValueIsNullAndDstIsPrimitive(Class<?> dstType, Object value) {
		if(value == null && dstType.isPrimitive()) {
			value = Defaults.defaultValue(dstType);
		}

		return value;
	}

	/**
	 * Promotes {@code value} to {@code dstType}, if this operation is legal according to java language specs. Otherwise returns
	 * passed {@code value}.
	 */
	protected Object promoteIfNecessary(Class<?> dstType, Object value) {
		if(typePromotor != null && value != null && typePromotor.canPromote(value.getClass(), dstType)) {
			return typePromotor.promote(value, dstType);
		}

		return value;
	}

	protected void throwExceptionIfObjectNotSet() {
		Preconditions.checkState(object != null, OBJECT_NOT_SET_MSG);
	}

	private void throwExceptionWhenObjectIsIllegal(Object object) {
		Class<?> actualType = object.getClass();
		String msg = String.format(ILLEGAL_OBJECT_MSG_PATTERN, actualType, objectType);
		Preconditions.checkState(actualType == objectType, msg);
	}

	private boolean canBeBoxedOrUnboxed(Class<?> dstType, Class<?> srcType) {
		Class<?> allowedType = ALLOWED_ASSIGNMENTS.get(srcType);
		return allowedType != null && allowedType.equals(dstType);
	}

	static {
		ALLOWED_ASSIGNMENTS.put(char.class, Character.class);
		ALLOWED_ASSIGNMENTS.put(Character.class, char.class);
		ALLOWED_ASSIGNMENTS.put(boolean.class, Boolean.class);
		ALLOWED_ASSIGNMENTS.put(Boolean.class, boolean.class);
		ALLOWED_ASSIGNMENTS.put(byte.class, Byte.class);
		ALLOWED_ASSIGNMENTS.put(Byte.class, byte.class);
		ALLOWED_ASSIGNMENTS.put(Short.class, short.class);
		ALLOWED_ASSIGNMENTS.put(int.class, Integer.class);
		ALLOWED_ASSIGNMENTS.put(Integer.class, int.class);
		ALLOWED_ASSIGNMENTS.put(float.class, Float.class);
		ALLOWED_ASSIGNMENTS.put(Float.class, float.class);
		ALLOWED_ASSIGNMENTS.put(double.class, Double.class);
		ALLOWED_ASSIGNMENTS.put(Double.class, double.class);
		ALLOWED_ASSIGNMENTS.put(long.class, Long.class);
		ALLOWED_ASSIGNMENTS.put(Long.class, long.class);
	}

}
