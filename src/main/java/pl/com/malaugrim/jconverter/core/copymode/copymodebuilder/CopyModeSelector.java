package pl.com.malaugrim.jconverter.core.copymode.copymodebuilder;

import pl.com.malaugrim.jconverter.core.TypeConverter;
import pl.com.malaugrim.jconverter.core.copymode.ConversionMode;
import pl.com.malaugrim.jconverter.core.copymode.DeepCopyMode;
import pl.com.malaugrim.jconverter.core.copymode.ShallowCopyMode;

/**
 * Provides a way to select the desired copy mode: shallow, deep copy or conversion mode.
 * 
 * @author Lukasz Tutka
 */
public class CopyModeSelector {

	private CopyModeBuilder copyModeBuilder;

	CopyModeSelector(CopyModeBuilder copyModeBuilder) {
		this.copyModeBuilder = copyModeBuilder;
	}

	/**
	 * Tells the builder to create {@link ShallowCopyMode shallow copy mode}
	 */
	public From shallowCopy() {
		return new From(copyModeBuilder);
	}

	/**
	 * Tells the builder to create {@link DeepCopyMode deep copy mode}
	 */
	public From deepCopy() {
		copyModeBuilder.deepCopy = true;
		return new From(copyModeBuilder);
	}

	/**
	 * Tells the builder to create {@link ConversionMode conversion mode}.
	 * 
	 * @param supplier conversion mode supplier, for convenience you can use of {@link ConversionModeVariant} enums.
	 * @param typeConverter a type converter for conversion mode
	 */
	public From convert(ConversionModeSupplier supplier, TypeConverter typeConverter) {
		copyModeBuilder.conversionModeSupplier = supplier;
		copyModeBuilder.typeConverter = typeConverter;
		return new From(copyModeBuilder);
	}
}
