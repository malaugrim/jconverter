package pl.com.malaugrim.jconverter.core.copymode;

import pl.com.malaugrim.jconverter.core.TypeConverter;
import pl.com.malaugrim.jconverter.core.accessor.Accessor;
import pl.com.malaugrim.jconverter.core.accessor.FieldAccessor;
import pl.com.malaugrim.jconverter.core.accessor.PropertyAccessor;
import pl.com.malaugrim.jconverter.core.copymode.validators.AccessibleValidator;

/**
 * In this mode accessibles are converted with the use of {@link TypeConverter} before assigning in destination object. If value in the
 * destination object is {@code null}, then {@link TypeConverter#convert(Object, Class)} is called providing source value and destination
 * accessible type, otherwise {@link TypeConverter#convert(Object, Object)} is called providing source value and destination value.
 * 
 * <p><b>Accessible</b>: either field or property</p>
 * <p><b>Accessible chain</b> is a dot separated list of accessibles. Basically accessible chain represents a parent-child relationship 
 * between accessibles - an accessible before the dot is said to contain accessbile after the dot. How exactly the chain is used to get and 
 * set accessibles - is dependent on provided {@link Accessor accessor classes}. For instance a {@link FieldAccessor} will use
 * reflection to get/set correspondent fields, while {@link PropertyAccessor} will use public getters/setters to get or set a property.
 *
 * <p>Example: </p>
 * Given the following chain: someValue.otherValue:
 * <ul>
 *     <li>A source accessor of type {@link FieldAccessor} will use reflective calls to get field 'otherValue', which is contained in 
 *     object represented by field 'someValue', which in turn is contained in the source object</li>
 *     <li>A source accessor of type {@link PropertyAccessor} will use following method calls to get the value: 
 *     srcObject.getSomeValue().getOtherValue()</li>
 * </ul>
 * </p>
 * 
 * @author Lukasz Tutka
 */
public class ConversionModeWithPassingDstValueOrType<S extends Accessor, D extends Accessor> extends ConversionMode<S,D> {


	public ConversionModeWithPassingDstValueOrType(Class<S> srcAccessorType, Class<D> dstAccessorType, AccessibleValidator srcValidator,
												   AccessibleValidator dstValidator, TypeConverter typeConverter) {
		super(srcAccessorType, dstAccessorType, srcValidator, dstValidator, typeConverter);
	}

	public ConversionModeWithPassingDstValueOrType(Class<S> srcAccessorType, Class<D> dstAccessorType, AccessibleValidator srcValidator,
												   AccessibleValidator dstValidator) {
		super(srcAccessorType, dstAccessorType, srcValidator, dstValidator);
	}

	public ConversionModeWithPassingDstValueOrType(Class<S> srcAccessorType, Class<D> dstAccessorType, TypeConverter typeConverter) {
		super(srcAccessorType, dstAccessorType, typeConverter);
	}

	public ConversionModeWithPassingDstValueOrType(Class<S> srcAccessorType, Class<D> dstAccessorType) {
		super(srcAccessorType, dstAccessorType);
	}

	@Override
	@SuppressWarnings("unchecked")
	protected Object convertValue(Object srcValue, String dstAccessibleChain) {
		Object dstValue = getDstValue(dstAccessibleChain);
		if(dstValue == null)
			return typeConverter.convert(srcValue, getDstType(dstAccessibleChain));
		else
			return typeConverter.convert(srcValue, dstValue);
	}
}
