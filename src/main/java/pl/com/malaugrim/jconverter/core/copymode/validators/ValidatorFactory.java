package pl.com.malaugrim.jconverter.core.copymode.validators;

import pl.com.malaugrim.jconverter.core.accessor.Accessor;
import pl.com.malaugrim.jconverter.core.accessor.FieldAccessor;
import pl.com.malaugrim.jconverter.core.accessor.PropertyAccessor;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

/**
 * A simple factory for creating {@link AccessibleValidator accessible validators}.
 * 
 * @author Lukasz Tutka
 */
public class ValidatorFactory {

	private static final Map<Class<? extends Accessor>, Supplier<AccessibleValidator>> srcValidatorMap = new HashMap<>();
	private static final Map<Class<? extends Accessor>, Supplier<AccessibleValidator>> dstValidatorMap = new HashMap<>();

	static {
		srcValidatorMap.put(FieldAccessor.class, SourceFieldValidator::new);
		srcValidatorMap.put(PropertyAccessor.class, SourcePropertyValidator::new);
		dstValidatorMap.put(FieldAccessor.class, DestinationFieldValidator::new);
		dstValidatorMap.put(PropertyAccessor.class, DestinationPropertyValidator::new);
	}



	public AccessibleValidator getSrcValidator(Class<? extends Accessor> srcAccessor) {
		return srcValidatorMap.get(srcAccessor).get();
	}

	public AccessibleValidator getDstValidator(Class<? extends Accessor> dstAccessor) {
		return dstValidatorMap.get(dstAccessor).get();
	}
}
