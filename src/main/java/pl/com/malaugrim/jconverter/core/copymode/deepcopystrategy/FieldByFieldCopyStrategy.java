package pl.com.malaugrim.jconverter.core.copymode.deepcopystrategy;

import java.util.Set;
import java.util.stream.Collectors;

import pl.com.malaugrim.jconverter.core.ObjectInstantiator;
import pl.com.malaugrim.jconverter.core.accessor.AccessibleObjectWrapper;
import pl.com.malaugrim.jconverter.core.accessor.Accessor;
import pl.com.malaugrim.jconverter.core.accessor.AccessorFactory;
import pl.com.malaugrim.jconverter.core.accessor.FieldAccessor;

/**
 * This strategy uses {@link FieldAccessor} to retrieve all fields. Then for each field value a {@link DeepCopyStrategyPicker} is used to
 * find the best strategy. Found strategy is used to perform deep copying.
 * 
 * @author Lukasz Tutka
 */
public class FieldByFieldCopyStrategy implements DeepCopyStrategy {

	private final DeepCopyStrategyPicker picker;
	private ObjectInstantiator instantiator;

	public FieldByFieldCopyStrategy(DeepCopyStrategyPicker picker) {
		this.picker = picker;
	}

	/**
	 * @return {@code true} only if <code>{@link ObjectInstantiator#canInstantiate(Class)} == true</code>
	 */
	@Override
	public boolean canHandle(Object object) {
		return object != null && canBeCloned(object);
	}

	@Override
	public Object deepCopy(Object object) {
		return copyFieldByField(object);
	}

	@Override
	public void setObjectInstantiator(ObjectInstantiator instantiator) {
		this.instantiator = instantiator;
	}

	private boolean canBeCloned(Object object) {
		return instantiator.canInstantiate(object.getClass());
	}

	private Object copyFieldByField(Object object) {
		Object dst = instantiator.createInstance(object.getClass());
		AccessorFactory accessorFactory = new AccessorFactory();
		Accessor srcFieldAccessor = accessorFactory.getFieldAccessor(object);
		Accessor dstFieldAccessor = accessorFactory.getFieldAccessor(dst);
		Set<String> fields = srcFieldAccessor.getAllAccessibleObjects().stream().map(AccessibleObjectWrapper::getAccessibleName).collect(Collectors.toSet());
		fields.forEach(f -> deepCopyField(srcFieldAccessor, dstFieldAccessor, f));
		return dst;
	}

	private void deepCopyField(Accessor srcFieldAccessor, Accessor dstFieldAccessor, String field) {
		Object value = srcFieldAccessor.getValue(field);
		DeepCopyStrategy strategy = picker.findStrategy(value);
		Object deepCopied =  strategy.deepCopy(value);
		dstFieldAccessor.setValue(field, deepCopied);
	}
}
