package pl.com.malaugrim.jconverter.core.copymode.deepcopystrategy;

import pl.com.malaugrim.jconverter.core.ObjectInstantiator;

/**
 * This strategy performs shallow instead of deep copy. It can be applied for any non-null object.
 * 
 * @author Lukasz Tutka
 */
public class ShallowCopyStrategy implements DeepCopyStrategy {

	/**
	 * @return {@code true} for non null objects
	 */
	@Override
	public boolean canHandle(Object object) {
		return object != null;
	}

	/**
	 * Simply returns the given {@code object}.
	 */
	@Override
	public Object deepCopy(Object object) {
		return object;
	}

	/**
	 * Does nothing. This strategy does not use {@link ObjectInstantiator}. 
	 */
	@Override
	public void setObjectInstantiator(ObjectInstantiator instantiator) {

	}
}
