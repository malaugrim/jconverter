package pl.com.malaugrim.jconverter.core.typepromotion.handlers;

import pl.com.malaugrim.jconverter.core.typepromotion.PromotionHandler;

/**
 * @author Lukasz Tutka
 */
public class CharToStringPromotionHandler implements PromotionHandler<Character, String> {

	@Override
	public boolean canPromote(Class<?> srcType, Class<?> dstType) {
		return Character.class.equals(srcType) && String.class.equals(dstType);
	}

	@Override
	public String promote(Character src) {
		return String.valueOf(src);
	}
}
