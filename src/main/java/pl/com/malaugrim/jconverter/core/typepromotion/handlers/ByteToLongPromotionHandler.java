package pl.com.malaugrim.jconverter.core.typepromotion.handlers;

import pl.com.malaugrim.jconverter.core.typepromotion.PromotionHandler;

/**
 * @author Lukasz Tutka
 */
public class ByteToLongPromotionHandler implements PromotionHandler<Byte, Long> {

	@Override
	public boolean canPromote(Class<?> srcType, Class<?> dstType) {
		return Byte.class.equals(srcType) && (long.class.equals(dstType) || Long.class.equals(dstType));
	}

	@Override
	public Long promote(Byte src) {
		return (long) src;
	}
}
