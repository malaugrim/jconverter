package pl.com.malaugrim.jconverter.core;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

/**
 * Responsible for creating instances of the desired type. If an instance supplier is provided via 
 * {@link ObjectInstantiator#addInstanceSupplier(Class, Supplier)}, it is used. Otherwise instantiator will try to invoke public 
 * default constructor. {@link IllegalStateException} will be thrown when class cannot be instantiated.
 * 
 * @author Lukasz Tutka
 */
public class ObjectInstantiator {

	private static final String INSTANTIATION_ERR = "Cannot create instance of class ";
	private final Map<Class<?>, Supplier<?>> instanceSuppliers = new HashMap<>();

	/**
	 * Creates instance of class {@code type}. If instance supplier is provided, it is used. Otherwise public default constructor is called.
	 * 
	 * @param type class, which instance will be created
	 */
	@SuppressWarnings("unchecked")
	public <T> T createInstance(Class<T> type) {
		try {
			if(instanceSuppliers.containsKey(type)) {
				return (T) instanceSuppliers.get(type).get();
			} else if(hasDefaultConstructor(type)){
				return type.newInstance();
			} else {
				throw new IllegalStateException(INSTANTIATION_ERR + type.getName());
			}
		} catch (InstantiationException | IllegalAccessException e) {
			throw new IllegalStateException(e);
		}
	}

	/**
	 * Tests whether this instantiator is able to create instance of class {@code type}
	 * @return {@code true} if there is an proper instance supplier provided or if {@code type} contains public default constructor
	 */
	public boolean canInstantiate(Class<?> type) {
		return instanceSuppliers.containsKey(type) || hasDefaultConstructor(type);
	}

	/**
	 * Adds an instance supplier. This instantiator will use this supplier to create instances of class {@code type}
	 */
	public <T> void addInstanceSupplier(Class<T> type, Supplier<T> supplier) {
		instanceSuppliers.put(type, supplier);
	}

	private boolean hasDefaultConstructor(Class<?> type) {
		return Arrays.stream(type.getConstructors()).filter(c -> c.getParameterCount() == 0).findFirst().isPresent();
	}
}
