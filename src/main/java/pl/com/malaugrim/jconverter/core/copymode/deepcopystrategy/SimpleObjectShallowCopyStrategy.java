package pl.com.malaugrim.jconverter.core.copymode.deepcopystrategy;

import pl.com.malaugrim.jconverter.core.ObjectInstantiator;

/**
 * This strategy performs shallow instead of deep copy. It can be applied only for simple objects. A simple object can be one of the 
 * following:
 * <ul>
 *     <li>Number</li>
 *     <li>Character</li>
 *     <li>String</li>
 *     <li>Boolean</li>
 * </ul>
 * 
 * @author Lukasz Tutka
 */
public class SimpleObjectShallowCopyStrategy implements DeepCopyStrategy {

	/**
	 * @return {@code true} only if {@code object} is one of the following:
	 * <ul>
	 *     <li>Number</li>
	 *     <li>Character</li>
	 *     <li>String</li>
	 *     <li>Boolean</li>
	 * </ul>
	 */
	@Override
	public boolean canHandle(Object object) {
		return object != null && isStringOrNumberOrCharacterOrBoolean(object);
	}

	/**
	 * Simply returns the given {@code object}.
	 */
	@Override
	public Object deepCopy(Object object) {
		return object;
	}

	/**
	 * Does nothing. This strategy does not use {@link ObjectInstantiator}.
	 */
	@Override
	public void setObjectInstantiator(ObjectInstantiator instantiator) {
		
	}

	private boolean isStringOrNumberOrCharacterOrBoolean(Object object) {
		return object instanceof Number || object instanceof String || object instanceof Boolean;
	}
}
