package pl.com.malaugrim.jconverter.core.typepromotion.handlers;

import pl.com.malaugrim.jconverter.core.typepromotion.PromotionHandler;

/**
 * @author Lukasz Tutka
 */
public class LongToDoublePromotionHandler implements PromotionHandler<Long, Double> {

	@Override
	public boolean canPromote(Class<?> srcType, Class<?> dstType) {
		return Long.class.equals(srcType) && (double.class.equals(dstType) || Double.class.equals(dstType));
	}

	@Override
	public Double promote(Long src) {
		return (double) src;
	}
}
