package pl.com.malaugrim.jconverter.core.accessor;

import com.google.common.collect.Lists;
import pl.com.malaugrim.jconverter.core.typepromotion.TypePromotor;
import pl.com.malaugrim.jconverter.core.typepromotion.handlers.*;

/**
 * Singleton factory producing desired accessors for given objects.
 * 
 * @author Lukasz Tutka
 */
public class AccessorFactory {

	@SuppressWarnings("unchecked")
	public <T extends Accessor> T getAccessor(Class<T> accessorType, Object obj) {
		if(accessorType.equals(FieldAccessor.class)) {
			return (T) getFieldAccessor(obj);
		} else if(accessorType.equals(PropertyAccessor.class)) {
			return (T) getPropertyAccessor(obj);
		} else {
			throw new IllegalArgumentException("Accessor type " +accessorType+ " not supported");
		}
	}

	public Accessor getFieldAccessor(Object obj) {
		FieldAccessor fieldAccessor = new FieldAccessor(obj);
		setUpTypePromotor(fieldAccessor);
		return fieldAccessor;
	}

	public Accessor getPropertyAccessor(Object obj) {
		PropertyAccessor propertyAccessor = new PropertyAccessor(obj);
		setUpTypePromotor(propertyAccessor);
		return propertyAccessor;
	}

	private void setUpTypePromotor(AbstractAccessor accessor) {
		accessor.setTypePromotor(new TypePromotor(Lists.newArrayList(
				new CharToIntPromotionHandler(),
				new CharToLongPromotionHandler(),
				new CharToFloatPromotionHandler(),
				new CharToDoublePromotionHandler(),
				new CharToStringPromotionHandler(),
				new ByteToShortPromotionHandler(),
				new ByteToIntPromotionHandler(),
				new ByteToLongPromotionHandler(),
				new ByteToFloatPromotionHandler(),
				new ByteToDoublePromotionHandler(),
				new ShortToIntPromotionHandler(),
				new ShortToLongPromotionHandler(),
				new ShortToFloatPromotionHandler(),
				new ShortToDoublePromotionHandler(),
				new IntToLongPromotionHandler(),
				new IntToFloatPromotionHandler(),
				new IntToDoublePromotionHandler(),
				new LongToFloatPromotionHandler(),
				new LongToDoublePromotionHandler(),
				new FloatToDoublePromotionHandler()
		)));
	}
}
