package pl.com.malaugrim.jconverter.core.copymode.deepcopystrategy;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.stream.Stream;

import pl.com.malaugrim.jconverter.core.ObjectInstantiator;

/**
 * This strategy uses public clone method to perform deep copying. The responsibility for performing actual deep copy is delegated to the 
 * method itself, thus be cautious when using this strategy and make sure clone performs real deep copying.
 * 
 * @author Lukasz Tutka
 */
public class CloneableMethodStrategy implements DeepCopyStrategy {

	/**
	 * @return {@code true} only if class of the given object (or any of its super classes) has public clone method 
	 * (see {@link Object#clone()}).
	 */
	@Override
	public boolean canHandle(Object object) {
		return object != null && isCloneable(object);
	}

	@Override
	public Object deepCopy(Object object) {
		return tryInvokeCloneMethod(object);
	}

	/**
	 * Does nothing. This strategy does not use {@link ObjectInstantiator}. 
	 */
	@Override
	public void setObjectInstantiator(ObjectInstantiator instantiator) {
		
	}

	private boolean isCloneable(Object object) {
		Class<?> type = object.getClass();
		Stream<Method> methods = Arrays.stream(type.getMethods());
		return object instanceof Cloneable && containsPublicCloneMethod(methods);
	}

	private boolean containsPublicCloneMethod(Stream<Method> methodsStream) {
		return methodsStream.filter(this::byPublicCloneMethod).findFirst().isPresent();
	}

	private boolean byPublicCloneMethod(Method m) {
		return m.getName().equals("clone") && m.getParameterCount() == 0;
	}

	private Object tryInvokeCloneMethod(Object object) {
		try {
			Method clone = object.getClass().getMethod("clone");
			return clone.invoke(object);
		} catch (InvocationTargetException | NoSuchMethodException | IllegalAccessException e) {
			throw new RuntimeException(e);
		}
	}
}
