package pl.com.malaugrim.jconverter.core.copymode.copymodebuilder;

import com.google.common.base.Preconditions;
import pl.com.malaugrim.jconverter.core.TypeConverter;
import pl.com.malaugrim.jconverter.core.accessor.Accessor;
import pl.com.malaugrim.jconverter.core.copymode.ConversionMode;
import pl.com.malaugrim.jconverter.core.copymode.CopyMode;
import pl.com.malaugrim.jconverter.core.copymode.DeepCopyMode;
import pl.com.malaugrim.jconverter.core.copymode.ShallowCopyMode;
import pl.com.malaugrim.jconverter.core.copymode.deepcopystrategy.*;
import pl.com.malaugrim.jconverter.core.copymode.validators.AccessibleValidator;
import pl.com.malaugrim.jconverter.core.copymode.validators.ValidatorFactory;

/**
 * <p>Convenient builder for creating various copy modes. The builder is designed in a way, which almost completely eliminates possibility to 
 * misuse it. After creating instance, use the builder to choose desired copy mode, and then fluently point from where to get the  
 * accessibles and where to set the copied value. Afterwards just use {@link CopyModeBuilder#build()} to finalize the builiding process.</p>
 * <p>Following is the example call chain to create shallow copy mode, which takes accessibles from source object fields, and set them as 
 * property using destination object setters:</p>
 * <pre>
 * CopyModeBuilder builder = new CopyModeBuilder();
 * CopyMode copyMode = builder.shallowCopy().fromField().toProperty().build();
 * </pre>
 * 
 * <p><b>Important note:</b> when creating deep copy mode, this builder sets {@link SequentialStrategyPicker} as strategy picker.
 * It also sets concrete strategies in the following order:</p>
 * <ul>
 *     <li>{@link NullObjectStrategy}</li>
 *     <li>{@link CloneableMethodStrategy}</li>
 *     <li>{@link SimpleObjectShallowCopyStrategy}</li>
 *     <li>{@link FieldByFieldCopyStrategy}</li>
 * </ul>
 * 
 * @see ShallowCopyMode
 * @see DeepCopyMode
 * @see ConversionMode
 * 
 * @author Lukasz Tutka
 */
public class CopyModeBuilder {

	private static final String ILLEGAL_COPY_MODE = "It is illegal to create a copy mode which is both a conversion and deep copy mode";
	private static final String MISSING_SRC_OR_DST = "Both source and destination must be selected";

	Class<? extends Accessor> srcAccessorType;
	Class<? extends Accessor> dstAccessorType;
	TypeConverter typeConverter;
	ConversionModeSupplier conversionModeSupplier;
	
	boolean buildStarted;

	boolean deepCopy;
	private final ValidatorFactory validatorFactory = new ValidatorFactory();

	
	/**
	 * Tells the builder to create {@link ShallowCopyMode shallow copy mode}
	 */
	public From shallowCopy() {
		return new From(this);
	}

	/**
	 * Tells the builder to create {@link DeepCopyMode deep copy mode}
	 */
	public From deepCopy() {
		deepCopy = true;
		return new From(this);
	}

	/**
	 * Tells the builder to create {@link ConversionMode conversion mode}.
	 *
	 * @param supplier conversion mode supplier, for convenience you can use of {@link ConversionModeVariant} enums.
	 * @param typeConverter a type converter for conversion mode
	 */
	public From convert(ConversionModeSupplier supplier, TypeConverter typeConverter) {
		conversionModeSupplier = supplier;
		this.typeConverter = typeConverter;
		return new From(this);
	}

	/**
	 * Finalizes the building process and returns created {@link CopyMode copy mode}.
	 */
	@SuppressWarnings("unchecked")
	public <T extends CopyMode> T build() {
		checkBuilderState();
		AccessibleValidator srcValidator = getSrcValidator();
		AccessibleValidator dstValidator = getDstValidator();

		T copyMode = (T) determineCopyMode(srcValidator, dstValidator);
		clearState();
		return copyMode;
	}

	private void checkBuilderState() {
		Preconditions.checkState(srcAccessorType != null, MISSING_SRC_OR_DST);
		Preconditions.checkState(dstAccessorType != null, MISSING_SRC_OR_DST);
		Preconditions.checkState(!deepCopy || typeConverter == null, ILLEGAL_COPY_MODE);
	}

	private AccessibleValidator getSrcValidator() {
		return validatorFactory.getSrcValidator(srcAccessorType);
	}

	private AccessibleValidator getDstValidator() {
		return validatorFactory.getDstValidator(dstAccessorType);
	}

	private CopyMode determineCopyMode(AccessibleValidator srcValidator, AccessibleValidator dstValidator) {
		if(deepCopy) {
			return createDeepCopyMode(srcValidator, dstValidator);
		} else if(conversionModeSupplier != null) {
			return conversionModeSupplier.get(srcAccessorType, dstAccessorType, srcValidator, dstValidator, typeConverter);
		} else {
			return new ShallowCopyMode<>(srcAccessorType, dstAccessorType, srcValidator, dstValidator);
		}
	}

	private DeepCopyMode<? extends Accessor, ? extends Accessor> createDeepCopyMode(AccessibleValidator srcValidator, AccessibleValidator dstValidator) {
		DeepCopyMode<? extends Accessor, ? extends Accessor> deepCopyMode = new DeepCopyMode<>(srcAccessorType, dstAccessorType, srcValidator, dstValidator);
		SequentialStrategyPicker picker = new SequentialStrategyPicker();
		deepCopyMode.setDeepCopyStrategyPicker(picker);
		deepCopyMode.setDeepCopyStrategies(new NullObjectStrategy(), new CloneableMethodStrategy(), new SimpleObjectShallowCopyStrategy(),
				new FieldByFieldCopyStrategy(picker));
		return deepCopyMode;
	}

	private void clearState() {
		srcAccessorType = null;
		dstAccessorType = null;
		deepCopy = false;
		typeConverter = null;
		buildStarted = false;
	}
}
