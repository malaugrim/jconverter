package pl.com.malaugrim.jconverter.core.accessor;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * @author Lukasz Tutka
 */
public class ClassInfo {

	private Class<?> type;
	private Map<String, Field> fields;
	private List<Method> propertyMethods;
	private Map<String, Boolean> fieldsDefaultAccessibility;
	private Map<String, Method> getters;
	private Map<String, Method> setters;



	public Class<?> getType() {
		return type;
	}

	public void setType(Class<?> type) {
		this.type = type;
	}

	public Map<String, Field> getFields() {
		return fields;
	}

	public void setFields(Map<String, Field> fields) {
		this.fields = fields;
	}

	public List<Method> getPropertyMethods() {
		return propertyMethods;
	}

	public void setPropertyMethods(List<Method> methods) {
		this.propertyMethods = methods;
	}

	public Map<String, Method> getGetters() {
		return getters;
	}

	public void setGetters(Map<String, Method> getters) {
		this.getters = getters;
	}

	public Map<String, Method> getSetters() {
		return setters;
	}

	public void setSetters(Map<String, Method> setters) {
		this.setters = setters;
	}

	public void setFieldsDefaultAccessibility(Map<String, Boolean> fieldsDefaultAccessibility) {
		this.fieldsDefaultAccessibility = fieldsDefaultAccessibility;
	}

	public boolean isFieldAccessibleByDefault(String field) {
		return fieldsDefaultAccessibility.get(field);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof ClassInfo)) return false;
		ClassInfo that = (ClassInfo) o;
		return Objects.equals(type, that.type);
	}

	@Override
	public int hashCode() {
		return Objects.hash(type);
	}
}
