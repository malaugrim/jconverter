package pl.com.malaugrim.jconverter.core.typepromotion.handlers;

import pl.com.malaugrim.jconverter.core.typepromotion.PromotionHandler;

/**
 * @author Lukasz Tutka
 */
public class IntToFloatPromotionHandler implements PromotionHandler<Integer, Float> {

	@Override
	public boolean canPromote(Class<?> srcType, Class<?> dstType) {
		return Integer.class.equals(srcType) && (float.class.equals(dstType) || Float.class.equals(dstType));
	}

	@Override
	public Float promote(Integer src) {
		return (float) src;
	}
}
