package pl.com.malaugrim.jconverter.core.typepromotion.handlers;

import pl.com.malaugrim.jconverter.core.typepromotion.PromotionHandler;

/**
 * @author Lukasz Tutka
 */
public class IntToLongPromotionHandler implements PromotionHandler<Integer, Long> {

	@Override
	public boolean canPromote(Class<?> srcType, Class<?> dstType) {
		return Integer.class.equals(srcType) && (long.class.equals(dstType) || Long.class.equals(dstType));
	}

	@Override
	public Long promote(Integer src) {
		return (long) src;
	}
}
