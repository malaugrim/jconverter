package pl.com.malaugrim.jconverter.core.copymode;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import pl.com.malaugrim.jconverter.core.ObjectInstantiator;
import pl.com.malaugrim.jconverter.core.accessor.Accessor;
import pl.com.malaugrim.jconverter.core.accessor.FieldAccessor;
import pl.com.malaugrim.jconverter.core.accessor.PropertyAccessor;
import pl.com.malaugrim.jconverter.core.copymode.deepcopystrategy.DeepCopyStrategy;
import pl.com.malaugrim.jconverter.core.copymode.deepcopystrategy.DeepCopyStrategyPicker;
import pl.com.malaugrim.jconverter.core.copymode.validators.AccessibleValidator;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

/**
 * <p>In this mode all accessibles are deep copied, which means that for source accessible new instance is created and filled with deep copies 
 * of its internal components. {@link ObjectInstantiator} is used for creating new instances (if destination accessible does not have
 * default public constructor, an instance supplier has to be provided with 
 * {@link ObjectInstantiator#addInstanceSupplier(Class, Supplier)}).</p>
 * 
 * <p>DeepCopyMode uses various {@link DeepCopyStrategy deep copy strategies} to perform the deep copy. A
 * {@link DeepCopyStrategyPicker} is utilized to pick the best strategy.</p>
 * 
 * <p>Please note that after creation, the deep copy mode does not have any strategies set and no 
 * {@link DeepCopyStrategyPicker strategy picker}. You need to provide strategies with 
 * {@link DeepCopyMode#setDeepCopyStrategies(List)} and strategy picker with 
 * {@link DeepCopyMode#setDeepCopyStrategyPicker(DeepCopyStrategyPicker)}.</p>
 * 
 * <p>Also note that strategies set in DeepCopyMode will 
 * overwrite strategies set in {@link DeepCopyStrategyPicker}. You can find some pre-defined strategies in package 
 * {@link pl.com.malaugrim.jconverter.core.copymode.deepcopystrategy}.</p>
 * 
 * <p><b>Accessible</b>: either field or property</p>
 * <p><b>Accessible chain</b> is a dot separated list of accessibles. Basically accessible chain represents a parent-child relationship 
 * between accessibles - an accessible before the dot is said to contain accessbile after the dot. How exactly the chain is used to get and 
 * set accessibles - is dependent on provided {@link Accessor accessor classes}. For instance a {@link FieldAccessor} will use
 * reflection to get/set correspondent fields, while {@link PropertyAccessor} will use public getters/setters to get or set a property.
 *
 * <p>Example: </p>
 * Given the following chain: someValue.otherValue:
 * <ul>
 *     <li>A source accessor of type {@link FieldAccessor} will use reflective calls to get field 'otherValue', which is contained in 
 *     object represented by field 'someValue', which in turn is contained in the source object</li>
 *     <li>A source accessor of type {@link PropertyAccessor} will use following method calls to get the value: 
 *     srcObject.getSomeValue().getOtherValue()</li>
 * </ul>
 * </p>
 * 
 * @author Lukasz Tutka
 */
public class DeepCopyMode<S extends Accessor, D extends Accessor> extends AbstractCopyMode<S, D> {

	private static final String MISSING_STRATEGY_PICKER = "Missing deep copy strategy picker";

	private List<DeepCopyStrategy> deepCopyStrategies = new ArrayList<>();
	private DeepCopyStrategyPicker strategyPicker;
	private boolean shallowCopyWhenCannotFindStrategy = true;
	private boolean strategyPickerSetUp;


	public DeepCopyMode(Class<S> srcAccessorType, Class<D> dstAccessorType, AccessibleValidator srcValidator,
						AccessibleValidator dstValidator) {
		super(srcAccessorType, dstAccessorType, srcValidator, dstValidator);
	}

	public DeepCopyMode(Class<S> srcAccessorType, Class<D> dstAccessorType) {
		super(srcAccessorType, dstAccessorType);
	}

	@SuppressWarnings("SameParameterValue")
	public void shallowCopyWhenCannotClone(boolean shallowCopy) {
		this.shallowCopyWhenCannotFindStrategy = shallowCopy;
	}
	
	public void setDeepCopyStrategyPicker(DeepCopyStrategyPicker strategyPicker) {
		strategyPickerSetUp = false;
		this.strategyPicker = strategyPicker;
	}
	
	public void setDeepCopyStrategies(List<DeepCopyStrategy> strategies) {
		this.deepCopyStrategies = new ArrayList<>(strategies);
	}
	
	public void setDeepCopyStrategies(DeepCopyStrategy... strategies) {
		this.deepCopyStrategies = Lists.newArrayList(strategies);
	}

	@Override
	protected void performCopy(String srcAccessibleChain, String dstAccessibleChain) {
		Object value = getSrcValue(srcAccessibleChain);
		Object copy = deepCopy(value);
		setValue(dstAccessibleChain, copy);
	}

	private Object deepCopy(Object object) {
		checkIfStrategyPickerProvided();
		
		if(object == null)
			return null;

		return findStrategy(object).deepCopy(object);
	}

	private void checkIfStrategyPickerProvided() {
		Preconditions.checkState(strategyPicker != null, MISSING_STRATEGY_PICKER);
	}

	private DeepCopyStrategy findStrategy(Object object) {
		setUpStrategyPicker();
		return strategyPicker.findStrategy(object);
	}

	private void setUpStrategyPicker() {
		if(!strategyPickerSetUp) {
			strategyPicker.setShallowCopyWhenCannotFindStrategy(shallowCopyWhenCannotFindStrategy);
			if(!deepCopyStrategies.isEmpty()) {
				strategyPicker.setStrategies(deepCopyStrategies);
			}
			
			strategyPicker.setObjectInstantiator(getObjectInstantiator());
			strategyPickerSetUp = true;
		}
	}

}
