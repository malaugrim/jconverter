package pl.com.malaugrim.jconverter.core.copymode.deepcopystrategy;

import pl.com.malaugrim.jconverter.core.ObjectInstantiator;
import pl.com.malaugrim.jconverter.core.copymode.DeepCopyMode;

/**
 * A strategy for performing deep copying. When creating a new instance of the destination object, each strategy may use 
 * {@link ObjectInstantiator}. The instantiator is provided with {@link DeepCopyStrategy#setObjectInstantiator(ObjectInstantiator)}.
 * 
 * @see DeepCopyMode
 * @see DeepCopyStrategyPicker
 * @see ObjectInstantiator
 * 
 * @author Lukasz Tutka
 */
public interface DeepCopyStrategy {

	/**
	 * Tests whether this strategy is suitable to deep copy the given object.
	 */
	boolean canHandle(Object object);

	/**
	 * Performs deep copying.
	 */
	Object deepCopy(Object object);

	/**
	 * Sets object instantiator. 
	 */
	void setObjectInstantiator(ObjectInstantiator instantiator);

}
