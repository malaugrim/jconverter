package pl.com.malaugrim.jconverter.core;

/**
 * Allows conversion object of type {@code S} to {@code D}. Converted object may be either a simple type 
 * (like Int, Long or String) or more complex structure consisting of many other objects.
 * 
 * @param <S> source type
 * @param <D> destination type
 *           
 *   
 */
public interface TypeConverter<S, D> {

	/**
	 * Converts {@code srcObj} to {@code dstObj}. May throw an unspecified {@link RuntimeException}, when illegal types are passed 
	 * for conversion, thus always use {@link TypeConverter#canConvert(Class, Class)} before passing arguments.
	 */
	D convert(S srcObj, D dstObj);

	/**
	 * Converts {@code srcObj} to object of given {@code dstClass}. May throw an unspecified {@link RuntimeException}, when 
	 * illegal object and type is passed for conversion, thus always use {@link TypeConverter#canConvert(Class, Class)} before 
	 * passing arguments.
	 * 
	 * @throws IllegalStateException when {@code dstClass} cannot be instantiated
	 */
	D convert(S srcObj, Class<? extends D> dstClass);

	/**
	 * Tests whether this {@code TypeConverter} is capable of converting objects of class {@code srcClass} to objects of class 
	 * {@code dstClass}.
	 */
	boolean canConvert(Class<?> srcClass, Class<?> dstClass);

}
