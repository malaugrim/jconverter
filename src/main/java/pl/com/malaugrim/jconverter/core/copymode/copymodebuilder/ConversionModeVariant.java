package pl.com.malaugrim.jconverter.core.copymode.copymodebuilder;

import java.lang.reflect.Constructor;

import pl.com.malaugrim.jconverter.core.TypeConverter;
import pl.com.malaugrim.jconverter.core.accessor.Accessor;
import pl.com.malaugrim.jconverter.core.copymode.ConversionMode;
import pl.com.malaugrim.jconverter.core.copymode.ConversionModeWithPassingDstTypeOnly;
import pl.com.malaugrim.jconverter.core.copymode.ConversionModeWithPassingDstValueOnly;
import pl.com.malaugrim.jconverter.core.copymode.ConversionModeWithPassingDstValueOrType;
import pl.com.malaugrim.jconverter.core.copymode.validators.AccessibleValidator;

public enum ConversionModeVariant implements ConversionModeSupplier {

	/**
	 * In this variant destination value will be passed to ({@link TypeConverter#convert(Object, Object)}) if not null. 
	 * Otherwise type od the destination value will be passed to {@link TypeConverter#convert(Object, Class)}.
	 */
	TAKE_DST_VALUE_OR_TYPE(ConversionModeWithPassingDstValueOrType.class),

	/**
	 * In this variant destination value will be passed to {@link TypeConverter#convert(Object, Object)}, even if it's null.
	 */
	TAKE_DST_VALUE_ONLY(ConversionModeWithPassingDstValueOnly.class),

	/**
	 * In this variant type od the destination value will be passed to {@link TypeConverter#convert(Object, Class)}.
	 */
	TAKE_DST_TYPE_ONLY(ConversionModeWithPassingDstTypeOnly.class);


	private final Class<? extends ConversionMode> conversionModeType;

	ConversionModeVariant(Class<? extends ConversionMode> conversionModeType) {
		this.conversionModeType = conversionModeType;
	}

	@Override
	public ConversionMode get(Class<? extends Accessor> srcAccessorType, Class<? extends Accessor> dstAccessorType,
							  AccessibleValidator srcValidator, AccessibleValidator dstValidator, TypeConverter typeConverter) {

		try {
			return tryCreateNewInstance(srcAccessorType, dstAccessorType, srcValidator, dstValidator, typeConverter);
		} catch (ReflectiveOperationException e) {
			throw new RuntimeException(e);
		}
	}

	private ConversionMode tryCreateNewInstance(Class<? extends Accessor> srcAccessorType, Class<? extends Accessor> dstAccessorType,
												AccessibleValidator srcValidator, AccessibleValidator dstValidator,
												TypeConverter typeConverter) throws ReflectiveOperationException {

		if (typeConverter != null) {
			Constructor<? extends ConversionMode> constructor = conversionModeType.getConstructor(Class.class, Class.class,
					AccessibleValidator.class, AccessibleValidator.class,
					TypeConverter.class);
			return constructor.newInstance(srcAccessorType, dstAccessorType, srcValidator, dstValidator, typeConverter);
		} else {
			Constructor<? extends ConversionMode> constructor = conversionModeType.getConstructor(Class.class, Class.class, AccessibleValidator.class, AccessibleValidator.class);
			return constructor.newInstance(srcAccessorType, dstAccessorType, srcValidator, dstValidator);
		}
	}
}
