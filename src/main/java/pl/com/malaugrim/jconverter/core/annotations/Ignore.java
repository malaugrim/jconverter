package pl.com.malaugrim.jconverter.core.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import pl.com.malaugrim.jconverter.objectconverters.AnnotationObjectConverter;

/**
 * <p>Used to mark accessibles, which should be omitted from conversion process.</p>
 * 
 * <p>Annotation can be placed either on field or method. However bare in mind that when placed on a method, the field name and property 
 * name infered from the method need to be exactly the same. If they are not, use {@link Ignore#value()} to point the corresponding 
 * field. It is very important, because {@link AnnotationObjectConverter} uses by default field to field shallow copy mode and it will
 * have no information, that the field should be ignored. And as a result the field will be shallow copied.</p>
 * 
 * <p><b>Accessible</b>: either field or property</p>
 * 
 * @author Lukasz Tutka
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.FIELD, ElementType.METHOD })
@ConverterAnnotation
public @interface Ignore {

	/**
	 * (Optional) Name of the corresponding field. Should be used when annotation is placed on method, which name is different than the 
	 * name of corresponding field. If this is omitted, the field will copied in such cases.
	 */
	String value() default "";
}
