package pl.com.malaugrim.jconverter.core.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation for marking other annotations read by {@link AnnotationProcessor}.
 * 
 * @see DeepCopy
 * @see Convert
 * @see Ignore
 * @see ShallowCopy
 * 
 * @author Lukasz Tutka
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.ANNOTATION_TYPE)
public @interface ConverterAnnotation {

}
