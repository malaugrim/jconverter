package pl.com.malaugrim.jconverter.core.typepromotion.handlers;

import pl.com.malaugrim.jconverter.core.typepromotion.PromotionHandler;

/**
 * @author Lukasz Tutka
 */
public class CharToIntPromotionHandler implements PromotionHandler<Character, Integer> {


	@Override
	public boolean canPromote(Class<?> srcType, Class<?> dstType) {
		return Character.class.equals(srcType) &&
				(Integer.class.equals(dstType) ||
				int.class.equals(dstType));
	}

	@Override
	public Integer promote(Character src) {
		return (int) src;
	}
}
