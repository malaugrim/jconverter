package pl.com.malaugrim.jconverter.core.copymode;

import com.google.common.base.Preconditions;
import pl.com.malaugrim.jconverter.core.TypeConverter;
import pl.com.malaugrim.jconverter.core.accessor.Accessor;
import pl.com.malaugrim.jconverter.core.accessor.AccessorFactory;
import pl.com.malaugrim.jconverter.core.accessor.FieldAccessor;
import pl.com.malaugrim.jconverter.core.accessor.PropertyAccessor;
import pl.com.malaugrim.jconverter.core.copymode.validators.AccessibleValidator;

import java.util.Objects;

/**
 * In this mode accessibles are converted with the use of {@link TypeConverter} before assigning in destination object.
 *
 * <p><b>Accessible</b>: either field or property</p>
 * <p><b>Accessible chain</b> is a dot separated list of accessibles. Basically accessible chain represents a parent-child relationship 
 * between accessibles - an accessible before the dot is said to contain accessbile after the dot. How exactly the chain is used to get and 
 * set accessibles - is dependent on provided {@link Accessor accessor classes}. For instance a {@link FieldAccessor} will use
 * reflection to get/set correspondent fields, while {@link PropertyAccessor} will use public getters/setters to get or set a property.
 *
 * <p>Example: </p>
 * Given the following chain: someValue.otherValue:
 * <ul>
 *     <li>A source accessor of type {@link FieldAccessor} will use reflective calls to get field 'otherValue', which is contained in 
 *     object represented by field 'someValue', which in turn is contained in the source object</li>
 *     <li>A source accessor of type {@link PropertyAccessor} will use following method calls to get the value: 
 *     srcObject.getSomeValue().getOtherValue()</li>
 * </ul>
 * </p>
 * 
 * @author Lukasz Tutka
 */
public abstract class ConversionMode<S extends Accessor, D extends Accessor> extends AbstractCopyMode<S, D> {


	protected TypeConverter typeConverter;


	/**
	 * Creates conversion copy mode.
	 *
	 * @param srcAccessorType type of the {@link Accessor}, which will be used to access accessibles in source object
	 * @param dstAccessorType type of the {@link Accessor}, which will be used to access accessibles in destination object
	 * @param srcValidator validator used for validating source accessible
	 * @param dstValidator validator used for validating destination accessible
	 * @param typeConverter used to convert source accessible into destination accessible type
	 */
	public ConversionMode(Class<S> srcAccessorType, Class<D> dstAccessorType, AccessibleValidator srcValidator,
						  AccessibleValidator dstValidator, TypeConverter typeConverter) {
		super(srcAccessorType, dstAccessorType, srcValidator, dstValidator);
		this.typeConverter = Preconditions.checkNotNull(typeConverter);
	}

	/**
	 * Creates conversion copy mode.
	 *
	 * @param srcAccessorType type of the {@link Accessor}, which will be used to access accessibles in source object
	 * @param dstAccessorType type of the {@link Accessor}, which will be used to access accessibles in destination object
	 * @param srcValidator validator used for validating source accessible
	 * @param dstValidator validator used for validating destination accessible
	 */
	public ConversionMode(Class<S> srcAccessorType, Class<D> dstAccessorType, AccessibleValidator srcValidator,
												   AccessibleValidator dstValidator) {
		super(srcAccessorType, dstAccessorType, srcValidator, dstValidator);
	}

	/**
	 * Creates conversion copy mode.
	 *
	 * @param srcAccessorType type of the {@link Accessor}, which will be used to access accessibles in source object
	 * @param dstAccessorType type of the {@link Accessor}, which will be used to access accessibles in destination object
	 * @param typeConverter used to convert source accessible into destination accessible type
	 */
	public ConversionMode(Class<S> srcAccessorType, Class<D> dstAccessorType, TypeConverter typeConverter) {
		super(srcAccessorType, dstAccessorType);
		this.typeConverter = Preconditions.checkNotNull(typeConverter);
	}

	/**
	 * Creates conversion copy mode.
	 *
	 * @param srcAccessorType type of the {@link Accessor}, which will be used to access accessibles in source object
	 * @param dstAccessorType type of the {@link Accessor}, which will be used to access accessibles in destination object
	 */
	public ConversionMode(Class<S> srcAccessorType, Class<D> dstAccessorType) {
		super(srcAccessorType, dstAccessorType);
	}


	public void setTypeConverter(TypeConverter typeConverter) {
		this.typeConverter = typeConverter;
	}

	/**
	 * Converts accessible extracted from {@code srcAccessibleChain} from the source object into the type of accessible extracted from 
	 * {@code dstAccessibleChain} in destination object.
	 *
	 * @throws IllegalStateException if provided converter is not able to perform conversion
	 */
	@Override
	protected void performCopy(String srcAccessibleChain, String dstAccessibleChain) {
		Object srcValue = getSrcValue(srcAccessibleChain);
		checkIfConverterIsAbleToCopy(srcValue, dstAccessibleChain);
		Object converted = convertValue(srcValue, dstAccessibleChain);
		setValue(dstAccessibleChain, converted);
	}

	@SuppressWarnings({"ConstantConditions", "unchecked"})
	protected void checkIfConverterIsAbleToCopy(Object value, String dstAccessibleChain) {
		Objects.requireNonNull(typeConverter, "Conversion copy mode needs TypeConverter to perform copy.");
		Class<?> dstClass = getDstType(dstAccessibleChain);

		if(value != null && !typeConverter.canConvert(value.getClass(), dstClass)) {
			throw new IllegalStateException("Provided converter is not suitable to copy [" + value.getClass() + "] to [" +dstClass+ "]");
		}
	}

	protected Class<?> getDstType(String dstAccessibleChain) {
		Class<?> dstType = null;
		Accessor accessor = dstAccessor;
		String[] accessibles = dstAccessibleChain.split("\\.");
		for (int i = 0, size = accessibles.length; i < size; i++) {
			String accessible = accessibles[i];
			dstType = accessor.getType(accessible);
			
			if(i < size - 1) {
				Object value = accessor.getValue(accessible);
				if(value == null) {
					value = getObjectInstantiator().createInstance(accessor.getType(accessible));
					accessor.setValue(accessible, value);
				}

				accessor = new AccessorFactory().getAccessor(accessor.getClass(), value);
			}
		}
		
		return dstType;
	}

	@SuppressWarnings("unchecked")
	protected abstract Object convertValue(Object srcValue, String dstAccessibleChain);
}
