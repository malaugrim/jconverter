package pl.com.malaugrim.jconverter.core.copymode.deepcopystrategy;

import pl.com.malaugrim.jconverter.core.ObjectInstantiator;

/**
 * This strategy returns {@code null}. It is applicable when source value is {@code null}. Using this strategy 
 * ensures that no NPE is thrown.
 * 
 * @author Lukasz Tutka
 */
public class NullObjectStrategy implements DeepCopyStrategy {

	/**
	 * @return {@code true} when {@code object == null}
	 */
	@Override
	public boolean canHandle(Object object) {
		return object == null;
	}

	/**
	 * @return {@code null}
	 */
	@Override
	public Object deepCopy(Object object) {
		return null;
	}

	@Override
	public void setObjectInstantiator(ObjectInstantiator instantiator) {

	}
}
