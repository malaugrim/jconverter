package pl.com.malaugrim.jconverter.core.annotations;

import pl.com.malaugrim.jconverter.core.TypeConverter;
import pl.com.malaugrim.jconverter.core.accessor.AccessibleObjectWrapper;

import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * Convenient class wrapping result of annotation processing.
 * 
 * @author Lukasz Tutka
 */
public class ProcessingResult {

	private final List<AccessibleObjectWrapper> annotatedAccessibles;
	private final List<AccessibleObjectWrapper> ignored;
	private final List<AccessibleObjectWrapper> toDeepCopy;
	private final List<AccessibleObjectWrapper> toShallowCopy;
	private final Map<AccessibleObjectWrapper, Class<? extends TypeConverter>> toConvert;

	/**
	 * Produces empty result, with each collection emtpy.
	 */
	public static ProcessingResult emptyResult() {
		return new ProcessingResult(Collections.emptyList(), Collections.emptyList(), Collections.emptyMap(), Collections.emptyList(),
				Collections.emptyList());
	}

	public ProcessingResult(List<AccessibleObjectWrapper> annotatedAccessibles, List<AccessibleObjectWrapper> ignored,
							Map<AccessibleObjectWrapper, Class<? extends TypeConverter>> toConvert,
							List<AccessibleObjectWrapper> toDeepCopy, List<AccessibleObjectWrapper> toShallowCopy) {
		this.annotatedAccessibles = annotatedAccessibles;
		this.ignored = ignored;
		this.toConvert = toConvert;
		this.toDeepCopy = toDeepCopy;
		this.toShallowCopy = toShallowCopy;
	}

	/**
	 * Gets all accessibles containing annotations marked with {@link ConverterAnnotation}.
	 */
	public List<AccessibleObjectWrapper> getAnnotatedAccessibles() {
		return annotatedAccessibles;
	}

	/**
	 * Gets all accessibles annotated with {@link Ignore}
	 */
	public List<AccessibleObjectWrapper> getIgnored() {
		return ignored;
	}

	/**
	 * Gets all accessibles annotated with {@link Convert}
	 * 
	 * @return a map where key is {@link AccessibleObjectWrapper}, and value is a {@link TypeConverter}, which should be used for 
	 * converting the accessible 
	 */
	public Map<AccessibleObjectWrapper, Class<? extends TypeConverter>> getToConvert() {
		return toConvert;
	}

	/**
	 * Gets all accessibles annotated with {@link DeepCopy}
	 */
	public List<AccessibleObjectWrapper> getToDeepCopy() {
		return toDeepCopy;
	}

	/**
	 * Gets all accessibles annotated with {@link ShallowCopy}
	 */
	public List<AccessibleObjectWrapper> getToShallowCopy() {
		return toShallowCopy;
	}
}
