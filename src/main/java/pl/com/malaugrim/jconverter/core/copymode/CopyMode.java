package pl.com.malaugrim.jconverter.core.copymode;

import java.util.List;

import pl.com.malaugrim.jconverter.core.ObjectInstantiator;
import pl.com.malaugrim.jconverter.core.accessor.AccessibleObjectWrapper;
import pl.com.malaugrim.jconverter.core.accessor.Accessor;

/**
 * <p>CopyMode is responsible for copying one accessible from source object into another accessible in destination object. The exact mode, in 
 * which this operation is performed, is implementation specific.</p>
 * 
 * <p><b>Accessible</b>: either field or property</p>
 * <p><b>Accessible chain</b> is a dot separated list of accessibles. Basically accessible chain represents a parent-child relationship 
 * between accessibles - an accessible before the dot is said to contain accessbile after the dot.</p>
 * 
 * @author Lukasz Tutka
 */
public interface CopyMode {

	/**
	 * Sets both source and destination objects.
	 */
	void setup(Object srcObj, Object dstObj);

	/**
	 * Extracts value from {@code srcAccessibleChain} in the source object and copies it to property extracted from {@code dstAccessibleChain} 
	 * in the destination object.
	 * 
	 * @throws IllegalStateException if this copy mode is not set up with source and destination objects
	 * @throws IllegalStateException if {@code srcAccessibleChain} is not present in source object or {@code dstAccessibleChain} is not 
	 * present in destination object
	 */
	void copy(String srcAccessibleChain, String dstAccessibleChain);

	/**
	 * Returns all accessibles from the source object. 
	 */
	List<AccessibleObjectWrapper> getSrcAccessibles();

	/**
	 * Returns all accessibles from the destination object.
	 */
	List<AccessibleObjectWrapper> getDstAccessibles();

	/**
	 * Sets object instantiator, which will be used to instantiate class of the destination accessible (if necessary).
	 * 
	 * @see ObjectInstantiator
	 */
	void setObjectInstantiator(ObjectInstantiator instantiator);
}
