package pl.com.malaugrim.jconverter.core.accessor;

import java.lang.annotation.Annotation;
import java.lang.reflect.AccessibleObject;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import com.google.common.base.MoreObjects;

/**
 * <p>A wrapper around {@link AccessibleObject}, which simplifies getting its name, type, annotations and also accessible name. 
 * There are few terms, which need explanation:</p>
 * <ul>
 *     <li>AccessibleObject: see {@link AccessibleObject}</li>
 *     <li>accessible object name: a name of field, method or other accessible object</li>
 *     <li>accessible: this is either field or property represented by this wrapper</li>
 * </ul>
 * 
 * <p>The following example is given to illustrate the difference between accessible name and accessible object name:</p>
 * <pre>
 * public class SomeObject {
 * 	public Object somePublicField;
 * 	private Object somePrivateField;
 *         
 * 	public Object getSomePrivateField() {
 * 		return somePrivateField;
 * 	}
 * }
 * 
 * ...
 * 
 * Class cl = SomeObject.class;
 * AccessibleObjectWrapper w1 = new AccessibleObjectWrapper(cl.getField("somePublicField"));
 * AccessibleObjectWrapper w2 = new AccessibleObjectWrapper(cl.getMethod("getSomePrivateField"));
 * System.out.println("w1.getAccessibleObjectName() = " +w1.getAccessibleObjectName());
 * System.out.println("w1.getAccessibleName() = " +w1.getAccessibleName());
 *
 * System.out.println("\nw2.getAccessibleObjectName() = " +w2.getAccessibleObjectName());
 * System.out.println("w2.getAccessibleName() = " +w2.getAccessibleName());
 * </pre>
 * 
 * <p>The following output will be printed:</p>
 * <pre>
 * w1.getAccessibleObjectName() = somePublicField
 * w1.getAccessibleName() = somePublicField
 * 
 * w2.getAccessibleObjectName() = getSomePrivateField
 * w2.getAccessibleName() = somePrivateField
 * </pre>
 * 
 * @author Lukasz Tutka
 */
public class AccessibleObjectWrapper<T extends AccessibleObject> {

	private final T accessibleObject;
	private String accessibleName;
	private String accessibleObjectName;
	private Class<?> type;

	/**
	 * Creates a wrapper around {@code accessibleObject}
	 */
	public AccessibleObjectWrapper(T accessibleObject) {
		this.accessibleObject = Objects.requireNonNull(accessibleObject);
	}

	/**
	 * Returns wrapped {@code accessibleObject}.
	 */
	public T getAccessibleObject() {
		return accessibleObject;
	}

	/**
	 * Returns the name of the accessible object, which may be field name, method name or other {@link AccessibleObject} name.
	 */
	public String getAccessibleObjectName() {
		if(accessibleObjectName == null) {
			Method[] methods = accessibleObject.getClass().getMethods();
			Optional<Method> optional = Arrays.stream(methods).filter(m -> m.getName().equals("getName")).findFirst();
			Method getNameMethod = optional.orElseThrow(UnsupportedOperationException::new);
			accessibleObjectName = tryInvoke(getNameMethod);
		}

		return accessibleObjectName;
	}

	/**
	 * Returns the name of the accessible, which may be field name or name of the property represented by this wrapper.
	 */
	public String getAccessibleName() {
		if(accessibleName == null) {
			if(accessibleObject instanceof Field) {
				accessibleName = getAccessibleObjectName();
			} else if(accessibleObject instanceof Method) {
				String methodName = getAccessibleObjectName().replaceFirst("(^is)|(^get)|(^set)", "");
				accessibleName = methodName.substring(0, 1).toLowerCase() + methodName.substring(1, methodName.length());
			}
		}

		return accessibleName;
	}

	/**
	 * Returns type of the accessible.
	 */
	public Class<?> getType() {
		if(type == null) {
			determineType();
		}

		return type;
	}

	/**
	 * Gets all annotations that are present on wrapped {@link AccessibleObject}.
	 */
	public List<Annotation> getAnnotations() {
		return Arrays.asList(accessibleObject.getAnnotations());
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this)
				.add("accessible object", accessibleObject instanceof Field ? "Field" : "Property")
				.add("accessibleName", getAccessibleName())
				.add("accessibleObjectName", getAccessibleObjectName())
				.add("type", getType().getCanonicalName())
				.toString();
	}

	private String tryInvoke(Method getNameMethod) {
		try {
			return (String) getNameMethod.invoke(accessibleObject);
		} catch (IllegalAccessException | InvocationTargetException e) {
			throw new RuntimeException(e);
		}
	}

	private void determineType() {
		if (accessibleObject instanceof Field) {
			type = ((Field) accessibleObject).getType();
		} else if (accessibleObject instanceof Method) {
			Method method = (Method) accessibleObject;
			type =  getPropertyType(method);
		} else {
			throw new IllegalStateException("Unsupported accessible object. Consider overriding getType() method");
		}
	}

	private Class<?> getPropertyType(Method method) {
		String name = method.getName();
		if(name.startsWith("set")) {
			return method.getParameterTypes()[0];
		} else {
			return method.getReturnType();
		}
	}

}
