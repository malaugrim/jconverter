package pl.com.malaugrim.jconverter.core.typepromotion.handlers;

import pl.com.malaugrim.jconverter.core.typepromotion.PromotionHandler;

/**
 * @author Lukasz Tutka
 */
public class CharToFloatPromotionHandler implements PromotionHandler<Character, Float> {

	@Override
	public boolean canPromote(Class<?> srcType, Class<?> dstType) {
		return Character.class.equals(srcType) && (float.class.equals(dstType) || Float.class.equals(dstType));
	}

	@Override
	public Float promote(Character src) {
		return (float) src;
	}
}
