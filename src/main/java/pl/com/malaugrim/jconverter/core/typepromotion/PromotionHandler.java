package pl.com.malaugrim.jconverter.core.typepromotion;

/**
 * Handler for promoting {@code S} to {@code D}.
 */
public interface PromotionHandler<S, D> {

	/**
	 * Tests, if promotion of {@code srcType} to {@code dstType} can be performed by this handler. 
	 */
	boolean canPromote(Class<?> srcType, Class<?> dstType);

	/**
	 * Performs promotion. A runtime exception may be thrown when this operation is illegal for the given {@code src} argument.
	 */
	D promote(S src);
}
