package pl.com.malaugrim.jconverter.core.typepromotion.handlers;

import pl.com.malaugrim.jconverter.core.typepromotion.PromotionHandler;

/**
 * @author Lukasz Tutka
 */
public class IntToDoublePromotionHandler implements PromotionHandler<Integer, Double> {

	@Override
	public boolean canPromote(Class<?> srcType, Class<?> dstType) {
		return Integer.class.equals(srcType) && (double.class.equals(dstType) || Double.class.equals(dstType));
	}

	@Override
	public Double promote(Integer src) {
		return (double) src;
	}
}
