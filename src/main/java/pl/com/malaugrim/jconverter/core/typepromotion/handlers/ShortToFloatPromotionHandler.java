package pl.com.malaugrim.jconverter.core.typepromotion.handlers;

import pl.com.malaugrim.jconverter.core.typepromotion.PromotionHandler;

/**
 * @author Lukasz Tutka
 */
public class ShortToFloatPromotionHandler implements PromotionHandler<Short, Float> {

	@Override
	public boolean canPromote(Class<?> srcType, Class<?> dstType) {
		return Short.class.equals(srcType) && (float.class.equals(dstType) || Float.class.equals(dstType));
	}

	@Override
	public Float promote(Short src) {
		return (float) src;
	}
}
