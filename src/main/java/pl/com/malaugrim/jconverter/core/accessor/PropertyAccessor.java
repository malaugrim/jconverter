package pl.com.malaugrim.jconverter.core.accessor;

import com.google.common.base.Preconditions;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Designed to access object properties via getters and setters. This accessor operates on property names rather than method names, thus 
 * you need to pass property name to methods like {@link PropertyAccessor#getValue}, {@link PropertyAccessor#setValue(String, Object)}, 
 * {@link PropertyAccessor#getType(String)} and others. It is assumed that property must have corresponding public getter and setter, 
 * otherwise an exception may be thrown during runtime.
 * 
 * @author Lukasz Tutka
 */
public class PropertyAccessor extends AbstractAccessor {

	private final ClassInfo classInfo;

	PropertyAccessor(Object object) {
		this(object.getClass());
		this.object = object;
	}

	PropertyAccessor(Class<?> objectType) {
		super(objectType);
		this.classInfo = ClassInfoExtractor.getInstance().extract(objectType);
	}

	/**
	 * Returns all public methods of the associated object wrapped in (@link {@link AccessibleObjectWrapper}.
	 * 
	 * @see AccessibleObjectWrapper
	 */
	@Override
	public List<? extends AccessibleObjectWrapper<Method>> getAllAccessibleObjects() {
		return classInfo.getPropertyMethods().stream()
				.map(AccessibleObjectWrapper::new)
				.collect(Collectors.toList());
	}

	/**
	 * Returns value of the given property with the use of public getter.
	 *
	 * @throws IllegalArgumentException when associated object does not contain getter for the given {@code propertyName}
	 */
	@Override
	public Object getValue(String propertyName) {
		throwExceptionIfObjectNotSet();
		Map<String, Method> getters = classInfo.getGetters();
		if(!getters.containsKey(propertyName))
			throw new IllegalArgumentException("No such property: " +propertyName);

		Method method = getters.get(propertyName);
		return tryGetPropertyValue(method);
	}

	/**
	 * Returns type of the given property.
	 *
	 * @throws IllegalArgumentException when there is no getter and no setter for {@code property}
	 */
	@Override
	public Class<?> getType(String property) {
		Map<String, Method> getters = classInfo.getGetters();
		Map<String, Method> setters = classInfo.getSetters();

		if(getters.containsKey(property)) {
			return getters.get(property).getReturnType();
		} else if(setters.containsKey(property)) {
			return setters.get(property).getParameterTypes()[0];
		}

		throw new IllegalArgumentException("No such property: " +property);
	}

	/**
	 * Sets the property {@code propertyName} with the given {@code value}.
	 *
	 * @throws IllegalArgumentException when associated object does not contain setter for the given {@code propertyName}
	 * @throws IllegalStateException when the assignment is illegal (see {@link AbstractAccessor#isAssignable(Class, Class)})
	 */
	@Override
	public void setValue(String propertyName, Object value) {
		throwExceptionIfObjectNotSet();

		Map<String, Method> setters = classInfo.getSetters();
		if(!setters.containsKey(propertyName)) {
			throw new IllegalArgumentException("No such property: " + propertyName);
		}

		Method method = setters.get(propertyName);
		checkIfValueCanBeAssignedToProperty(propertyName, value);
		trySetPropertyValue(method, propertyName, value);
	}

	/**
	 * Tests, whether associated object contains getter for the given property.
	 */
	public boolean containsGetter(String property) {
		return classInfo.getGetters().containsKey(property);
	}

	/**
	 * Tests, whether associated object contains setter for the given property.
	 */
	public boolean containsSetter(String property) {
		return classInfo.getSetters().containsKey(property);
	}

	private Object tryGetPropertyValue(Method method) {
		try {
			return method.invoke(object);
		} catch (IllegalAccessException | InvocationTargetException e) {
			throw new RuntimeException(e);
		}
	}

	private void checkIfValueCanBeAssignedToProperty(String property, Object value) {
		if(value == null) {
			return;
		}

		Class<?> propertyType = getType(property);
		Class<?> valueType = value.getClass();
		boolean assignable = isAssignable(propertyType, valueType);
		Preconditions.checkState(assignable, "Cannot assign " +valueType.getName()+ " to " +propertyType.getName()+ ".");
	}

	private void trySetPropertyValue(Method method, String propertyName, Object value) {
		try {
			Class<?> dstType = getType(propertyName);
			value = promoteIfNecessary(dstType, value);
			value = getDefaultIfValueIsNullAndDstIsPrimitive(dstType, value);
			method.invoke(object, value);
		} catch (IllegalAccessException | InvocationTargetException e) {
			throw new RuntimeException(e);
		}
	}

}
