package pl.com.malaugrim.jconverter.core.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import pl.com.malaugrim.jconverter.objectconverters.AnnotationObjectConverter;
import pl.com.malaugrim.jconverter.core.TypeConverter;

/**
 * <p>Used to mark accessibles, which should be converted.</p>
 * 
 * <p>Annotation can be placed either on field or method. However bare in mind that when placed on a method, the {@link AnnotationObjectConverter}
 * will use setter on a destination object to set converted value. Also note that when {@link Convert#fromFieldChain()} and
 * {@link Convert#fromPropertyChain()} are not defined, {@link AnnotationObjectConverter} will use getter to retrieve the source value.</p>
 *
 * <p><b>Accessible</b>: either field or property</p>
 * 
 * @author Lukasz Tutka
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.FIELD, ElementType.METHOD })
@ConverterAnnotation
public @interface Convert {

	/**
	 * {@link TypeConverter}, which will be used for converting source value.
	 */
	Class<? extends TypeConverter> value();

	/**
	 * (Optional) Points field in the source object, from which the source value will be taken.
	 */
	String fromFieldChain() default "";

	/**
	 * (Optional) Points property in the source object, from which the source value will be taken. Converter will use proper getter to 
	 * retrieve the property value.
	 */
	String fromPropertyChain() default "";
}
