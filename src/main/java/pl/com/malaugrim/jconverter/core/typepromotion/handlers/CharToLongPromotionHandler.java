package pl.com.malaugrim.jconverter.core.typepromotion.handlers;

import pl.com.malaugrim.jconverter.core.typepromotion.PromotionHandler;

/**
 * @author Lukasz Tutka
 */
public class CharToLongPromotionHandler implements PromotionHandler<Character, Long> {


	@Override
	public boolean canPromote(Class<?> srcType, Class<?> dstType) {
		return Character.class.equals(srcType) &&
				(long.class.equals(dstType) || Long.class.equals(dstType));
	}

	@Override
	public Long promote(Character src) {
		return (long) src;
	}
}
