package pl.com.malaugrim.jconverter.core.typepromotion.handlers;

import pl.com.malaugrim.jconverter.core.typepromotion.PromotionHandler;

/**
 * @author Lukasz Tutka
 */
public class ShortToDoublePromotionHandler implements PromotionHandler<Short, Double> {


	@Override
	public boolean canPromote(Class<?> srcType, Class<?> dstType) {
		return Short.class.equals(srcType) && (double.class.equals(dstType) || Double.class.equals(dstType));
	}

	@Override
	public Double promote(Short src) {
		return (double) src;
	}
}
