package pl.com.malaugrim.jconverter.core.typepromotion.handlers;

import pl.com.malaugrim.jconverter.core.typepromotion.PromotionHandler;

/**
 * @author Lukasz Tutka
 */
public class ByteToFloatPromotionHandler implements PromotionHandler<Byte, Float> {

	@Override
	public boolean canPromote(Class<?> srcType, Class<?> dstType) {
		return Byte.class.equals(srcType) && (float.class.equals(dstType) || Float.class.equals(dstType));
	}

	@Override
	public Float promote(Byte src) {
		return (float) src;
	}
}
