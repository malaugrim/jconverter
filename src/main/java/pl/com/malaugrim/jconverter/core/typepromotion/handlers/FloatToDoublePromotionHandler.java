package pl.com.malaugrim.jconverter.core.typepromotion.handlers;

import pl.com.malaugrim.jconverter.core.typepromotion.PromotionHandler;

/**
 * @author Lukasz Tutka
 */
public class FloatToDoublePromotionHandler implements PromotionHandler<Float, Double> {

	@Override
	public boolean canPromote(Class<?> srcType, Class<?> dstType) {
		return Float.class.equals(srcType) && (double.class.equals(dstType) || Double.class.equals(dstType));
	}

	@Override
	public Double promote(Float src) {
		return (double) src;
	}
}
