package pl.com.malaugrim.jconverter.core.annotations;

import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import pl.com.malaugrim.jconverter.core.TypeConverter;
import pl.com.malaugrim.jconverter.core.accessor.AccessibleObjectWrapper;
import pl.com.malaugrim.jconverter.core.accessor.Accessor;
import pl.com.malaugrim.jconverter.core.accessor.AccessorFactory;

/**
 * 
 * @author Lukasz Tutka
 */
public class AnnotationProcessor {

	private static final String MISSING_CONVERTER_TYPE = "Convert annotation must have converter type defined";

	public ProcessingResult process(Object object) {
		if(object == null) {
			return ProcessingResult.emptyResult();
		}

		return proceedWithProcessing(object);
	}

	private ProcessingResult proceedWithProcessing(Object object) {
		List<AccessibleObjectWrapper> annotated = getAllAnnotatedAccessibles(object);
		List<AccessibleObjectWrapper> ignored = getIgnored(annotated, object);
		Map<AccessibleObjectWrapper, Class<? extends TypeConverter>> toConvert = getToConvert(annotated);
		List<AccessibleObjectWrapper> toDeepCopy = getToDeepCopy(annotated);
		List<AccessibleObjectWrapper> toShallowCopy = getToShallowCopy(annotated);

		return new ProcessingResult(annotated, ignored, toConvert, toDeepCopy, toShallowCopy);
	}

	private List<AccessibleObjectWrapper> getToDeepCopy(List<AccessibleObjectWrapper> all) {
		return all.stream().filter(this::hasDeepCopyAnnotation).collect(Collectors.toList());
	}

	private Map<AccessibleObjectWrapper, Class<? extends TypeConverter>> getToConvert(List<AccessibleObjectWrapper> all) {
		return all.stream().filter(this::hasConvertAnnotation)
				.collect(Collectors.toMap(accessibleObjectWrapper -> accessibleObjectWrapper, this::getConverterType));
	}

	private List<AccessibleObjectWrapper> getIgnored(List<AccessibleObjectWrapper> all, Object object) {
		List<AccessibleObjectWrapper> toIgnore = new ArrayList<>();
		for (AccessibleObjectWrapper accessible : all) {
			addIfHasIgnoreAnnotation(toIgnore, accessible, object);
		}

		return toIgnore;
	}

	private void addIfHasIgnoreAnnotation(List<AccessibleObjectWrapper> toIgnore, AccessibleObjectWrapper<?> accessible, Object object) {
		getIgnoreAnnotation(accessible).ifPresent(ignore -> {
			String correspondingField = ignore.value();
			if(!correspondingField.isEmpty()) {
				AccessibleObjectWrapper<?> correspondingFieldAccessible = getCorrespondingFieldAccessible(correspondingField, object);
				toIgnore.add(correspondingFieldAccessible);
			}

			toIgnore.add(accessible);
		});
	}

	private AccessibleObjectWrapper<?> getCorrespondingFieldAccessible(String name, Object object) {
		Accessor fieldAccessor = new AccessorFactory().getFieldAccessor(object);
		return fieldAccessor.getAllAccessibleObjects().stream().filter(accessible -> accessible.getAccessibleName().equals(name))
				.findFirst().orElseThrow(() -> new IllegalStateException("Ignore annotation points to non existing field"));
	}

	private Optional<Ignore> getIgnoreAnnotation(AccessibleObjectWrapper<?> accessible) {
		Optional<Annotation> opt = accessible.getAnnotations().stream().filter(annotation -> annotation.annotationType().equals(Ignore.class))
				.findFirst();
		return opt.isPresent() ? Optional.of((Ignore) opt.get()) : Optional.empty();
	}

	private Class<? extends TypeConverter> getConverterType(AccessibleObjectWrapper<?> accessible) {
		return accessible.getAnnotations().stream().map((annotation) -> ((Convert)annotation).value()).findFirst()
				.orElseThrow(() -> new IllegalStateException(MISSING_CONVERTER_TYPE));
	}

	@SuppressWarnings("unchecked")
	private List<AccessibleObjectWrapper> getAllAnnotatedAccessibles(Object object) {
		AccessorFactory accessorFactory = new AccessorFactory();
		List<? extends AccessibleObjectWrapper> fields = accessorFactory.getFieldAccessor(object).getAllAccessibleObjects();
		List<? extends AccessibleObjectWrapper> methods = accessorFactory.getPropertyAccessor(object).getAllAccessibleObjects();
		return Stream.concat(fields.stream(), methods.stream())
				.filter(this::hasConverterAnnotation).collect(Collectors.toList());
	}

	private boolean hasConverterAnnotation(AccessibleObjectWrapper<?> accessible) {
		List<Annotation> annotations = accessible.getAnnotations();
		return annotations.stream().filter(annotation -> annotation.annotationType().getAnnotation(ConverterAnnotation.class) != null)
				.findAny().isPresent();
	}

	private boolean hasConvertAnnotation(AccessibleObjectWrapper<?> accessible) {
		return isAnnotationPresent(accessible, Convert.class);
	}

	private boolean hasDeepCopyAnnotation(AccessibleObjectWrapper<?> accessible) {
		return isAnnotationPresent(accessible, DeepCopy.class);
	}

	private List<AccessibleObjectWrapper> getToShallowCopy(List<AccessibleObjectWrapper> accessibles) {
		List<AccessibleObjectWrapper> result = accessibles.stream().filter(this::hasShallowCopyAnnotation).collect(Collectors.toList());
		result.forEach(this::validateShallowCopyAnnotation);
		return result;
	}

	private void validateShallowCopyAnnotation(AccessibleObjectWrapper<?> accessible) {
		ShallowCopy annotation = accessible.getAccessibleObject().getAnnotation(ShallowCopy.class);
		if(!annotation.fromFieldChain().isEmpty() && !annotation.fromPropertyChain().isEmpty()) {
			throw new IllegalStateException("ShallowCopy annotation cannot point source field and property and the same time");
		}
	}

	private boolean hasShallowCopyAnnotation(AccessibleObjectWrapper<?> accessible) {
		return isAnnotationPresent(accessible, ShallowCopy.class);
	}

	private boolean isAnnotationPresent(AccessibleObjectWrapper<?> accessible, Class<? extends Annotation> annotation) {
		List<Annotation> annotations = accessible.getAnnotations();
		return annotations.stream().map(Annotation::annotationType).filter(type -> type.equals(annotation)).findAny().isPresent();
	}

}
