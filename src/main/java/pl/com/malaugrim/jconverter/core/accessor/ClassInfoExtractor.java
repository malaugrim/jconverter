package pl.com.malaugrim.jconverter.core.accessor;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.Lists;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * @author Lukasz Tutka
 */
public enum ClassInfoExtractor {

	INSTANCE;

	private static final String ACCESSOR_PREFIXES_REGEX = "(^get)|(^set)|(^is)";
	private static  final List<String> IGNORED_ACCESSIBLES = Lists.newArrayList("getClass", "serialVersionUID");
	private static final int MINUTES_TO_CACHE_EXPIRE = 60;
	private LoadingCache<Class<?>, ClassInfo> classInfoCache;

	public static ClassInfoExtractor getInstance() {
		return INSTANCE;
	}

	ClassInfoExtractor() {
		initCache();
	}

	public ClassInfo extract(Class<?> type) {
		try {
			return classInfoCache.get(type);
		} catch (ExecutionException e) {
			throw new RuntimeException(e);
		}
	}

	private void initCache() {
		classInfoCache = CacheBuilder.newBuilder().weakKeys().softValues().expireAfterAccess(MINUTES_TO_CACHE_EXPIRE, TimeUnit.MINUTES)
				.build(new CacheLoader<Class<?>, ClassInfo>() {
					@Override
					public ClassInfo load(Class<?> type) throws Exception {
						return extractClassInfo(type);
					}
				});
	}

	private ClassInfo extractClassInfo(Class<?> type) {
		Map<String, Field> fields = extractFields(type);
		List<Method> methods = extractPropertyMethods(type);
		Map<String, Boolean> fieldsDefaultAccessibility = getFieldsDefaultAccessibility(fields.values());

		return buildClassInfo(type, fields, fieldsDefaultAccessibility, methods);
	}

	private Map<String, Field> extractFields(Class<?> objectType) {
		List<Field> allFields = getAllFields(objectType);
		return allFields.stream()
				.filter(f -> !isAlwaysIgnored(f.getName()))
				.collect(Collectors.toMap(Field::getName, f -> f));
	}

	private List<Field> getAllFields(Class<?> objectType) {
		List<Field> fields = new ArrayList<>();
		Set<String> addedFields = new HashSet<>();

		for(Class<?> clazz = objectType; clazz != null; clazz = clazz.getSuperclass()) {
			List<Field> fieldsInClass = Arrays.asList(clazz.getDeclaredFields());
			List<Field> filtered = fieldsInClass.stream().filter(excludeSuperFieldIfOverridden(addedFields))
					.collect(Collectors.toList());

			fieldsInClass.forEach(m -> addedFields.add(m.getName()));
			fields.addAll(filtered);
		}
		return fields;
	}

	private Predicate<Field> excludeSuperFieldIfOverridden(Set<String> addedFields) {
		return field -> !addedFields.contains(field.getName());
	}

	private ClassInfo buildClassInfo(Class<?> type, Map<String, Field> fields, Map<String, Boolean> fieldsDefaultAccessibility,
									 List<Method> methods) {
		ClassInfo classInfo = new ClassInfo();
		classInfo.setType(type);
		classInfo.setFields(fields);
		classInfo.setPropertyMethods(methods);
		classInfo.setFieldsDefaultAccessibility(fieldsDefaultAccessibility);
		classInfo.setGetters(filterGetters(methods));
		classInfo.setSetters(filterSetters(methods));
		return classInfo;
	}

	private List<Method> extractPropertyMethods(Class<?> objectType) {
		List<Method> methods = Arrays.asList(objectType.getMethods());
		return methods.stream().filter(this::isGetterOrSetter)
				.filter(m -> !isAlwaysIgnored(m.getName()))
				.collect(Collectors.toList());
	}

	private boolean isGetterOrSetter(Method method) {
		String pattern = "(" + ACCESSOR_PREFIXES_REGEX + ").*";
		String methodName = method.getName();
		return methodName.matches(pattern);
	}

	private boolean isAlwaysIgnored(String accessibleName) {
		return IGNORED_ACCESSIBLES.contains(accessibleName);
	}

	private Map<String, Boolean> getFieldsDefaultAccessibility(Collection<Field> fields) {
		return fields.stream().collect(Collectors.toMap(Field::getName, Field::isAccessible));
	}

	private Map<String, Method> filterGetters(List<Method> methods) {
		return filterMethods(methods, this::isGetter);
	}

	private boolean isGetter(Method method) {
		String name = method.getName();
		return name.startsWith("is") || name.startsWith("get");
	}

	private Map<String, Method> filterSetters(List<Method> methods) {
		return filterMethods(methods, this::isSetter);
	}

	private boolean isSetter(Method method) {
		return method.getName().startsWith("set");
	}

	private Map<String, Method> filterMethods(List<Method> methods, Predicate<Method> predicate) {
		return methods.stream().filter(predicate).collect(Collectors.toMap(this::getPropertyName, method -> method));
	}

	private String getPropertyName(Method method) {
		String baseName = method.getName().replaceFirst(ACCESSOR_PREFIXES_REGEX, "");
		return baseName.substring(0, 1).toLowerCase() + baseName.substring(1, baseName.length());
	}
}
