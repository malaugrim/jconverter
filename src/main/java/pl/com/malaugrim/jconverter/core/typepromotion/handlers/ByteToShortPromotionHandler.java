package pl.com.malaugrim.jconverter.core.typepromotion.handlers;

import pl.com.malaugrim.jconverter.core.typepromotion.PromotionHandler;

/**
 * @author Lukasz Tutka
 */
public class ByteToShortPromotionHandler implements PromotionHandler<Byte, Short> {

	@Override
	public boolean canPromote(Class<?> srcType, Class<?> dstType) {
		return Byte.class.equals(srcType) && (short.class.equals(dstType) || Short.class.equals(dstType) );
	}

	@Override
	public Short promote(Byte src) {
		return (short) src;
	}
}
