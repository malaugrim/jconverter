package pl.com.malaugrim.jconverter.core.typepromotion.handlers;

import pl.com.malaugrim.jconverter.core.typepromotion.PromotionHandler;

/**
 * @author Lukasz Tutka
 */
public class ByteToIntPromotionHandler implements PromotionHandler<Byte, Integer> {

	@Override
	public boolean canPromote(Class<?> srcType, Class<?> dstType) {
		return Byte.class.equals(srcType) && (int.class.equals(dstType) || Integer.class.equals(dstType));
	}

	@Override
	public Integer promote(Byte src) {
		return (int) src;
	}
}
