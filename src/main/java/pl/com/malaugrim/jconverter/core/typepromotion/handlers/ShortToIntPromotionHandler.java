package pl.com.malaugrim.jconverter.core.typepromotion.handlers;

import pl.com.malaugrim.jconverter.core.typepromotion.PromotionHandler;

/**
 * @author Lukasz Tutka
 */
public class ShortToIntPromotionHandler implements PromotionHandler<Short, Integer> {

	@Override
	public boolean canPromote(Class<?> srcType, Class<?> dstType) {
		return Short.class.equals(srcType) && (int.class.equals(dstType) || Integer.class.equals(dstType));
	}

	@Override
	public Integer promote(Short src) {
		return (int) src;
	}
}
