package pl.com.malaugrim.jconverter.core.typepromotion;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.function.Supplier;

/**
 * Class responsible for promoting java types. It delegates the promotion to the first capable {@link PromotionHandler} it finds.
 * Throws {@link IllegalStateException} when no handler is able to do the promoting, thus use 
 * {@link TypePromotor#canPromote(Class, Class)} beforehand.
 * 
 * @author Lukasz Tutka
 */
@SuppressWarnings("unused")
public class TypePromotor {
	
	private List<PromotionHandler> handlers = new ArrayList<>();


	public TypePromotor() {
	}

	public TypePromotor(List<PromotionHandler> handlers) {
		this.handlers = handlers;
	}

	public void setHandlers(List<PromotionHandler> handlers) {
		this.handlers = handlers;
	}

	public boolean canPromote(Class<?> srcType, Class<?> dstType) {
		return handlers.stream().filter(getPromotionHandlerPredicate(srcType, dstType)).findFirst().isPresent();
	}

	/**
	 * Promotes {@code src} type to desired {@code dstType}. Actual promotion is delegated to the capable {@link PromotionHandler}.
	 * 
	 * @throws IllegalStateException if there is no {@link PromotionHandler} capable of promoting {@code src} value
	 */
	@SuppressWarnings("unchecked")
	public <T> T promote(Object src, Class<T> dstType) {
		PromotionHandler promotionHandler = handlers.stream().filter(getPromotionHandlerPredicate(src.getClass(), dstType)).findFirst()
				.orElseThrow(cannotPromoteException(src, dstType));
		
		return (T) promotionHandler.promote(src);
	}

	@SuppressWarnings("unchecked")
	private Predicate<PromotionHandler> getPromotionHandlerPredicate(Class<?> srcType, Class<?> dstType) {
		return handler -> handler.canPromote(srcType, dstType);
	}

	private <T> Supplier<IllegalStateException> cannotPromoteException(Object src, Class<T> dstType) {
		return () -> new IllegalStateException(String.format("Cannot promote %s to %s", src.getClass().getName(), dstType.getName()));
	}
}
