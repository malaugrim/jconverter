package pl.com.malaugrim.jconverter.core.copymode;

import java.util.List;
import java.util.Objects;

import com.google.common.base.Preconditions;

import pl.com.malaugrim.jconverter.core.ObjectInstantiator;
import pl.com.malaugrim.jconverter.core.accessor.AccessibleObjectWrapper;
import pl.com.malaugrim.jconverter.core.accessor.Accessor;
import pl.com.malaugrim.jconverter.core.accessor.AccessorFactory;
import pl.com.malaugrim.jconverter.core.accessor.FieldAccessor;
import pl.com.malaugrim.jconverter.core.accessor.PropertyAccessor;
import pl.com.malaugrim.jconverter.core.copymode.validators.AccessibleValidator;
import pl.com.malaugrim.jconverter.core.copymode.validators.ValidatorFactory;

/**
 * Contains functionality common for each copy mode, like setting up source and destination objects, validating copy arguments and 
 * maintaining information on how to retrieve source and destination accessbiles. {@code AbstractCopyMode} utilizes 
 * {@link AccessorFactory} to create proper {@link Accessor accessors}.
 * 
 * <p><b>Accessible</b>: either field or property</p>
 * <p><b>Accessible chain</b> is a dot separated list of accessibles. Basically accessible chain represents a parent-child relationship 
 * between accessibles - an accessible before the dot is said to contain accessbile after the dot. How exactly the chain is used to get and 
 * set accessibles - is dependent on provided {@link Accessor accessor classes}. For instance a {@link FieldAccessor} will use
 * reflection to get/set correspondent fields, while {@link PropertyAccessor} will use public getters/setters to get or set a property.
 * 
 * <p>Example: </p>
 * Given the following chain: someValue.otherValue:
 * <ul>
 *     <li>A source accessor of type {@link FieldAccessor} will use reflective calls to get field 'otherValue', which is contained in 
 *     object represented by field 'someValue', which in turn is contained in the source object</li>
 *     <li>A source accessor of type {@link PropertyAccessor} will use following method calls to get the value: 
 *     srcObject.getSomeValue().getOtherValue()</li>
 * </ul>
 * </p>
 * 
 * @param <S> accessor used for accessing source accessibles
 * @param <D> accessor used for accessing destination accessibles
 * 
 * @author Lukasz Tutka
 */
public abstract class AbstractCopyMode<S extends Accessor, D extends Accessor> implements CopyMode {

	private static final String NULL_VALIDATOR = "Accessible validators must be provided";
	private static final String NOT_SETUP_MSG = "Copy mode needs to be setup with source and destination objects";
	private static final String SRC_OBJECT_NULL_MSG = "Source object is null";
	private static final String DST_OBJECT_NULL_MSG = "Destination object is null";
	private static final AccessorFactory ACCESSOR_FACTORY = new AccessorFactory();

	/**
	 * {@link Accessor} used to access accessibles in source object
	 */
	protected S srcAccessor;

	/**
	 * {@link Accessor} used to access accessibles in destination object
	 */
	protected D dstAccessor;

	final Class<S> srcAccessorType;
	final Class<D> dstAccessorType;
	final AccessibleValidator srcValidator;
	final AccessibleValidator dstValidator;
	
	private ObjectInstantiator instantiator = new ObjectInstantiator();


	/**
	 * Creates copy mode
	 * 
	 * @param srcAccessorType type of the {@link Accessor}, which will be used to access accessibles in source object
	 * @param dstAccessorType type of the {@link Accessor}, which will be used to access accessibles in destination object
	 * @param srcValidator validator used for validating source accessible
	 * @param dstValidator validator used for validating destination accessible
	 */
	protected AbstractCopyMode(Class<S> srcAccessorType, Class<D> dstAccessorType, AccessibleValidator srcValidator, 
							   AccessibleValidator dstValidator) {
		this.srcAccessorType = srcAccessorType;
		this.dstAccessorType = dstAccessorType;
		this.srcValidator = srcValidator;
		this.dstValidator = dstValidator;
	}

	/**
	 * Creates copy mode
	 *
	 * @param srcAccessorType type of the {@link Accessor}, which will be used to access accessibles in source object
	 * @param dstAccessorType type of the {@link Accessor}, which will be used to access accessibles in destination object
	 */
	protected AbstractCopyMode(Class<S> srcAccessorType, Class<D> dstAccessorType) {
		this.srcAccessorType = srcAccessorType;
		this.dstAccessorType = dstAccessorType;
		ValidatorFactory validatorFactory = new ValidatorFactory();
		this.srcValidator = validatorFactory.getSrcValidator(srcAccessorType);
		this.dstValidator = validatorFactory.getDstValidator(dstAccessorType);
	}


	/**
	 * First checks if this copy mode is set up with source and destination objects, next validates copied accessibles and afterwards 
	 * performs the actual copying via {@link AbstractCopyMode#performCopy(String, String)}.
	 * 
	 * @throws IllegalStateException if this copy mode is not set up with source and destination objects
	 * @throws IllegalStateException if source accessible extracted from {@code srcAccessibleChain} is not present in source object or 
	 * the destination accessible extracted from {@code dstAccessibleChain} is not present in destination object
	 */
	@Override
	public void copy(String srcAccessibleChain, String dstAccessibleChain) {
		throwExceptionIfNotSetup();
		validateArguments(srcAccessibleChain, dstAccessibleChain);
		performCopy(srcAccessibleChain, dstAccessibleChain);
	}

	/**
	 * Sets both source and destination objects, and creates proper {@link Accessor accessors} for them with the use of 
	 * {@link AccessorFactory}.
	 */
	@Override
	public void setup(Object srcObj, Object dstObj) {
		checkNotNull(srcObj, dstObj);
		srcAccessor = ACCESSOR_FACTORY.getAccessor(srcAccessorType, srcObj);
		dstAccessor = ACCESSOR_FACTORY.getAccessor(dstAccessorType, dstObj);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<AccessibleObjectWrapper> getSrcAccessibles() {
		return (List<AccessibleObjectWrapper>) srcAccessor.getAllAccessibleObjects();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<AccessibleObjectWrapper> getDstAccessibles() {
		return (List<AccessibleObjectWrapper>) dstAccessor.getAllAccessibleObjects();
	}

	@Override
	public void setObjectInstantiator(ObjectInstantiator instantiator) {
		this.instantiator = Objects.requireNonNull(instantiator);
	}

	protected ObjectInstantiator getObjectInstantiator() {
		return instantiator;
	}

	/**
	 * Extracts source accessible from {@code srcAccessibleChain} in the source object and sets its value in the destination accessible 
	 * extracted from {@code dstAccessibleChain} in the destination object.
	 */
	protected abstract void performCopy(String srcAccessibleChain, String dstAccessibleChain);

	/**
	 * Validates, if accessibles extracted from src and dst chains exist in their corresponding objects.
	 */
	@SuppressWarnings("unchecked")
	protected void validateArguments(String srcAccessibleChain, String dstAccessibleChain) {
		Class<?> srcObjectType = srcAccessor.getObject().getClass();
		Class<?> dstObjectType = dstAccessor.getObject().getClass();
		Objects.requireNonNull(srcValidator, NULL_VALIDATOR).validate(srcObjectType, srcAccessibleChain);
		Objects.requireNonNull(dstValidator, NULL_VALIDATOR).validate(dstObjectType, dstAccessibleChain);
	}

	/**
	 * Extracts value from {@code srcAccessibleChain} in the source object.
	 */
	protected Object getSrcValue(String srcAccessibleChain) {
		return getValue(srcAccessor, srcAccessibleChain);
	}

	/**
	 * Extracts value from {@code dstAccessibleChain} in the source object.
	 */
	protected Object getDstValue(String dstAccessibleChain) {
		return getValue(dstAccessor, dstAccessibleChain);
	}
	
	/**
	 * Sets accessible extracted from {@code dstAccessibleChain} with the given value in destination object.
	 */
	protected void setValue(String dstAccessibleChain, Object value) {
		Accessor accessor = dstAccessor;
		String accessible = null;
		String[] accessibles = dstAccessibleChain.split("\\.");
		for (int i = 0, size = accessibles.length; i < size; i++) {
			accessible = accessibles[i];
			
			if(i < size - 1) {
				Object obj = accessor.getValue(accessible);
				if(obj == null) {
					obj = instantiator.createInstance(accessor.getType(accessible));
					accessor.setValue(accessible, obj);
				}

				accessor = ACCESSOR_FACTORY.getAccessor(accessor.getClass(), obj);
			}
		}


		accessor.setValue(accessible, value);
	}

	private Object getValue(Accessor accessor, String accessibleChain) {
		Object value = null;
		String[] accessibles = accessibleChain.split("\\.");
		for (int i = 0, size = accessibles.length; i < size; i++) {
			String accessible = accessibles[i];
			value = accessor.getValue(accessible);

			if(value == null) {
				return null;
			}

			if(i < size - 1) {
				accessor = ACCESSOR_FACTORY.getAccessor(accessor.getClass(), value);
			}
		}

		return value;
	}

	private void throwExceptionIfNotSetup() {
		if(!isSetup())
			throw new IllegalStateException(NOT_SETUP_MSG);
	}

	private void checkNotNull(Object srcObj, Object dstObj) {
		Preconditions.checkNotNull(srcObj, SRC_OBJECT_NULL_MSG);
		Preconditions.checkNotNull(dstObj, DST_OBJECT_NULL_MSG);
	}

	private boolean isSetup() {
		return srcAccessor != null && dstAccessor != null;
	}
}
