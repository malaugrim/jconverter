package pl.com.malaugrim.jconverter.core.copymode.validators;

import java.lang.reflect.Method;
import java.util.Map;

import pl.com.malaugrim.jconverter.core.accessor.*;

/**
 * <p>Checks if the property exist in the destination object.</p>
 *
 * <p><b>Accessible</b>: either field or property</p>
 * <p><b>Accessible chain</b> is a dot separated list of accessibles. Basically accessible chain represents a parent-child relationship 
 * between accessibles - an accessible before the dot is said to contain accessbile after the dot. How exactly the chain is used to get and 
 * set accessibles - is dependent on provided {@link Accessor accessor classes}. For instance a {@link FieldAccessor} will use
 * reflection to get/set correspondent fields, while {@link PropertyAccessor} will use public getters/setters to get or set a property.
 *
 * <p>Example: </p>
 * Given the following chain: someValue.otherValue:
 * <ul>
 *     <li>A source accessor of type {@link FieldAccessor} will use reflective calls to get field 'otherValue', which is contained in 
 *     object represented by field 'someValue', which in turn is contained in the source object</li>
 *     <li>A source accessor of type {@link PropertyAccessor} will use following method calls to get the value: 
 *     srcObject.getSomeValue().getOtherValue()</li>
 * </ul>
 * </p>
 * 
 * @author Lukasz Tutka
 */
public class DestinationPropertyValidator extends AbstractAccessibleValidator {

	/**
	 * Checks if {@code objectType} contains proper setters for accessibles represented by the given {@code propertyChain}.
	 * 
	 * @throws IllegalStateException when validation fails
	 */
	@Override
	public void validate(Class<?> objectType, String propertyChain) {
		Class<?> type = objectType;
		String[] properties = propertyChain.split("\\.");
		for (String property : properties) {
			ClassInfo classInfo = ClassInfoExtractor.getInstance().extract(type);
			Map<String, Method> setters = classInfo.getSetters();

			if(!setters.containsKey(property)) {
				throw createNoSuchAccessibleException(DST_PREFIX, property, PROPERTY_ACCESSIBLE_TYPE);
			}

			type = setters.get(property).getParameterTypes()[0];
		}
	}
}
