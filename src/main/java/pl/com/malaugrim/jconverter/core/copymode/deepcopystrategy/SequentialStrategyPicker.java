package pl.com.malaugrim.jconverter.core.copymode.deepcopystrategy;

import java.util.*;

import pl.com.malaugrim.jconverter.core.ObjectInstantiator;

/**
 * This strategy picker calls {@link DeepCopyStrategy#canHandle(Object)} and picks the first strategy that returns {@code true}.
 * 
 * @author Lukasz Tutka
 */
public class SequentialStrategyPicker implements DeepCopyStrategyPicker {

	private static final String CANT_FIND_STRATEGY_MSG_PATTERN = "Cannot find deep copy strategy for copying object [%s]. Either check if provided " +
			"strategies are sufficient, provide proper instance supplier via copyMode.setObjectInstantiator() method, or just set " +
			"'shallowCopyWhenCannotFindStrategy' flag to true.";
	
	private List<DeepCopyStrategy> strategies = new ArrayList<>();
	private boolean shallowCopyWhenCannotFindStrategy = true;

	@Override
	public void setStrategies(List<DeepCopyStrategy> strategies) {
		this.strategies = strategies;
	}
	
	@Override
	public DeepCopyStrategy findStrategy(Object object) {
		for (DeepCopyStrategy strategy : strategies) {
			if(strategy.canHandle(object))
				return strategy;
		}

		return throwExceptionOrReturnShallowCopyStrategy(object);
	}

	@Override
	public void setShallowCopyWhenCannotFindStrategy(boolean shallowCopyWhenCannotFindStrategy) {
		this.shallowCopyWhenCannotFindStrategy = shallowCopyWhenCannotFindStrategy;
	}

	@Override
	public void setObjectInstantiator(ObjectInstantiator objectInstantiator) {
		strategies.forEach(s -> s.setObjectInstantiator(objectInstantiator));
	}

	private DeepCopyStrategy throwExceptionOrReturnShallowCopyStrategy(Object object) {
		if(shallowCopyWhenCannotFindStrategy) {
			return new ShallowCopyStrategy();
		} else {
			throw new IllegalStateException(String.format(CANT_FIND_STRATEGY_MSG_PATTERN, object.getClass().getName()));
		}
	}
}
