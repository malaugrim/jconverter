package pl.com.malaugrim.jconverter.core.typepromotion.handlers;

import pl.com.malaugrim.jconverter.core.typepromotion.PromotionHandler;

/**
 * @author Lukasz Tutka
 */
public class ByteToDoublePromotionHandler implements PromotionHandler<Byte, Double> {

	@Override
	public boolean canPromote(Class<?> srcType, Class<?> dstType) {
		return Byte.class.equals(srcType) && (double.class.equals(dstType) || Double.class.equals(dstType));
	}

	@Override
	public Double promote(Byte src) {
		return (double) src;
	}
}
