package pl.com.malaugrim.jconverter.core.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import pl.com.malaugrim.jconverter.objectconverters.AnnotationObjectConverter;
import pl.com.malaugrim.jconverter.core.copymode.ShallowCopyMode;

/**
 * <p>Used to mark accessibles, which should be shallow copied either from pointed field or property. Note that there is no need to use 
 * this annotation when source and destination fields have the same name. {@link AnnotationObjectConverter} uses {@link ShallowCopyMode}
 * by default on each not ignored field.</p>
 *
 * <p>Annotation can be placed either on field or method. However bare in mind that when placed on a method, the {@link AnnotationObjectConverter} 
 * will use setter on a destination object to set copied value. Also note that when {@link ShallowCopy#fromFieldChain()} and
 * {@link ShallowCopy#fromPropertyChain()} are not defined, {@link AnnotationObjectConverter} will use getter to retrieve source value.</p>
 *
 * <p><b>Accessible</b>: either field or property</p>
 * 
 * @author Lukasz Tutka
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.FIELD, ElementType.METHOD })
@ConverterAnnotation
public @interface ShallowCopy {

	/**
	 * (Optional) Points field in the source object, from which the source value will be taken.
	 */
	String fromFieldChain() default "";

	/**
	 * (Optional) Points property in the source object, from which the source value will be taken. Converter will use proper getter to 
	 * retrieve the property value.
	 */
	String fromPropertyChain() default "";
}
