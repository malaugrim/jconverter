package pl.com.malaugrim.jconverter.core.accessor;

import com.google.common.base.Preconditions;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Designed to access fields in the given object.
 * 
 * @author Lukasz Tutka
 */
public class FieldAccessor extends AbstractAccessor {

	private final ClassInfo classInfo;

	FieldAccessor(Object object) {
		this(object.getClass());
		this.object = object;
	}

	FieldAccessor(Class<?> objectType) {
		super(objectType);
		classInfo = ClassInfoExtractor.getInstance().extract(objectType);
	}

	/**
	 * Retrieves all fields of the associated object wrapped in {@link AccessibleObjectWrapper}.
	 *
	 * @see AccessibleObjectWrapper
	 */
	@Override
	public List<? extends AccessibleObjectWrapper<Field>> getAllAccessibleObjects() {
		return classInfo.getFields().values().stream().map(AccessibleObjectWrapper::new).collect(Collectors.toList());
	}

	/**
	 * Returns value of the given field.
	 *
	 * @throws IllegalArgumentException when associated object does not contain the given field
	 */
	@Override
	public Object getValue(String field) {
		throwExceptionIfObjectNotSet();
		setFieldAccessIfWasntPublicByDefault(field, true);
		Object value = tryGetFieldValue(field);
		setFieldAccessIfWasntPublicByDefault(field, false);
		return value;
	}

	/**
	 * Assigns {@code value} to the given field. Java type promotion is used when necessary. If the source value
	 * is null and the destination type is a primitive, then the default value is assigned for this type.
	 *
	 * @throws IllegalArgumentException when associated object does not contain the given field
	 * @throws IllegalStateException when the assignment is illegal (see {@link AbstractAccessor#isAssignable(Class, Class)})
	 */
	@Override
	public void setValue(String field, Object value) {
		throwExceptionIfObjectNotSet();
		setFieldAccessIfWasntPublicByDefault(field, true);
		checkIfValueCanBeAssignedToField(field, value);
		trySetFieldValue(field, value);
		setFieldAccessIfWasntPublicByDefault(field, false);
	}


	/**
	 * Retrieves {@link Field} with the given name {@code field}
	 *
	 * @throws IllegalArgumentException when associated object does not contain the given field
	 */
	public Field getField(String field) {
		return Optional.ofNullable(classInfo.getFields().get(field)).orElseThrow(() -> new IllegalArgumentException("No such field: " +field));
	}

	/**
	 * Gets type of the given field.
	 *
	 * @throws IllegalArgumentException when associated object does not contain the given field
	 */
	@Override
	public Class<?> getType(String field) {
		return getField(field).getType();
	}

	/**
	 * Tests, if associated object contains field with name {@code field}.
	 */
	@SuppressWarnings("BooleanMethodIsAlwaysInverted")
	public boolean contains(String field) {
		return classInfo.getFields().containsKey(field);
	}

	private void setFieldAccessIfWasntPublicByDefault(String field, boolean accessible) {
		if(!isFieldAccessibleByDefault(field)) {
			classInfo.getFields().get(field).setAccessible(accessible);
		}
	}

	private boolean isFieldAccessibleByDefault(String field) {
		return classInfo.isFieldAccessibleByDefault(field);
	}

	private Object tryGetFieldValue(String field) {
		try {
			return getField(field).get(object);
		} catch (IllegalAccessException e) {
			throw new RuntimeException(e);
		}
	}

	private void checkIfValueCanBeAssignedToField(String field, Object value) {
		if (value == null)
			return;

		Class<?> fieldType = getType(field);
		Class<?> valueType = value.getClass();
		boolean assignable = isAssignable(fieldType, valueType);
		Preconditions.checkState(assignable, "Cannot assign " +valueType.getName()+ " to " +fieldType.getName()+ ".");
	}

	private void trySetFieldValue(String field, Object value) {
		try {
			Class<?> dstType = getType(field);
			value = promoteIfNecessary(dstType, value);
			value = getDefaultIfValueIsNullAndDstIsPrimitive(dstType, value);
			getField(field).set(object, value);
		} catch (IllegalAccessException e) {
			throw new RuntimeException(e);
		}
	}
}
