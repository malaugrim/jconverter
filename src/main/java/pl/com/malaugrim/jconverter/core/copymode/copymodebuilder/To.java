package pl.com.malaugrim.jconverter.core.copymode.copymodebuilder;

import pl.com.malaugrim.jconverter.core.accessor.Accessor;
import pl.com.malaugrim.jconverter.core.accessor.FieldAccessor;
import pl.com.malaugrim.jconverter.core.accessor.PropertyAccessor;

/**
 * Allows to select a way for setting accessbiles in the destination object.
 *
 * @author Lukasz Tutka
 */
public class To {

	private CopyModeBuilder copyModeBuilder;

	To(CopyModeBuilder copyModeBuilder) {
		this.copyModeBuilder = copyModeBuilder;
	}

	/**
	 * Tells the {@link CopyModeBuilder} to create a copy mode, which will set copied values to fields of the destination 
	 * object via reflection.
	 */
	public CopyModeBuilder toField() {
		copyModeBuilder.dstAccessorType = FieldAccessor.class;
		return copyModeBuilder;
	}

	/**
	 * Tells the {@link CopyModeBuilder} to create a copy mode, which will set copied values to properties of the destination 
	 * object via public setters.
	 */
	public CopyModeBuilder toProperty() {
		copyModeBuilder.dstAccessorType = PropertyAccessor.class;
		return copyModeBuilder;
	}

	/**
	 * Tells the {@link CopyModeBuilder} to create a copy mode, which will use provided {@code srcAccessor} to set copied values in the 
	 * destination object.
	 */
	public CopyModeBuilder to(Class<? extends Accessor> dstAccessor) {
		copyModeBuilder.dstAccessorType = dstAccessor;
		return copyModeBuilder;
	}
}
