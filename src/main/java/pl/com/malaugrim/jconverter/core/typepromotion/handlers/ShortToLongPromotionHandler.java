package pl.com.malaugrim.jconverter.core.typepromotion.handlers;

import pl.com.malaugrim.jconverter.core.typepromotion.PromotionHandler;

/**
 * @author Lukasz Tutka
 */
public class ShortToLongPromotionHandler implements PromotionHandler<Short, Long> {

	@Override
	public boolean canPromote(Class<?> srcType, Class<?> dstType) {
		return Short.class.equals(srcType) && (long.class.equals(dstType) || Long.class.equals(dstType));
	}

	@Override
	public Long promote(Short src) {
		return (long) src;
	}
}
