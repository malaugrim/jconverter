package pl.com.malaugrim.jconverter.core.copymode.copymodebuilder;

import pl.com.malaugrim.jconverter.core.accessor.Accessor;
import pl.com.malaugrim.jconverter.core.accessor.FieldAccessor;
import pl.com.malaugrim.jconverter.core.accessor.PropertyAccessor;

/**
 * Allows to select a way for retrieving accessbiles from the source object.
 * 
 * @author Lukasz Tutka
 */
public class From {

	private CopyModeBuilder copyModeBuilder;

	From(CopyModeBuilder copyModeBuilder) {
		this.copyModeBuilder = copyModeBuilder;
	}

	/**
	 * Tells the {@link CopyModeBuilder} to create a copy mode, which will copy source values from fields of the source 
	 * object via reflection.
	 */
	public To fromField() {
		copyModeBuilder.srcAccessorType = FieldAccessor.class;
		return new To(copyModeBuilder);
	}

	/**
	 * Tells the {@link CopyModeBuilder} to create a copy mode, which will copy source values from properties of the source object via 
	 * public getter.
	 */
	public To fromProperty() {
		copyModeBuilder.srcAccessorType = PropertyAccessor.class;
		return new To(copyModeBuilder);
	}

	/**
	 * Tells the {@link CopyModeBuilder} to create a copy mode, which will use provided {@code srcAccessor} to retrieve source values from 
	 * the source object.
	 */
	public To from(Class<? extends Accessor> srcAccessor) {
		copyModeBuilder.srcAccessorType = srcAccessor;
		return new To(copyModeBuilder);
	}
}
