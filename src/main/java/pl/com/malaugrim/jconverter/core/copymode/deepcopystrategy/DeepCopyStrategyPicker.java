package pl.com.malaugrim.jconverter.core.copymode.deepcopystrategy;

import java.util.List;

import pl.com.malaugrim.jconverter.core.ObjectInstantiator;

/**
 * It picks the best strategy for deep copying given object. It is required that the strategies are provided before calling 
 * {@link DeepCopyStrategyPicker#findStrategy(Object)}.
 * 
 * @author Lukasz Tutka
 */
public interface DeepCopyStrategyPicker {

	/**
	 * Sets deep copy strategies.
	 */
	void setStrategies(List<DeepCopyStrategy> strategies);

	/**
	 * Finds strategy suitable to perform deep copying of {@code object}. This method never returns {@code null}. When no 
	 * strategy can be found, it throws an exception or returns a strategy that performs shallow copy instead of deep copy.
	 * 
	 * @throws IllegalStateException when no suitable strategy can be found and {@code shallowCopyWhenCannotFindStrategy} flag is set 
	 * to {@code false}
	 * 
	 * @see DeepCopyStrategyPicker#setShallowCopyWhenCannotFindStrategy(boolean) 
	 */
	DeepCopyStrategy findStrategy(Object object);

	/**
	 * Sets {@code shallowCopyWhenCannotFindStrategy} flag. When set to {@code true}, 
	 * {@link DeepCopyStrategyPicker#findStrategy(Object)} will return a strategy which performs shallow copy instead of deep copy if no 
	 * strategy can be found. When set to {@code false}, <code>{@link IllegalStateException}</code> exception is thrown.
	 */
	void setShallowCopyWhenCannotFindStrategy(boolean shallowCopyWhenCannotFindStrategy);

	/**
	 * Passes the given {@link ObjectInstantiator} to each strategy.
	 */
	void setObjectInstantiator(ObjectInstantiator objectInstantiator);
}
