package pl.com.malaugrim.jconverter.core.typepromotion.handlers;

import pl.com.malaugrim.jconverter.core.typepromotion.PromotionHandler;

/**
 * @author Lukasz Tutka
 */
public class LongToFloatPromotionHandler implements PromotionHandler<Long, Float> {

	@Override
	public boolean canPromote(Class<?> srcType, Class<?> dstType) {
		return Long.class.equals(srcType) && (float.class.equals(dstType) || Float.class.equals(dstType));
	}

	@Override
	public Float promote(Long src) {
		return (float) src;
	}
}
