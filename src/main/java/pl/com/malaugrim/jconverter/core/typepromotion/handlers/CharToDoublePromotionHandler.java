package pl.com.malaugrim.jconverter.core.typepromotion.handlers;

import pl.com.malaugrim.jconverter.core.typepromotion.PromotionHandler;

/**
 * @author Lukasz Tutka
 */
public class CharToDoublePromotionHandler implements PromotionHandler<Character, Double> {

	@Override
	public boolean canPromote(Class<?> srcType, Class<?> dstType) {
		return Character.class.equals(srcType) && (double.class.equals(dstType) || Double.class.equals(dstType));
	}

	@Override
	public Double promote(Character src) {
		return (double) src;
	}
}
