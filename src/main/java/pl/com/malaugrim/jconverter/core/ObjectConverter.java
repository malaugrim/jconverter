package pl.com.malaugrim.jconverter.core;

import pl.com.malaugrim.jconverter.core.accessor.AccessibleObjectWrapper;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.function.Supplier;

/**
 * <p>{@code ObjectConverter} is designed for converting complex objects, which contain many other objects (either simple or complex). 
 * The way in which the source object is converted is specified by implementations of this interface.
 * Please note that all not ignored accessibles in destination type must have corresponding accessibles in source object. Otherwise an 
 * IllegalStateException will be thrown during runtime. Also, when providing type of the destination object 
 * ({@link ObjectConverter#convert(Object, Class)}), please make sure, that it has public default constructor, or else provide an instance 
 * supplier via {@link ObjectConverter#addInstanceSupplier(Class, Supplier)}.</p>
 * 
 * <p><b>Accessible</b>: field, property or other 'accessible' object</p>
 * 
 * @see AccessibleObjectWrapper
 * 
 * @author Lukasz Tutka
 */
public interface ObjectConverter<S, D> extends TypeConverter<S,D> {


	/**
	 * Converts {@code srcObj} to object of given {@code dstClass}.
	 * 
	 * @throws IllegalStateException when object of {@code dstClass} doesn't have public default constructor and no instance supplier 
	 * was provided.
	 */
	@Override
	D convert(S srcObj, Class<? extends D> dstClass);

	/**
	 * Converts {@code srcObj} to {@code dstObj}.
	 */
	@Override
	D convert(S srcObj, D dstObj);

	/**
	 * Marks given accessibles to be omitted from conversion process.
	 * @param toIgnore accessible names to ignore
	 */
	void ignore(String... toIgnore);

	/**
	 * Adds a supplier able to provide instances of given type. It is highly recommended, that each instance supplier acts as a factory, 
	 * creating new instance each time it is called.
	 * 
	 * @param <T> type of class the supplier is providing
	 * @param type type produced by the given supplier
	 * @param supplier instance supplier
	 */
	<T> void addInstanceSupplier(Class<T> type, Supplier<T> supplier);

	/**
	 * Tests whether this {@code TypeConverter} is capable of converting objects of {@code srcClass} to objects of 
	 * {@code dstClass}. By default this methods behavior is as follows:
	 * <ul>
	 *     <li>if this converter is a {@link ParameterizedType}, given {@code srcClass} and {@code dstClass} are checked whether 
	 *     they are equal to the ones declared as generic types</li>
	 *     
	 *     <li>in all other cases this method returns {@code true}</li>
	 * </ul>
	 * 
	 * If some other specific checks need to be made, one must override this method.
	 */
	@Override
	default boolean canConvert(Class<?> srcClass, Class<?> dstClass) {
		Type genericSuperclass = getClass().getGenericSuperclass();
		if(genericSuperclass instanceof ParameterizedType) {
			ParameterizedType parameterizedType = (ParameterizedType) genericSuperclass;
			Type[] genericTypes = parameterizedType.getActualTypeArguments();
			if(genericTypes.length == 2) {
				return genericTypes[0].equals(srcClass) && genericTypes[1].equals(dstClass);
			}
		}
		
		return true;
	}
}
