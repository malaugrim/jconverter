package pl.com.malaugrim.jconverter.registry;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.collect.Lists;

import pl.com.malaugrim.jconverter.core.ObjectConverter;

/**
 * @author Lukasz Tutka
 */
public class ConverterRegistry {

	private static final int HOURS_TO_EXPIRE_AFTER_ACCESS = 1;
	private List<ObjectConverter> converters = new ArrayList<>();
	private Cache<ConversionClasses, ObjectConverter> cache;


	public ConverterRegistry() {
		cache = buildDefaultCache();
	}

	public void register(ObjectConverter converter) {
		converters.add(converter);
	}

	public void register(List<ObjectConverter> converters) {
		this.converters.addAll(converters);
	}

	public List<ObjectConverter> getAllConverters() {
		return Lists.newArrayList(converters);
	}

	public boolean contains(ObjectConverter converter) {
		return converters.contains(converter);
	}

	@SuppressWarnings("unchecked")
	public Optional<ObjectConverter> find(Class<?> srcClass, Class<?> dstClass) {
		ConversionClasses conversionClasses = ConversionClasses.of(srcClass, dstClass);
		Optional<ObjectConverter> optConverter = Optional.ofNullable(cache.getIfPresent(conversionClasses));
		if(!optConverter.isPresent()) {
			optConverter = converters.stream().filter(c -> c.canConvert(srcClass, dstClass)).findAny();
			optConverter.ifPresent(c -> cache.put(conversionClasses, c));
		}
		
		return optConverter;
	}

	public void setCache(Cache<ConversionClasses, ObjectConverter> cache) {
		this.cache = cache;
	}

	private Cache<ConversionClasses, ObjectConverter> buildDefaultCache() {
		return CacheBuilder.<ConversionClasses, ObjectConverter>newBuilder()
				.weakKeys()
				.softValues()
				.expireAfterAccess(HOURS_TO_EXPIRE_AFTER_ACCESS, TimeUnit.HOURS)
				.build();
	}


}
