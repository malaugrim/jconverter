package pl.com.malaugrim.jconverter.registry;

import com.google.common.base.Objects;

/**
 * @author Lukasz Tutka
 */
public class ConversionClasses {

	private final Class<?> srcClass;
	private final Class<?> dstClass;

	public static ConversionClasses of(Class<?> srcClass, Class<?> dstClass) {
		return new ConversionClasses(srcClass, dstClass);
	}

	private ConversionClasses(Class<?> srcClass, Class<?> dstClass) {
		this.srcClass = srcClass;
		this.dstClass = dstClass;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof ConversionClasses)) return false;
		ConversionClasses conversionClasses = (ConversionClasses) o;
		return Objects.equal(srcClass, conversionClasses.srcClass) &&
				Objects.equal(dstClass, conversionClasses.dstClass);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(srcClass, dstClass);
	}
}
