package pl.com.malaugrim.jconverter.objectconverters;

import pl.com.malaugrim.jconverter.core.accessor.Accessor;
import pl.com.malaugrim.jconverter.core.accessor.FieldAccessor;
import pl.com.malaugrim.jconverter.core.accessor.PropertyAccessor;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import pl.com.malaugrim.jconverter.core.copymode.*;
import pl.com.malaugrim.jconverter.core.copymode.copymodebuilder.CopyModeBuilder;
import pl.com.malaugrim.jconverter.core.copymode.deepcopystrategy.DeepCopyStrategy;
import pl.com.malaugrim.jconverter.core.copymode.deepcopystrategy.DeepCopyStrategyPicker;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

import static org.hamcrest.CoreMatchers.*;
import static pl.com.malaugrim.jconverter.testtools.ValueExtractor.getValue;
import static org.junit.Assert.assertThat;

/**
 * @author Lukasz Tutka
 */
@RunWith(Parameterized.class)
public class StandardObjectConverterFactoryTest {

	@Parameterized.Parameters
	public static Object[][] getParams() {
		return new Object[][] {
				{ "createFieldToFieldConverter", FieldAccessor.class, FieldAccessor.class },
				{ "createFieldToPropertyConverter", FieldAccessor.class, PropertyAccessor.class },
				{ "createPropertyToFieldConverter", PropertyAccessor.class, FieldAccessor.class },
				{ "createPropertyToPropertyConverter", PropertyAccessor.class, PropertyAccessor.class }
		};
	}

	private final String createMethod;
	private final Class<? extends Accessor> expectedSrcAccessor;
	private final Class<? extends Accessor> expectedDstAccessor;
	private final StandardObjectConverterFactory factory = new StandardObjectConverterFactory();

	public StandardObjectConverterFactoryTest(String createMethod, Class<? extends Accessor> expectedSrcAccessor,
											  Class<? extends Accessor> expectedDstAccessor) {
		this.createMethod = createMethod;
		this.expectedSrcAccessor = expectedSrcAccessor;
		this.expectedDstAccessor = expectedDstAccessor;
	}

	@Test
	public void shouldTestCreateMethod() throws Exception {
		Object result = invokeCreateMethod();
		assertThat(result, instanceOf(StandardObjectConverter.class));
		assertConverter((StandardObjectConverter) result);
	}

	private Object invokeCreateMethod() throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
		return factory.getClass().getMethod(createMethod).invoke(factory);
	}

	private void assertConverter(StandardObjectConverter objectConverter) {
		CopyMode shallowCopyMode = objectConverter.getShallowCopyMode();
		CopyMode deepCopyMode = objectConverter.getDeepCopyMode();
		CopyMode conversionMode = objectConverter.getConversionMode();

		assertCopyMode(shallowCopyMode,  ShallowCopyMode.class);
		assertCopyMode(conversionMode, ConversionMode.class);
		assertDeepCopyMode((DeepCopyMode) deepCopyMode);
	}

	@SuppressWarnings("unchecked")
	private <T extends AbstractCopyMode> void assertCopyMode(CopyMode copyMode, Class<T> expectedCopyMode) {
		assertThat(copyMode, instanceOf(expectedCopyMode));

		T shallowCopyModeCasted = (T) copyMode;
		Class<Accessor> srcAccessorType = getValue(shallowCopyModeCasted, "srcAccessorType");
		Class<Accessor> dstAccessorType = getValue(shallowCopyModeCasted, "dstAccessorType");
		assertThat(srcAccessorType, equalTo(expectedSrcAccessor));
		assertThat(dstAccessorType, equalTo(expectedDstAccessor));
	}

	private void assertDeepCopyMode(DeepCopyMode deepCopyMode) {
		DeepCopyMode deepCopyModeFromBuilder = new CopyModeBuilder().deepCopy().fromField().toField().build();
		List<DeepCopyStrategy> strategies = getField(deepCopyMode, "deepCopyStrategies");
		List<DeepCopyStrategy> strategiesFromBuilder = getField(deepCopyModeFromBuilder, "deepCopyStrategies");
		assertSameStrategies(strategies, strategiesFromBuilder);

		DeepCopyStrategyPicker strategyPicker = getField(deepCopyMode, "strategyPicker");
		DeepCopyStrategyPicker strategyPickerFromBuilder = getField(deepCopyModeFromBuilder, "strategyPicker");
		assertThat(strategyPicker.getClass(), equalTo(strategyPickerFromBuilder.getClass()));

	}

	private void assertSameStrategies(List<DeepCopyStrategy> strategies, List<DeepCopyStrategy> strategiesFromBuilder) {
		assertThat(strategies.size(), not(0));
		assertThat(strategies.size(), equalTo(strategiesFromBuilder.size()));

		for (int i = 0; i < strategies.size(); i++) {
			assertThat("Position: " + i, strategies.get(i).getClass(), equalTo(strategiesFromBuilder.get(i).getClass()));
		}
	}

	@SuppressWarnings("unchecked")
	private <T> T getField(DeepCopyMode deepCopyMode, String fieldName) {
		try {
			Field field = deepCopyMode.getClass().getDeclaredField(fieldName);
			field.setAccessible(true);
			return (T) field.get(deepCopyMode);
		} catch (ReflectiveOperationException e) {
			throw new RuntimeException(e);
		}
	}
}
