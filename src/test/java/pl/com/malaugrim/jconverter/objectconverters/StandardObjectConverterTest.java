package pl.com.malaugrim.jconverter.objectconverters;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.function.Supplier;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.*;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;

import pl.com.malaugrim.jconverter.TestConstants;
import pl.com.malaugrim.jconverter.core.ObjectInstantiator;
import pl.com.malaugrim.jconverter.core.TypeConverter;
import pl.com.malaugrim.jconverter.core.accessor.AccessibleObjectWrapper;
import pl.com.malaugrim.jconverter.core.accessor.PropertyAccessor;
import pl.com.malaugrim.jconverter.core.copymode.*;
import pl.com.malaugrim.jconverter.core.copymode.copymodebuilder.CopyModeBuilder;
import pl.com.malaugrim.jconverter.testconverters.IntToIntConverter;
import pl.com.malaugrim.jconverter.core.copymode.copymodebuilder.ConversionModeVariant;
import pl.com.malaugrim.jconverter.testobjects.simple.*;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.same;
import static org.mockito.Mockito.*;

/**
 * @author Lukasz Tutka
 */
@SuppressWarnings("unchecked")
public class StandardObjectConverterTest implements TestConstants {
	
	@Rule
	public final ExpectedException thrown = ExpectedException.none();
	
	private TestObject1 testObject1;
	private TestObject2 testObject2;
	private TestObject3 testObject3;

	@Mock
	private ShallowCopyMode shallowCopyMode;
	@Mock
	private DeepCopyMode deepCopyMode;
	@Mock
	private ConversionMode conversionMode;


	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		testObject1 = new TestObject1(INT_VALUE, LONG_VALUE, DOUBLE_VALUE, BOOLEAN_VALUE, STRING_VALUE);
		testObject2 = new TestObject2(INT_VALUE, DOUBLE_VALUE, STRING_VALUE);
		testObject3 = new TestObject3(testObject1, testObject2);
	}

	@SuppressWarnings("SpellCheckingInspection")
	@Test
	public void shouldShallowCopyObjectWhenProvidedDstClass() {
		Set<String> accessibles = Sets.newHashSet("intValue", "doubleValue", "stringValue");
		ShallowCopyMode shallowCopyMode = createCopyModeMock(accessibles, ShallowCopyMode.class);

		StandardObjectConverter<TestObject2, TestObject2> converter = new StandardObjectConverter<>(shallowCopyMode, deepCopyMode, 
				conversionMode);
		TestObject2 result = converter.convert(testObject2, TestObject2.class);

		ArgumentCaptor<TestObject2> dstCaptor = ArgumentCaptor.forClass(TestObject2.class);
		InOrder inOrder = inOrder(shallowCopyMode);
		inOrder.verify(shallowCopyMode).setup(same(testObject2), dstCaptor.capture());
		inOrder.verify(shallowCopyMode).getSrcAccessibles();
		inOrder.verify(shallowCopyMode).getDstAccessibles();
		assertThat(result, sameInstance(dstCaptor.getValue()));
		verifyAccessiblesCopied(shallowCopyMode, accessibles);
		verifyCopyNotCalled(deepCopyMode, conversionMode);
	}

	@Test
	public void shouldShallowCopyObjectWhenProvidedDstInstance() {
		Set<String> accessibles = Sets.newHashSet("intValue", "doubleValue", "stringValue");
		ShallowCopyMode shallowCopyMode = createCopyModeMock(accessibles, ShallowCopyMode.class);

		StandardObjectConverter<TestObject2, TestObject2> converter = new StandardObjectConverter(shallowCopyMode, deepCopyMode,
				conversionMode);
		TestObject2 dst = new TestObject2();
		TestObject2 result = converter.convert(testObject2, dst);

		InOrder inOrder = inOrder(shallowCopyMode);
		inOrder.verify(shallowCopyMode).setup(same(testObject2), same(dst));
		inOrder.verify(shallowCopyMode).getSrcAccessibles();
		inOrder.verify(shallowCopyMode).getDstAccessibles();
		assertThat(result, sameInstance(dst));
		verifyAccessiblesCopied(shallowCopyMode, accessibles);
		verifyCopyNotCalled(deepCopyMode, conversionMode);
	}

	@Test
	public void shouldIgnoreSelectedAccessiblesWhileShallowCopy() {
		Set<String> toCopy = Sets.newHashSet("longValue", "booleanValue", "stringValue");
		Set<String> toIgnore = Sets.newHashSet("intValue", "doubleValue");
		ShallowCopyMode shallowCopyMode = createCopyModeMock(ImmutableSet.<String>builder().addAll(toCopy).addAll(toIgnore).build(), ShallowCopyMode.class);

		StandardObjectConverter<TestObject1, TestObject1> converter = new StandardObjectConverter<>(shallowCopyMode, mock(DeepCopyMode.class),
				mock(ConversionMode.class));
		converter.ignore(toIgnore.toArray(new String[toIgnore.size()]));
		TestObject1 result = converter.convert(testObject1, TestObject1.class);

		ArgumentCaptor<TestObject2> dstCaptor = ArgumentCaptor.forClass(TestObject2.class);
		InOrder inOrder = inOrder(shallowCopyMode);
		inOrder.verify(shallowCopyMode).setup(same(testObject1), dstCaptor.capture());
		inOrder.verify(shallowCopyMode).getSrcAccessibles();
		inOrder.verify(shallowCopyMode).getDstAccessibles();
		assertThat(result, sameInstance(dstCaptor.getValue()));
		verifyAccessiblesCopied(shallowCopyMode, toCopy);
		verifyAccessiblesIgnored(shallowCopyMode, toIgnore);
		verifyCopyNotCalled(deepCopyMode, conversionMode);
	}

	@Test
	public void shouldDeepCopyObjectWhenProvidedDstClass() {
		Set<String> accessibles = Sets.newHashSet("testObject1", "testObject2");
		DeepCopyMode deepCopyMode = createCopyModeMock(accessibles, DeepCopyMode.class);

		StandardObjectConverter<TestObject3, TestObject3> converter = new StandardObjectConverter<>(shallowCopyMode, deepCopyMode,
				conversionMode);
		converter.deepCopy("testObject1");
		TestObject3 result = converter.convert(testObject3, TestObject3.class);

		ArgumentCaptor<TestObject2> dstCaptor = ArgumentCaptor.forClass(TestObject2.class);
		verify(deepCopyMode).setup(same(testObject3), dstCaptor.capture());
		assertThat(result, sameInstance(dstCaptor.getValue()));
		verifyAccessiblesCopied(deepCopyMode);
		verifyCopyNotCalled(shallowCopyMode, conversionMode);
	}

	@Test
	public void shouldDeepCopyObjectWhenProvidedDstInstance() {
		Set<String> accessibles = Sets.newHashSet("intValue", "doubleValue", "stringValue");
		DeepCopyMode deepCopyMode = createCopyModeMock(accessibles, DeepCopyMode.class);

		StandardObjectConverter<TestObject3, TestObject3> converter = new StandardObjectConverter(shallowCopyMode, deepCopyMode,
				conversionMode);
		converter.deepCopy("testObject1");
		TestObject3 dst = new TestObject3();
		TestObject3 result = converter.convert(testObject3, dst);

		verify(deepCopyMode).setup(same(testObject3), same(dst));
		assertThat(result, sameInstance(dst));
		verifyAccessiblesCopied(deepCopyMode);
		verifyCopyNotCalled(shallowCopyMode, conversionMode);
	}

	@Test
	public void shouldIgnoreSelectedAccessiblesWhileDeepCopy() {
		Set<String> toCopy = Sets.newHashSet("longValue", "booleanValue", "stringValue");
		Set<String> toIgnore = Sets.newHashSet("intValue", "doubleValue");
		DeepCopyMode deepCopyMode = createCopyModeMock(ImmutableSet.<String>builder().addAll(toCopy).addAll(toIgnore).build(), DeepCopyMode.class);

		StandardObjectConverter<TestObject1, TestObject1> converter = new StandardObjectConverter<>(shallowCopyMode, deepCopyMode,
				conversionMode);
		converter.ignore(toIgnore.toArray(new String[toIgnore.size()]));
		converter.deepCopy(toCopy);
		TestObject1 result = converter.convert(testObject1, TestObject1.class);

		ArgumentCaptor<TestObject2> dstCaptor = ArgumentCaptor.forClass(TestObject2.class);
		verify(deepCopyMode).setup(same(testObject1), dstCaptor.capture());
		assertThat(result, sameInstance(dstCaptor.getValue()));
		verifyAccessiblesCopied(deepCopyMode, toCopy);
		verifyAccessiblesIgnored(deepCopyMode, toIgnore);
		verifyCopyNotCalled(shallowCopyMode, conversionMode);
	}

	@Test
	public void shouldUseTypeBaseConverter() {
		TypeConverter<Long, String> longToStringConverter = mock(TypeConverter.class);
		when(longToStringConverter.canConvert(Matchers.any(), Matchers.any())).thenReturn(true);
		CopyMode shallowCopyMode = createShallowCopyMode();
		CopyMode deepCopyMode = createDeepCopyMode();
		ConversionMode conversionMode = createConversionMode(longToStringConverter);
		
		StandardObjectConverter<ObjectWithLongField, ObjectWithStringField> converter = new StandardObjectConverter<>(shallowCopyMode,
				deepCopyMode, conversionMode);

		converter.addConverter(longToStringConverter);
		converter.convert(new ObjectWithLongField(LONG_VALUE), ObjectWithStringField.class);
		verify(longToStringConverter, atLeastOnce()).canConvert(Long.class, String.class);
		verify(longToStringConverter).convert(LONG_VALUE, String.class);
	}

	@Test
	public void shouldUseNameBasedConverter() {
		TypeConverter<Long, Long> longToLongConverter = createConverterMock();
		CopyModeBuilder builder = new CopyModeBuilder();
		CopyMode shallowCopyMode = builder.shallowCopy().fromField().toField().build();
		CopyMode deepCopyMode = builder.deepCopy().fromField().toField().build();
		ConversionMode conversionMode = builder.convert(ConversionModeVariant.TAKE_DST_VALUE_OR_TYPE, longToLongConverter).fromField()
				.toField().build();

		StandardObjectConverter<TestObject1, TestObject1> converter = new StandardObjectConverter<>(shallowCopyMode,
				deepCopyMode, conversionMode);
		
		converter.addConverter("longValue", longToLongConverter);
		converter.deepCopy("longValue");
		converter.convert(testObject1, TestObject1.class);
		verify(longToLongConverter).convert(LONG_VALUE, 0L);
	}

	@Test
	public void shouldNotUseTypeBasedConverterWhenNameBasedIsProvided() {
		TypeConverter<Long, Long> nameBasedConverter = createConverterMock();
		TypeConverter<Long, Long> typeBasedConverter = createConverterMock();
		CopyModeBuilder builder = new CopyModeBuilder();
		CopyMode shallowCopyMode = builder.shallowCopy().fromField().toField().build();
		CopyMode deepCopyMode = builder.deepCopy().fromField().toField().build();
		ConversionMode conversionMode = builder.convert(ConversionModeVariant.TAKE_DST_VALUE_OR_TYPE, nameBasedConverter).fromField()
				.toField().build();

		StandardObjectConverter<TestObject1, TestObject1> converter = new StandardObjectConverter<>(shallowCopyMode,
				deepCopyMode, conversionMode);

		converter.addConverter("longValue", nameBasedConverter);
		converter.addConverter(typeBasedConverter);
		converter.convert(testObject1, TestObject1.class);
		
		verify(nameBasedConverter).convert(LONG_VALUE, 0L);
		verify(typeBasedConverter, never()).convert(LONG_VALUE, Long.class);
		verify(typeBasedConverter, never()).convert(LONG_VALUE, 0L);
	}

	@Test
	public void shouldClearInternalStateAfterConversion() {
		TypeConverter<Long, Long> longToLongConverter = createConverterMock();
		CopyModeBuilder builder = new CopyModeBuilder();
		CopyMode shallowCopyMode = builder.shallowCopy().fromField().toField().build();
		CopyMode deepCopyMode = builder.deepCopy().fromField().toField().build();
		ConversionMode conversionMode = builder.convert(ConversionModeVariant.TAKE_DST_VALUE_OR_TYPE, longToLongConverter).fromField()
				.toField().build();

		StandardObjectConverter<TestObject1, TestObject1> converter = new StandardObjectConverter<>(shallowCopyMode,
				deepCopyMode, conversionMode);

		converter.addConverter("longValue", longToLongConverter);
		converter.convert(testObject1, TestObject1.class);
		
		assertThat(converter.conversionModeSetUp, is(false));
		assertThat(converter.convertedAccessibles.isEmpty(), is(true));
	}

	@Test
	public void shouldUseProvidedInstanceSupplierToCreateDstObject() {
		StandardObjectConverter<WithoutDefaultConstructor, WithoutDefaultConstructor> converter = spy(new StandardObjectConverter<>(shallowCopyMode,
				deepCopyMode, conversionMode));
		Supplier<WithoutDefaultConstructor> supplier = spy(new WithoutDefaultConstructorSupplier(LONG_VALUE));
		converter.addInstanceSupplier(WithoutDefaultConstructor.class, supplier);
		WithoutDefaultConstructor src = new WithoutDefaultConstructor(LONG_VALUE + 10);
		converter.convert(src, WithoutDefaultConstructor.class);

		verify(supplier).get();
		verify(converter).convert(src, supplier.get());
	}

	@Test
	public void shouldThrowExceptionWhenDstDoesNotHaveDefaultConstructorAndNoInstanceSupplierWasProvided() {
		thrown.expect(IllegalStateException.class);
		thrown.expectMessage("Cannot create instance of class");
		
		StandardObjectConverter<WithoutDefaultConstructor, WithoutDefaultConstructor> converter = new StandardObjectConverter<>(shallowCopyMode,
				deepCopyMode, conversionMode);
		converter.deepCopy("field");
		WithoutDefaultConstructor src = new WithoutDefaultConstructor(LONG_VALUE + 10);
		converter.convert(src, WithoutDefaultConstructor.class);
	}

	@Test
	public void shouldUseProvidedInstanceSupplierInDeepCopyMode() {
		CopyMode shallowCopyMode = createCopyModeSpy(false);
		CopyMode deepCopyMode = createCopyModeSpy(true);
		ConversionMode conversionMode = createConversionMode(createConverterMock());
		StandardObjectConverter<TestObject5, TestObject5> converter = new StandardObjectConverter<>(shallowCopyMode, deepCopyMode,
				conversionMode);
		
		TestObject5 src = new TestObject5(new WithoutDefaultConstructor(LONG_VALUE + 10));
		Supplier supplier = spy(new WithoutDefaultConstructorSupplier(LONG_VALUE));
		converter.addInstanceSupplier(WithoutDefaultConstructor.class, supplier);
		converter.deepCopy("field");
		converter.convert(src, TestObject5.class);

		verify(deepCopyMode).setObjectInstantiator(any(ObjectInstantiator.class));
		verify(deepCopyMode).setup(eq(src), any(TestObject5.class));
		verify(deepCopyMode).copy("field", "field");
		verify(supplier).get();
	}

	@Test
	public void shouldReturnGivenDstInstance() {
		StandardObjectConverter<TestObject1, TestObject1> converter = new StandardObjectConverter<>(shallowCopyMode, deepCopyMode,
				conversionMode);
		
		TestObject1 dst = new TestObject1();
		TestObject1 result = converter.convert(testObject1, dst);
		assertThat(result, sameInstance(dst));
	}

	@Test
	public void shouldConvertUsingPropertyToPropertyCopyModes() {
		CopyModeBuilder builder = new CopyModeBuilder();
		ShallowCopyMode shallowCopyMode = builder.shallowCopy().fromProperty().toProperty().build();
		DeepCopyMode deepCopyMode = builder.deepCopy().fromProperty().toProperty().build();
		ConversionMode conversionMode = new ConversionModeWithPassingDstValueOrType(PropertyAccessor.class, PropertyAccessor.class);
		StandardObjectConverter<TestObject2, TestObject2> converter = new StandardObjectConverter<>(shallowCopyMode, deepCopyMode, conversionMode);
		converter.deepCopy("doubleValue");
		converter.addConverter("intValue", new IntToIntConverter());

		TestObject2 result = converter.convert(testObject2, TestObject2.class);
		assertThat(result, equalTo(new TestObject2(INT_VALUE + 10, DOUBLE_VALUE, STRING_VALUE)));
	}

	private <S, D> TypeConverter<S, D> createConverterMock() {
		TypeConverter<S, D> typeConverter = mock(TypeConverter.class);
		when(typeConverter.canConvert(Matchers.any(), Matchers.any())).thenReturn(true);
		return typeConverter;
	}

	private CopyMode createCopyModeSpy(boolean deepCopy) {
		CopyMode copyMode;
		if(deepCopy) {
			copyMode = createDeepCopyMode();
		} else {
			copyMode = createShallowCopyMode();
		}
		
		return spy(copyMode);
	}

	private <T extends CopyMode> T createCopyModeMock(Set<String> accessibles, Class<T> copyModeType) {
		T copyMode = mock(copyModeType);
		List<AccessibleObjectWrapper> acc = createAccessibleWrapperMocks(accessibles);
		when(copyMode.getSrcAccessibles()).thenReturn(acc);
		when(copyMode.getDstAccessibles()).thenReturn(acc);
		return copyMode;
	}

	private List<AccessibleObjectWrapper> createAccessibleWrapperMocks(Set<String> accessibles) {
		List<AccessibleObjectWrapper> mocks = new ArrayList<>();
		for (String accessibleName : accessibles) {
			AccessibleObjectWrapper wrapper = mock(AccessibleObjectWrapper.class);
			when(wrapper.getAccessibleName()).thenReturn(accessibleName);
			mocks.add(wrapper);
		}
		
		return mocks;
	}

	private void verifyAccessiblesCopied(CopyMode copyMode) {
		verifyAccessiblesCopied(copyMode, Sets.newHashSet("testObject1"));
	}

	private void verifyAccessiblesCopied(CopyMode copyMode, Set<String> accessibles) {
		accessibles.forEach(accessible -> verify(copyMode).copy(accessible, accessible));
	}

	private void verifyAccessiblesIgnored(CopyMode copyMode, Set<String> accessibles) {
		accessibles.forEach(accessible -> verify(copyMode, never()).copy(accessible, accessible));
	}

	private void verifyCopyNotCalled(CopyMode... modes) {
		Arrays.stream(modes).forEach(this::verifyCopyNotCalled);
	}

	private void verifyCopyNotCalled(CopyMode copyMode) {
		verify(copyMode, never()).copy(Matchers.any(), Matchers.any());
	}

	private ConversionMode createConversionMode(TypeConverter<Long, String> typeConverter) {
		return new CopyModeBuilder().convert(ConversionModeVariant.TAKE_DST_VALUE_OR_TYPE, typeConverter).fromField().toField().build();
	}

	private CopyMode createDeepCopyMode() {
		return new CopyModeBuilder().deepCopy().fromField().toField().build();
	}

	private CopyMode createShallowCopyMode() {
		return new CopyModeBuilder().shallowCopy().fromField().toField().build();
	}

}
