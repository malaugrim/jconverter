package pl.com.malaugrim.jconverter.objectconverters;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.mockito.InOrder;
import org.mockito.verification.VerificationMode;
import pl.com.malaugrim.jconverter.TestConstants;
import pl.com.malaugrim.jconverter.testobjects.annotated.*;
import pl.com.malaugrim.jconverter.testobjects.simple.TestObject1;
import pl.com.malaugrim.jconverter.testobjects.simple.TestObject3;

import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;
import static pl.com.malaugrim.jconverter.testtools.matchers.TestMatchers.isProperlyCopied;

/**
 * @author Lukasz Tutka
 */
@RunWith(Parameterized.class)
public class AnnotationObjectConverterCopyFromAccessibleChainTest implements TestConstants {

	private final Class<?> dstClass;
	private final Object expected;
	private final VerificationMode timesCalled;

	private AnnotationObjectConverter converter;
	private TestObject3 testObject3;
	private TestObject1 testObject1;

	@Parameterized.Parameters
	public static Object[] data() {
		return new Object[][] {
				{ WithShallowCopyOnFieldPointingSrcFieldChain.class, new WithShallowCopyOnFieldPointingSrcFieldChain(STRING_VALUE), 0 },
				{ WithShallowCopyOnPropertyPointingSrcFieldChain.class, new WithShallowCopyOnPropertyPointingSrcFieldChain(STRING_VALUE), 0 },
				{ WithDeepCopyOnFieldPointingSrcFieldChain.class, new WithDeepCopyOnFieldPointingSrcFieldChain(STRING_VALUE), 0 },
				{ WithDeepCopyOnPropertyPointingSrcFieldChain.class, new WithDeepCopyOnPropertyPointingSrcFieldChain(STRING_VALUE), 0 },
				{ WithConvertOnFieldPointingSrcFieldChain.class, new WithConvertOnFieldPointingSrcFieldChain(STRING_VALUE), 0 },
				{ WithConvertOnPropertyPointingSrcFieldChain.class, new WithConvertOnPropertyPointingSrcFieldChain(STRING_VALUE), 0 },

				{ WithShallowCopyOnFieldPointingSrcPropertyChain.class, new WithShallowCopyOnFieldPointingSrcPropertyChain(STRING_VALUE), 1 },
				{ WithShallowCopyOnPropertyPointingSrcPropertyChain.class, new WithShallowCopyOnPropertyPointingSrcPropertyChain(STRING_VALUE), 1 },
				{ WithDeepCopyOnFieldPointingSrcPropertyChain.class, new WithDeepCopyOnFieldPointingSrcPropertyChain(STRING_VALUE), 1 },
				{ WithDeepCopyOnPropertyPointingSrcPropertyChain.class, new WithDeepCopyOnPropertyPointingSrcPropertyChain(STRING_VALUE), 1 },
				{ WithConvertOnFieldPointingSrcPropertyChain.class, new WithConvertOnFieldPointingSrcPropertyChain(STRING_VALUE), 1 },
				{ WithConvertOnPropertyPointingSrcPropertyChain.class, new WithConvertOnPropertyPointingSrcPropertyChain(STRING_VALUE), 1 }
		};
	}

	public AnnotationObjectConverterCopyFromAccessibleChainTest(Class<?> dstClass, Object expected, int timesDstMethodsCalled) {
		this.dstClass = dstClass;
		this.expected = expected;
		this.timesCalled = times(timesDstMethodsCalled);
	}

	@Before
	public void setUp() {
		converter = new AnnotationObjectConverter();
		testObject1 = spy(new TestObject1(INT_VALUE, LONG_VALUE, DOUBLE_VALUE, BOOLEAN_VALUE, STRING_VALUE));
		testObject3 = spy(new TestObject3(testObject1, null));
	}

	@SuppressWarnings("unchecked")
	@Test
	public void shouldCopyFromPointedSrcFieldChain() {
		Object result = converter.convert(testObject3, dstClass);

		assertThat(result, isProperlyCopied(expected));
		InOrder inOrder = inOrder(testObject3, testObject1);
		inOrder.verify(testObject3, timesCalled).getTestObject1();
		inOrder.verify(testObject1, timesCalled).getStringValue();
	}
}
