package pl.com.malaugrim.jconverter.objectconverters;

import pl.com.malaugrim.jconverter.core.copymode.ConversionMode;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import pl.com.malaugrim.jconverter.core.ObjectConverter;
import pl.com.malaugrim.jconverter.core.copymode.CopyMode;
import pl.com.malaugrim.jconverter.testobjects.simple.TestObject1;
import pl.com.malaugrim.jconverter.testobjects.simple.TestObject2;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;

/**
 * @author Lukasz Tutka
 */
@RunWith(Parameterized.class)
public class CanConvertMethodTest {

	private static CopyMode shallowCopyMode = mock(CopyMode.class);
	private static CopyMode deepCopyMode = mock(CopyMode.class);
	private static ConversionMode conversionMode = mock(ConversionMode.class);
	
	private final ObjectConverter converter;
	private final boolean genericTypesDeclared;
	
	@Parameterized.Parameters
	public static Object[] data() {
		return new Object[][] {
				{ new StandardObjectConverterWithGenericTypes(), true },
				{ new FlexibleObjectConverterWithGenericTypes(), true },
				{ new AnnotationObjectConverterWithGenericTypes(), true },
				{ new StandardObjectConverterWithoutGenericTypes(), false },
				{ new FlexibleObjectConverterWithoutGenericTypes(), false },
				{ new AnnotationObjectConverterWithoutGenericTypes(), false }
		};
	}

	public CanConvertMethodTest(ObjectConverter converter, boolean genericTypesDeclared) {
		this.converter = converter;
		this.genericTypesDeclared = genericTypesDeclared;
	}

	@SuppressWarnings("unchecked")
	@Test
	public void canConvertMethodTest() {
		assertTrue(converter.canConvert(TestObject1.class, TestObject2.class));
		assertThat(converter.canConvert(TestObject2.class, TestObject1.class), equalTo(!genericTypesDeclared));
	}

	private static class StandardObjectConverterWithGenericTypes extends StandardObjectConverter<TestObject1, TestObject2> {
		StandardObjectConverterWithGenericTypes() {
			super(shallowCopyMode, deepCopyMode, conversionMode);
		}
	}

	private static class StandardObjectConverterWithoutGenericTypes extends StandardObjectConverter {
		StandardObjectConverterWithoutGenericTypes() {
			super(shallowCopyMode, deepCopyMode, conversionMode);
		}
	}
	
	private static class FlexibleObjectConverterWithGenericTypes extends FlexibleObjectConverter<TestObject1, TestObject2> {}
	
	private static class FlexibleObjectConverterWithoutGenericTypes extends FlexibleObjectConverter {}
	
	private static class AnnotationObjectConverterWithGenericTypes extends AnnotationObjectConverter<TestObject1, TestObject2> {}
	
	private static class AnnotationObjectConverterWithoutGenericTypes extends AnnotationObjectConverter {}
}
