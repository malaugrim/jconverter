package pl.com.malaugrim.jconverter.objectconverters;

import java.util.function.Supplier;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Matchers;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import static org.hamcrest.CoreMatchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static pl.com.malaugrim.jconverter.testtools.matchers.TestMatchers.isProperlyCopied;

import pl.com.malaugrim.jconverter.TestConstants;
import pl.com.malaugrim.jconverter.core.ObjectInstantiator;
import pl.com.malaugrim.jconverter.core.copymode.ConversionMode;
import pl.com.malaugrim.jconverter.core.copymode.CopyMode;
import pl.com.malaugrim.jconverter.core.copymode.ShallowCopyMode;
import pl.com.malaugrim.jconverter.core.copymode.copymodebuilder.ConversionModeVariant;
import pl.com.malaugrim.jconverter.core.copymode.copymodebuilder.CopyModeBuilder;
import pl.com.malaugrim.jconverter.testconverters.LongToStringConverter;
import pl.com.malaugrim.jconverter.testobjects.simple.*;

/**
 * @author Lukasz Tutka
 */
@SuppressWarnings("unchecked")
public class FlexibleObjectConverterTest implements TestConstants {

	@Rule
	public final ExpectedException thrown = ExpectedException.none();
	
	private FlexibleObjectConverter converter;

	private final CopyModeBuilder cmb = new CopyModeBuilder();
	
	@Spy
	private LongToStringConverter ltsConverter;
	@Spy
	private final CopyMode fieldToFieldCopyMode = cmb.shallowCopy().fromField().toField().build();
	@Spy
	private final CopyMode fieldToFieldDeepCopyMode = cmb.deepCopy().fromField().toField().build();
	@Spy
	private final CopyMode propertyToFieldCopyMode = cmb.shallowCopy().fromProperty().toField().build();
	@Spy
	private final CopyMode propertyToFieldDeepCopyMode = cmb.deepCopy().fromProperty().toField().build();
	@Spy
	private final CopyMode fieldToPropertyCopyMode = cmb.shallowCopy().fromField().toProperty().build();
	@Spy
	private final CopyMode fieldToPropertyDeepCopyMode = cmb.deepCopy().fromField().toProperty().build();
	@Spy
	private final CopyMode propertyToPropertyCopyMode = cmb.shallowCopy().fromProperty().toField().build();
	@Spy
	private final CopyMode propertyToPropertyDeepCopyMode = cmb.deepCopy().fromProperty().toProperty().build();

	private TestObject1 testObject1;
	private TestObject3 testObject3;


	@Before
	public void setUp() {
		converter = new FlexibleObjectConverter();
		testObject1 = new TestObject1(INT_VALUE, LONG_VALUE, DOUBLE_VALUE, BOOLEAN_VALUE, STRING_VALUE);
		testObject3 = new TestObject3(testObject1, null);
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void shouldCopyDstFieldUsingPointedSrcFieldWithTheSameName() {
		testCopying(testObject3, TestObject3.class, fieldToFieldCopyMode);
	}

	@Test
	public void shouldDeepCopyDstFieldUsingPointedSrcFieldWithTheSameName() {
		testCopying(testObject3, TestObject3.class, fieldToFieldDeepCopyMode);
	}

	@Test
	public void shouldCopyDstFieldUsingPointedSrcFieldWithDifferentSameName() {
		WithTestObject1Getter expected  = new WithTestObject1Getter(testObject1);
		testCopying("object", expected, WithTestObject1Getter.class, fieldToFieldCopyMode);
	}

	@Test
	public void shouldDeepCopyDstFieldUsingPointedSrcFieldWithDifferentSameName() {
		WithTestObject1Getter expected  = new WithTestObject1Getter(testObject1);
		testCopying("object", expected, WithTestObject1Getter.class, fieldToFieldDeepCopyMode);
	}

	@Test
	public void shouldSetDstFieldWithValueConvertedFromSrcField() {
		ConversionMode conversionMode = cmb.convert(ConversionModeVariant.TAKE_DST_VALUE_OR_TYPE, ltsConverter).fromField().toField().build();
		testConversion(spy(conversionMode));
	}

	@Test
	public void shouldCopyDstFieldUsingPointedSrcPropertyWithTheSameName() {
		WithTestObject1Setter3 expected = new WithTestObject1Setter3(testObject1);
		testCopying(expected, WithTestObject1Setter3.class, propertyToFieldCopyMode);
	}

	@Test
	public void shouldDeepCopyDstFieldUsingPointedSrcPropertyWithTheSameName() {
		WithTestObject1Setter3 expected = new WithTestObject1Setter3(testObject1);
		testCopying(expected, WithTestObject1Setter3.class, propertyToFieldDeepCopyMode);
	}

	@Test
	public void shouldCopyDstFieldUsingPointedSrcPropertyWithDifferentName() {
		WithTestObject1Setter expected = new WithTestObject1Setter(testObject1);
		testCopying("field", expected, WithTestObject1Setter.class, propertyToFieldCopyMode);
	}

	@Test
	public void shouldDeepCopyDstFieldUsingPointedSrcPropertyWithDifferentName() {
		WithTestObject1Setter expected = new WithTestObject1Setter(testObject1);
		testCopying("field", expected, WithTestObject1Setter.class, propertyToFieldDeepCopyMode);
	}

	@Test
	public void shouldSetDstFieldWithValueConvertedFromSrcProperty() {
		ConversionMode conversionMode = cmb.convert(ConversionModeVariant.TAKE_DST_VALUE_OR_TYPE, ltsConverter).fromProperty().toField().build();
		testConversion(spy(conversionMode));
	}

	@Test
	public void shouldCopyDstPropertyUsingPointedSrcFieldWithTheSameName() {
		testCopying(testObject3, TestObject3.class, fieldToPropertyCopyMode);
	}

	@Test
	public void shouldDeepCopyDstPropertyUsingPointedSrcFieldWithTheSameName() {
		testCopying(testObject3, TestObject3.class, fieldToPropertyDeepCopyMode);
	}

	@Test
	public void shouldCopyDstPropertyUsingPointedSrcFieldWithDifferentName() {
		WithTestObject1Setter2 expected = new WithTestObject1Setter2(testObject1);
		testCopying("object", expected, WithTestObject1Setter2.class, fieldToPropertyCopyMode);
	}

	@Test
	public void shouldDeepCopyDstPropertyUsingPointedSrcFieldWithDifferentName() {
		WithTestObject1Setter2 expected = new WithTestObject1Setter2(testObject1);
		testCopying("object", expected, WithTestObject1Setter2.class, fieldToPropertyDeepCopyMode);
	}

	@Test
	public void shouldSetDstPropertyWithValueConvertedFromSrcField() {
		ConversionMode conversionMode = cmb.convert(ConversionModeVariant.TAKE_DST_VALUE_OR_TYPE, ltsConverter).fromField().toProperty().build();
		testConversion(spy(conversionMode));
	}

	@Test
	public void shouldCopyDstPropertyUsingPointedPropertyWithTheSameName() {
		testCopying(testObject3, TestObject3.class, propertyToPropertyCopyMode);
	}

	@Test
	public void shouldDeepCopyDstPropertyUsingPointedPropertyWithTheSameName() {
		testCopying(testObject3, TestObject3.class, propertyToPropertyDeepCopyMode);
	}

	@Test
	public void shouldCopyDstPropertyUsingPointedSrcPropertyWithDifferentName() {
		WithTestObject1Setter2 expected = new WithTestObject1Setter2(testObject1);
		testCopying("object", expected, WithTestObject1Setter2.class, propertyToPropertyCopyMode);
	}

	@Test
	public void shouldDeepCopyDstPropertyUsingPointedSrcPropertyWithDifferentName() {
		WithTestObject1Setter2 expected = new WithTestObject1Setter2(testObject1);
		testCopying("object", expected, WithTestObject1Setter2.class, propertyToPropertyDeepCopyMode);
	}

	@Test
	public void shouldSetDstPropertyWithValueConvertedFromSrcProperty() {
		ConversionMode conversionMode = cmb.convert(ConversionModeVariant.TAKE_DST_VALUE_OR_TYPE, ltsConverter).fromProperty()
				.toProperty().build();
		testConversion(spy(conversionMode));
	}

	@Test
	public void shouldUseProvidedDefaultCopyModeToCopyAllCommonAccessibles() {
		converter.setDefaultCopyMode(fieldToFieldCopyMode);
		testObject3.setTestObject2(new TestObject2(INT_VALUE, DOUBLE_VALUE, STRING_VALUE));
		converter.convert(testObject3, TestObject3.class);
		verify(fieldToFieldCopyMode).setup(any(TestObject3.class), any(TestObject3.class));
		verify(fieldToFieldCopyMode).copy("testObject1", "testObject1");
		verify(fieldToFieldCopyMode).copy("testObject2", "testObject2");
	}

	@Test
	public void shouldIgnoreDesignatedPropertiesEvenWhenMarkedToCopyOrForConversion() {
		String intValue = "intValue";
		String longValue = "longValue";
		String[] toIgnore = new String[] { intValue, longValue, "doubleValue" };
		
		converter.setDefaultCopyMode(fieldToFieldCopyMode);
		converter.setCopyMode(intValue, fieldToPropertyCopyMode);
		converter.setCopyMode(longValue, propertyToFieldCopyMode);
		converter.ignore(toIgnore);
		converter.convert(testObject1, TestObject1.class);
		
		for (String s : toIgnore) {
			verify(fieldToFieldCopyMode, never()).copy(s, s);
			verify(fieldToPropertyCopyMode, never()).copy(s, s);
			verify(propertyToFieldCopyMode, never()).copy(s, s);
		}


		verify(fieldToFieldCopyMode).copy("booleanValue", "booleanValue");
		verify(fieldToFieldCopyMode).copy("stringValue", "stringValue");
	}
	
	@Test
	public void shouldUseProvidedInstanceSupplierToCreateDstObject() {
		Supplier supplier = spy(new WithoutDefaultConstructorSupplier(LONG_VALUE));
		converter.addInstanceSupplier(WithoutDefaultConstructor.class, supplier);
		converter.setCopyMode("field", fieldToFieldCopyMode);
		WithoutDefaultConstructor src = new WithoutDefaultConstructor(LONG_VALUE + 10);
		WithoutDefaultConstructor result = (WithoutDefaultConstructor) converter.convert(src, WithoutDefaultConstructor.class);
		
		verify(supplier).get();
		verify(fieldToFieldCopyMode).copy("field", "field");
		assertThat(result, isProperlyCopied(src));
	}

	@Test
	public void shouldThrowExceptionWhenDstDoesNotHaveDefaultConstructorAndNoInstanceSupplierWasProvided() {
		thrown.expect(IllegalStateException.class);
		thrown.expectMessage("Cannot create instance of class");
		
		converter.setCopyMode("field", fieldToFieldCopyMode);
		WithoutDefaultConstructor src = new WithoutDefaultConstructor(LONG_VALUE + 10);
		converter.convert(src, WithoutDefaultConstructor.class);
	}

	@Test
	public void shouldUseProvidedInstanceSupplierInDeepCopyMode() {
		TestObject5 src = new TestObject5(new WithoutDefaultConstructor(LONG_VALUE + 10));

		Supplier supplier = spy(new WithoutDefaultConstructorSupplier(LONG_VALUE));
		converter.addInstanceSupplier(WithoutDefaultConstructor.class, supplier);
		converter.setCopyMode("field", fieldToFieldDeepCopyMode);
		converter.convert(src, TestObject5.class);

		verify(fieldToFieldDeepCopyMode).setObjectInstantiator(any(ObjectInstantiator.class));
		verify(fieldToFieldDeepCopyMode).setup(eq(src), any(TestObject5.class));
		verify(fieldToFieldDeepCopyMode).copy("field", "field");
		verify(supplier).get();
	}

	@Test
	public void shouldRetainNotCopiedFields() {
		CopyMode copyMode = mock(CopyMode.class);
		converter.setCopyMode("longValue", copyMode);
		converter.convert(testObject1, TestObject1.class);
		
		verify(copyMode).copy("longValue", "longValue");
		verify(copyMode, never()).copy("intValue", "intValue");
		verify(copyMode, never()).copy("doubleValue", "doubleValue");
		verify(copyMode, never()).copy("booleanValue", "booleanValue");
		verify(copyMode, never()).copy("stringValue", "stringValue");
	}

	@Test
	public void shouldReturnGivenDstInstance() {
		TestObject1 dst = new TestObject1();
		TestObject1 result = (TestObject1) converter.convert(testObject1, dst);
		assertThat(result, sameInstance(dst));
	}

	@Test
	public void shouldNotUseDefaultCopyModeIfExplicitCopyModeWasUsedBefore() {
		ShallowCopyMode explicitShallowCopyMode = mock(ShallowCopyMode.class);
		converter.setDefaultCopyMode(fieldToFieldCopyMode);
		converter.setCopyMode("testField", explicitShallowCopyMode);
		converter.convert(new ObjectWithLongField(LONG_VALUE), ObjectWithStringField.class);

		verify(explicitShallowCopyMode).copy("testField", "testField");
		verify(fieldToFieldCopyMode, never()).copy(Matchers.any(String.class), Matchers.any(String.class));
	}

	@Test
	public void shouldShallowCopyUsingSrcAccessibleChain() {
		TestObject3 src = new TestObject3(null, new TestObject2(INT_VALUE, DOUBLE_VALUE, STRING_VALUE));
		TestObject1 expected = new TestObject1(INT_VALUE, 0L, 0.0, false, null);
		converter.setCopyMode("testObject2.intValue", "intValue", fieldToFieldCopyMode);
		TestObject1 result = (TestObject1) converter.convert(src, TestObject1.class);
		
		assertThat(result, isProperlyCopied(expected));
	}

	@Test
	public void shouldShallowCopyUsingDstAccessibleChain() {
		TestObject3 src = new TestObject3(null, new TestObject2(INT_VALUE, DOUBLE_VALUE, STRING_VALUE));
		converter.setCopyMode("testObject2.intValue", "testObject1.intValue", fieldToFieldCopyMode);
		TestObject3 result = (TestObject3) converter.convert(src, TestObject3.class);

		TestObject3 expected = new TestObject3(new TestObject1(INT_VALUE, 0L, 0.0, false, null), null);
		assertThat(result, isProperlyCopied(expected));
	}

	@Test
	public void shouldIgnoreAccessibleWhenDstAccessibleChainIsUsed() {
		converter.ignore("testObject1");
		converter.setCopyMode("intValue", "testObject1.intValue", fieldToFieldCopyMode);
		converter.setCopyMode("stringValue", "testObject1.stringValue", fieldToFieldCopyMode);
		converter.setCopyMode("stringValue", "testObject2.stringValue", fieldToFieldCopyMode);
		TestObject3 result = (TestObject3) converter.convert(testObject1, TestObject3.class);
		
		TestObject3 expected = new TestObject3(null, new TestObject2(0, 0.0, STRING_VALUE));
		assertThat(result, isProperlyCopied(expected));
	}

	private <T> void testCopying(T expected, Class<T> dstClass, CopyMode copyMode) {
		converter.setCopyMode("testObject1", copyMode);
		T result = (T) converter.convert(testObject3, dstClass);
		assertCopying("testObject1", expected, dstClass, copyMode, result);
	}

	private <T> void testCopying(String dstAccessible, T expected, Class<T> dstClass, CopyMode copyMode) {
		converter.setCopyMode("testObject1", dstAccessible, copyMode);
		T result = (T) converter.convert(testObject3, dstClass);
		assertCopying(dstAccessible, expected, dstClass, copyMode, result);
	}

	private <T> void assertCopying(String dstAccessible, T expected, Class<T> dstClass, CopyMode copyMode, T result) {
		verify(copyMode).setup(eq(testObject3), any(dstClass));
		verify(copyMode).copy("testObject1", dstAccessible);
		assertThat(result, isProperlyCopied(expected));
	}

	private void testConversion(ConversionMode conversionMode) {
		ObjectWithLongField src = spy(new ObjectWithLongField(LONG_VALUE));
		TestObject2 dst = spy(new TestObject2());
		converter.setCopyMode("testField", "stringValue", conversionMode);
		converter.convert(src, dst);

		verify(conversionMode).setup(src, dst);
		verify(conversionMode).copy("testField", "stringValue");
		verify(ltsConverter).convert(LONG_VALUE, String.class);
	}
}
