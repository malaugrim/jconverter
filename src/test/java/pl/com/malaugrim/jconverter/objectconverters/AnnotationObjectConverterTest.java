package pl.com.malaugrim.jconverter.objectconverters;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.ArgumentCaptor;
import org.mockito.Matchers;
import pl.com.malaugrim.jconverter.TestConstants;
import pl.com.malaugrim.jconverter.core.copymode.ConversionMode;
import pl.com.malaugrim.jconverter.core.copymode.CopyMode;
import pl.com.malaugrim.jconverter.core.copymode.DeepCopyMode;
import pl.com.malaugrim.jconverter.core.copymode.ShallowCopyMode;
import pl.com.malaugrim.jconverter.testobjects.annotated.*;
import pl.com.malaugrim.jconverter.testobjects.simple.*;
import pl.com.malaugrim.jconverter.testtools.ReturnValueCaptor;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;

/**
 * @author Lukasz Tutka
 */
@SuppressWarnings("unchecked")
public class AnnotationObjectConverterTest implements TestConstants {
	
	@Rule
	public final ExpectedException thrown = ExpectedException.none();

	private AnnotationObjectConverter converter;
	private FlexibleObjectConverter flexibleObjectConverter;
	private TestObject1 testObject1;
	private TestObject1 testObject1Spy;
	private TestObject2 testObject2;
	private TestObject3 testObject3;

	@Before
	public void setUp() {
		converter = new AnnotationObjectConverter<>();
		flexibleObjectConverter = spy(FlexibleObjectConverter.class);
		converter.setFlexibleObjectConverter(flexibleObjectConverter);

		testObject1 = new TestObject1(INT_VALUE, LONG_VALUE, DOUBLE_VALUE, BOOLEAN_VALUE, STRING_VALUE);
		testObject1Spy = spy(testObject1);
		testObject2 = new TestObject2(INT_VALUE, DOUBLE_VALUE, STRING_VALUE);
		testObject3 = new TestObject3(testObject1, testObject2);
	}

	@Test
	public void shouldShallowCopyObjectWhenNoAnnotationsAreProvided() {
		Object result = converter.convert(testObject1, TestObject1.class);

		verifyThatShallowCopyModeWasSetAsDefaultMode(flexibleObjectConverter);
		Object flexibleObjectConverterResult = verifyConvertWasCalledOnFlexibleObjectConverter(testObject1);
		checkIfConversionResultTheSameAsResultOfFlexibleObjectConverter(result, flexibleObjectConverterResult, TestObject1.class);
		verifyNoCopyModeWasSet();
		assertThat(result, equalTo(testObject1));
	}

	@Test
	public void shouldShallowCopyFieldFromPointedSrcField() {
		Object result = converter.convert(testObject1, WithShallowCopyOnFieldPointingSrcField.class);

		ArgumentCaptor<CopyMode> captor = ArgumentCaptor.forClass(CopyMode.class);
		verify(flexibleObjectConverter).setCopyMode(Matchers.eq("stringValue"), Matchers.eq("field"), captor.capture());
		assertThat(captor.getValue(), instanceOf(ShallowCopyMode.class));

		assertThat(result, equalTo(new WithShallowCopyOnFieldPointingSrcField(STRING_VALUE)));
	}

	@Test
	public void shouldShallowCopyFieldFromPointedSrcProperty() {
		Object result = converter.convert(testObject1Spy, WithShallowCopyOnFieldPointingSrcProperty.class);

		ArgumentCaptor<CopyMode> captor = ArgumentCaptor.forClass(CopyMode.class);
		verify(flexibleObjectConverter).setCopyMode(Matchers.eq("stringValue"), Matchers.eq("field"), captor.capture());
		assertThat(captor.getValue(), instanceOf(ShallowCopyMode.class));

		assertThat(result, equalTo(new WithShallowCopyOnFieldPointingSrcProperty(STRING_VALUE)));
		verify(testObject1Spy).getStringValue();
	}

	@Test
	public void shouldShallowCopyPropertyFromPointedSrcField()  {
		Object result = converter.convert(testObject1Spy, WithShallowCopyOnPropertyPointingSrcField.class);
		
		ArgumentCaptor<CopyMode> captor = ArgumentCaptor.forClass(CopyMode.class);
		verify(flexibleObjectConverter).setCopyMode(Matchers.eq("stringValue"), Matchers.eq("field"), captor.capture());
		assertThat(captor.getValue(), instanceOf(ShallowCopyMode.class));

		verify(testObject1Spy, never()).getStringValue();
		assertThat(result, equalTo(new WithShallowCopyOnPropertyPointingSrcField(STRING_VALUE)));
	}

	@Test
	public void shouldShallowCopyPropertyFromPointedSrcProperty() {
		Object result = converter.convert(testObject1Spy, WithShallowCopyOnPropertyPointingSrcProperty.class);

		ArgumentCaptor<CopyMode> captor = ArgumentCaptor.forClass(CopyMode.class);
		verify(flexibleObjectConverter).setCopyMode(Matchers.eq("stringValue"), Matchers.eq("field"), captor.capture());
		assertThat(captor.getValue(), instanceOf(ShallowCopyMode.class));

		verify(testObject1Spy).getStringValue();
		assertThat(result, equalTo(new WithShallowCopyOnPropertyPointingSrcProperty(STRING_VALUE)));
	}

	@Test
	public void shouldDeepCopyAnnotatedField() {
		WithDeepCopyField src = new WithDeepCopyField(testObject1);
		testAnnotatedAccessible(src, src, "testObject1", DeepCopyMode.class);
	}

	@Test
	public void shouldDeepCopyAnnotatedPropertySetter() {
		WithDeepCopyProperty src = new WithDeepCopyProperty(testObject1);
		testAnnotatedAccessible(src, src, "testObject1", DeepCopyMode.class);
	}

	@Test
	public void shouldDeepCopyAnnotatedPropertyGetter() {
		WithDeepCopyProperty src = new WithDeepCopyProperty(testObject2);
		testAnnotatedAccessible(src, src, "testObject2", DeepCopyMode.class);
	}

	@Test
	public void shouldDeepCopyFieldFromPointedSrcField() {
		Object result = converter.convert(testObject3, WithDeepCopyOnFieldPointingSrcField.class);

		ArgumentCaptor<CopyMode> captor = ArgumentCaptor.forClass(CopyMode.class);
		verify(flexibleObjectConverter).setCopyMode(Matchers.eq("testObject2"), Matchers.eq("field"), captor.capture());
		assertThat(captor.getValue(), instanceOf(DeepCopyMode.class));

		assertThat(result, equalTo(new WithDeepCopyOnFieldPointingSrcField(testObject2)));
	}

	@Test
	public void shouldDeepCopyFieldFromPointedSrcProperty() {
		TestObject3 testObject3Spy = spy(testObject3);
		Object result = converter.convert(testObject3Spy, WithDeepCopyOnFieldPointingSrcProperty.class);

		ArgumentCaptor<CopyMode> captor = ArgumentCaptor.forClass(CopyMode.class);
		verify(flexibleObjectConverter).setCopyMode(Matchers.eq("testObject2"), Matchers.eq("field"), captor.capture());
		assertThat(captor.getValue(), instanceOf(DeepCopyMode.class));

		verify(testObject3Spy).getTestObject2();
		assertThat(result, equalTo(new WithDeepCopyOnFieldPointingSrcProperty(testObject2)));
	}

	@Test
	public void shouldDeepCopyPropertyFromPointedSrcField() {
		TestObject3 testObject3Spy = spy(testObject3);
		Object result = converter.convert(testObject3Spy, WithDeepCopyOnPropertyPointingSrcField.class);

		ArgumentCaptor<CopyMode> captor = ArgumentCaptor.forClass(CopyMode.class);
		verify(flexibleObjectConverter).setCopyMode(Matchers.eq("testObject2"), Matchers.eq("field"), captor.capture());
		assertThat(captor.getValue(), instanceOf(DeepCopyMode.class));

		verify(testObject3Spy, never()).getTestObject2();
		assertThat(result, equalTo(new WithDeepCopyOnPropertyPointingSrcField(testObject2)));
	}

	@Test
	public void shouldDeepCopyPropertyFromPointedSrcProperty() {
		TestObject3 testObject3Spy = spy(testObject3);
		Object result = converter.convert(testObject3Spy, WithDeepCopyOnPropertyPointingSrcProperty.class);

		ArgumentCaptor<CopyMode> captor = ArgumentCaptor.forClass(CopyMode.class);
		verify(flexibleObjectConverter).setCopyMode(Matchers.eq("testObject2"), Matchers.eq("field"), captor.capture());
		assertThat(captor.getValue(), instanceOf(DeepCopyMode.class));

		verify(testObject3Spy).getTestObject2();
		assertThat(result, equalTo(new WithDeepCopyOnPropertyPointingSrcProperty(testObject2)));
	}

	@Test
	public void shouldConvertAnnotatedField() {
		ObjectWithLongField src = new ObjectWithLongField(LONG_VALUE);
		WithConvertField expectedResult = new WithConvertField(String.valueOf(LONG_VALUE));
		testAnnotatedAccessible(src, expectedResult, "testField", ConversionMode.class);
	}

	@Test
	public void shouldConvertAnnotatedPropertyGetter() {
		ObjectWithLongField src = new ObjectWithLongField(LONG_VALUE);
		WithConvertPropertyGetter expectedResult = new WithConvertPropertyGetter(String.valueOf(LONG_VALUE));
		testAnnotatedAccessible(src, expectedResult, "testField", ConversionMode.class);
	}

	@Test
	public void shouldConvertAnnotatedPropertySetter() {
		ObjectWithLongField src = new ObjectWithLongField(LONG_VALUE);
		WithConvertPropertySetter expectedResult = new WithConvertPropertySetter(String.valueOf(LONG_VALUE));
		testAnnotatedAccessible(src, expectedResult, "testField", ConversionMode.class);
	}

	@Test
	public void shouldConvertFieldFromPointedSrcField() {
		Object result = converter.convert(testObject1Spy, WithConvertOnFieldPointingSrcField.class);

		ArgumentCaptor<CopyMode> captor = ArgumentCaptor.forClass(CopyMode.class);
		verify(flexibleObjectConverter).setCopyMode(Matchers.eq("longValue"), Matchers.eq("field"), captor.capture());
		assertThat(captor.getValue(), instanceOf(ConversionMode.class));

		verify(testObject1Spy, never()).getStringValue();
		verify(testObject1Spy, never()).getLongValue();
		assertThat(result, equalTo(new WithConvertOnFieldPointingSrcField(String.valueOf(LONG_VALUE))));
	}

	@Test
	public void shouldConvertFieldFromPointedSrcProperty() {
		Object result = converter.convert(testObject1Spy, WithConvertOnFieldPointingSrcProperty.class);

		ArgumentCaptor<CopyMode> captor = ArgumentCaptor.forClass(CopyMode.class);
		verify(flexibleObjectConverter).setCopyMode(Matchers.eq("longValue"), Matchers.eq("field"), captor.capture());
		assertThat(captor.getValue(), instanceOf(ConversionMode.class));

		verify(testObject1Spy).getLongValue();
		assertThat(result, equalTo(new WithConvertOnFieldPointingSrcProperty(String.valueOf(LONG_VALUE))));
	}

	@Test
	public void shouldConvertPropertyFromPointedSrcField() {
		Object result = converter.convert(testObject1Spy, WithConvertOnPropertyPointingSrcField.class);

		ArgumentCaptor<CopyMode> captor = ArgumentCaptor.forClass(CopyMode.class);
		verify(flexibleObjectConverter).setCopyMode(Matchers.eq("longValue"), Matchers.eq("field"), captor.capture());
		assertThat(captor.getValue(), instanceOf(ConversionMode.class));

		verify(testObject1Spy, never()).getLongValue();
		assertThat(result, equalTo(new WithConvertOnPropertyPointingSrcField(String.valueOf(LONG_VALUE))));
	}

	@Test
	public void shouldConvertPropertyFromPointedSrcProperty() {
		Object result = converter.convert(testObject1Spy, WithConvertOnPropertyPointingSrcProperty.class);

		ArgumentCaptor<CopyMode> captor = ArgumentCaptor.forClass(CopyMode.class);
		verify(flexibleObjectConverter).setCopyMode(Matchers.eq("longValue"), Matchers.eq("field"), captor.capture());
		assertThat(captor.getValue(), instanceOf(ConversionMode.class));

		verify(testObject1Spy).getLongValue();
		assertThat(result, equalTo(new WithConvertOnPropertyPointingSrcProperty(String.valueOf(LONG_VALUE))));
	}

	@Test
	public void shouldIgnoreAnnotatedField() {
		WithIgnoredField src = new WithIgnoredField(STRING_VALUE, LONG_VALUE);
		Object result = converter.convert(src, WithIgnoredField.class);
		assertThat(result, equalTo(new WithIgnoredField(null, LONG_VALUE)));
	}

	@Test
	public void shouldIgnoreAnnotatedPropertyGetter() {
		WithIgnoredPropertyGetter src = new WithIgnoredPropertyGetter(STRING_VALUE, LONG_VALUE);
		Object result = converter.convert(src, WithIgnoredPropertyGetter.class);
		assertThat(result, equalTo(new WithIgnoredPropertyGetter(null, LONG_VALUE)));
	}

	@Test
	public void shouldIgnoreAnnotatedPropertySetter() {
		WithIgnoredPropertySetter src = new WithIgnoredPropertySetter(STRING_VALUE, LONG_VALUE);
		Object result = converter.convert(src, WithIgnoredPropertySetter.class);
		assertThat(result, equalTo(new WithIgnoredPropertySetter(null, LONG_VALUE)));
	}

	@Test
	public void shouldIgnoreFieldAndPropertyWhenTheyHaveDifferentNames() {
		ObjectWithFieldNameDifferentFromMethodName src = new ObjectWithFieldNameDifferentFromMethodName(STRING_VALUE, LONG_VALUE);
		ObjectWithFieldNameDifferentFromMethodName expected = new ObjectWithFieldNameDifferentFromMethodName(null, LONG_VALUE);
		Object result = converter.convert(src, ObjectWithFieldNameDifferentFromMethodName.class);
		assertThat(result, equalTo(expected));
	}

	@Test
	public void shouldUseProvidedInstanceSupplierToCreateDstObject() {
		ReturnValueCaptor<WithoutDefaultConstructor> captor = new ReturnValueCaptor<>();
		WithoutDefaultConstructorSupplier supplier = spy(new WithoutDefaultConstructorSupplier(LONG_VALUE + 10));
		when(supplier.get()).thenAnswer(captor);
		
		converter.addInstanceSupplier(WithoutDefaultConstructor.class, supplier);
		Object result = converter.convert(new WithoutDefaultConstructor(LONG_VALUE), WithoutDefaultConstructor.class);

		verify(supplier).get();
		assertThat(result, equalTo(captor.getReturnValue()));
	}

	@Test
	public void shouldThrowExceptionWhenDstDoesNotHaveDefaultConstructorAndNoInstanceSupplierWasProvided() {
		thrown.expect(IllegalStateException.class);
		thrown.expectMessage("Cannot create instance of class");

		WithoutDefaultConstructor src = new WithoutDefaultConstructor(LONG_VALUE + 10);
		converter.convert(src, WithoutDefaultConstructor.class);
	}

	@Test
	public void shouldRetainNotCopiedFields() {
		WithIgnoredField dst = new WithIgnoredField("", 0L);
		Object result = converter.convert(new WithIgnoredField(STRING_VALUE, LONG_VALUE), dst);
		
		assertThat(result, equalTo(new WithIgnoredField("", LONG_VALUE)));
	}

	@Test
	public void shouldReturnGivenDstInstance() {
		TestObject1 dst = new TestObject1();
		TestObject1 result = (TestObject1) converter.convert(testObject1, dst);
		assertThat(result, sameInstance(dst));
	}

	private <T> void testAnnotatedAccessible(Object src, T expectedResult, String accessibleName, Class<? extends CopyMode> copyMode) {
		Object result = converter.convert(src, expectedResult.getClass());
		Object flexibleObjectConverterResult = verifyConvertWasCalledOnFlexibleObjectConverter(src);

		ArgumentCaptor<CopyMode> captor = ArgumentCaptor.forClass(CopyMode.class);
		verify(flexibleObjectConverter).setCopyMode(Matchers.eq(accessibleName), Matchers.eq(accessibleName), captor.capture());
		assertThat(captor.getValue(), instanceOf(copyMode));
		checkIfConversionResultTheSameAsResultOfFlexibleObjectConverter(result, flexibleObjectConverterResult, expectedResult.getClass());
		assertThat(result, equalTo(expectedResult));
	}

	private void verifyThatShallowCopyModeWasSetAsDefaultMode(FlexibleObjectConverter flexibleObjectConverter) {
		ArgumentCaptor<CopyMode> copyModeCaptor = ArgumentCaptor.forClass(CopyMode.class);
		verify(flexibleObjectConverter).setDefaultCopyMode(copyModeCaptor.capture());
		assertThat(copyModeCaptor.getValue(), instanceOf(ShallowCopyMode.class));
	}

	private void checkIfConversionResultTheSameAsResultOfFlexibleObjectConverter(Object annotationObjectConverterResult, 
																				 Object flexibleObjectConverterResult,
																				 Class<?> expectedResultType) {
		assertThat(flexibleObjectConverterResult, instanceOf(expectedResultType));
		assertThat(annotationObjectConverterResult, equalTo(flexibleObjectConverterResult));
	}

	@SuppressWarnings("unchecked")
	private <T> Object verifyConvertWasCalledOnFlexibleObjectConverter(T srcArg) {
		ArgumentCaptor<Object> dstCaptor = ArgumentCaptor.forClass(Object.class);
		verify(flexibleObjectConverter).convert(Matchers.eq(srcArg), dstCaptor.capture());
		return dstCaptor.getValue();
	}

	private void verifyNoCopyModeWasSet() {
		verify(flexibleObjectConverter, never()).setCopyMode(Matchers.any(), Matchers.any());
		verify(flexibleObjectConverter, never()).setCopyMode(Matchers.any(), Matchers.any(), Matchers.any());
	}

}
