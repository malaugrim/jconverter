package pl.com.malaugrim.jconverter.testobjects.simple;

/**
 * @author Lukasz Tutka
 */
public class WithTestObject1Setter {

	private TestObject1 field;

	public WithTestObject1Setter() {
	}

	public WithTestObject1Setter(TestObject1 testObject1) {
		field = testObject1;
	}

	public void setTestObject1(TestObject1 obj) {
		field = obj;
	}
	
	public TestObject1 getTestObject1() {
		return field;
	}
}
