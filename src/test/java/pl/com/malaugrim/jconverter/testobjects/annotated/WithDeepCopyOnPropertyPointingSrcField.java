package pl.com.malaugrim.jconverter.testobjects.annotated;

import java.util.Objects;

import com.google.common.base.MoreObjects;

import pl.com.malaugrim.jconverter.core.annotations.DeepCopy;
import pl.com.malaugrim.jconverter.testobjects.simple.TestObject2;

/**
 * @author Lukasz Tutka
 */
public class WithDeepCopyOnPropertyPointingSrcField {

	private TestObject2 field;

	public WithDeepCopyOnPropertyPointingSrcField() {
	}

	public WithDeepCopyOnPropertyPointingSrcField(TestObject2 field) {
		this.field = field;
	}

	public TestObject2 getField() {
		return field;
	}

	@DeepCopy(fromFieldChain = "testObject2")
	public void setField(TestObject2 field) {
		this.field = field;
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this)
				.add("field", field)
				.toString();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof WithDeepCopyOnPropertyPointingSrcField)) return false;
		WithDeepCopyOnPropertyPointingSrcField that = (WithDeepCopyOnPropertyPointingSrcField) o;
		return Objects.equals(field, that.field);
	}

	@Override
	public int hashCode() {
		return Objects.hash(field);
	}
}
