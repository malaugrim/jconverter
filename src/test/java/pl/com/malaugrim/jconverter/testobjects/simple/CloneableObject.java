package pl.com.malaugrim.jconverter.testobjects.simple;

import com.google.common.base.Objects;

/**
 * @author Lukasz Tutka
 */
public class CloneableObject implements Cloneable {
	
	private Integer intValue;
	
	private Long longValue;

	public CloneableObject() {
	}

	public CloneableObject(Integer intValue, Long longValue) {
		this.intValue = intValue;
		this.longValue = longValue;
	}

	public Integer getIntValue() {
		return intValue;
	}

	public void setIntValue(Integer intValue) {
		this.intValue = intValue;
	}

	public Long getLongValue() {
		return longValue;
	}

	public void setLongValue(Long longValue) {
		this.longValue = longValue;
	}

	@SuppressWarnings("CloneDoesntCallSuperClone")
	@Override
	public Object clone() throws CloneNotSupportedException {
		return new CloneableObject(intValue, longValue);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof CloneableObject)) return false;
		CloneableObject that = (CloneableObject) o;
		return Objects.equal(intValue, that.intValue) &&
				Objects.equal(longValue, that.longValue);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(intValue, longValue);
	}
}
