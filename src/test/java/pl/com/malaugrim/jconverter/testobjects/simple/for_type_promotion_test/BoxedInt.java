package pl.com.malaugrim.jconverter.testobjects.simple.for_type_promotion_test;

import java.util.Objects;

import com.google.common.base.MoreObjects;

/**
 * @author Lukasz Tutka
 */
public class BoxedInt {

	private Integer field;

	public BoxedInt() {
	}

	public BoxedInt(Integer field) {
		this.field = field;
	}

	public Integer getField() {
		return field;
	}

	public void setField(Integer field) {
		this.field = field;
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this)
				.add("field", field)
				.toString();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof BoxedInt)) return false;
		BoxedInt boxedInt = (BoxedInt) o;
		return Objects.equals(field, boxedInt.field);
	}

	@Override
	public int hashCode() {
		return Objects.hash(field);
	}
}
