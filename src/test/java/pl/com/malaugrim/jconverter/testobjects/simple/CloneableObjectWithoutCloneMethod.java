package pl.com.malaugrim.jconverter.testobjects.simple;

import com.google.common.base.Objects;

/**
 * @author Lukasz Tutka
 */
public class CloneableObjectWithoutCloneMethod implements Cloneable {

	private Integer field;

	public CloneableObjectWithoutCloneMethod() {
	}

	@SuppressWarnings("SameParameterValue")
	public CloneableObjectWithoutCloneMethod(Integer field) {
		this.field = field;
	}

	public Integer getField() {
		return field;
	}

	public void setField(Integer field) {
		this.field = field;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof CloneableObjectWithoutCloneMethod)) return false;
		CloneableObjectWithoutCloneMethod that = (CloneableObjectWithoutCloneMethod) o;
		return Objects.equal(field, that.field);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(field);
	}
}
