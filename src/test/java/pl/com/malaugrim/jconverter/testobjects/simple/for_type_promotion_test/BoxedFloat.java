package pl.com.malaugrim.jconverter.testobjects.simple.for_type_promotion_test;

import com.google.common.base.MoreObjects;

import java.util.Objects;

/**
 * @author Lukasz Tutka
 */
public class BoxedFloat {

	private Float field;

	public BoxedFloat() {
	}

	public BoxedFloat(Float field) {
		this.field = field;
	}

	public Float getField() {
		return field;
	}

	public void setField(Float field) {
		this.field = field;
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this)
				.add("field", field)
				.toString();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof BoxedFloat)) return false;
		BoxedFloat that = (BoxedFloat) o;
		return Objects.equals(field, that.field);
	}

	@Override
	public int hashCode() {
		return Objects.hash(field);
	}
}
