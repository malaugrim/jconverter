package pl.com.malaugrim.jconverter.testobjects.simple.for_type_promotion_test;

import com.google.common.base.MoreObjects;

import java.util.Objects;

/**
 * @author Lukasz Tutka
 */
public class PrimitiveDouble {

	private double field;

	public PrimitiveDouble() {
	}

	public PrimitiveDouble(double field) {
		this.field = field;
	}

	public double getField() {
		return field;
	}

	public void setField(double field) {
		this.field = field;
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this)
				.add("field", field)
				.toString();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof PrimitiveDouble)) return false;
		PrimitiveDouble that = (PrimitiveDouble) o;
		return Double.compare(that.field, field) == 0;
	}

	@Override
	public int hashCode() {
		return Objects.hash(field);
	}
}
