package pl.com.malaugrim.jconverter.testobjects.simple.for_type_promotion_test;

import com.google.common.base.MoreObjects;

import java.util.Objects;

/**
 * @author Lukasz Tutka
 */
public class BoxedChar {

	private Character field;

	public BoxedChar() {
	}

	@SuppressWarnings("SameParameterValue")
	public BoxedChar(Character field) {
		this.field = field;
	}

	public Character getField() {
		return field;
	}

	public void setField(Character field) {
		this.field = field;
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this)
				.add("field", field)
				.toString();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof BoxedChar)) return false;
		BoxedChar boxedChar = (BoxedChar) o;
		return Objects.equals(field, boxedChar.field);
	}

	@Override
	public int hashCode() {
		return Objects.hash(field);
	}
}
