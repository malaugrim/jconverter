package pl.com.malaugrim.jconverter.testobjects.simple;

/**
 * Created by Lukasz Tutka
 */
public class SubClass extends SuperClass {

	private Double doubleValue;

	public SubClass() {
	}

	@SuppressWarnings("SameParameterValue")
	public SubClass(String stringValue, Long longValue, Double doubleValue) {
		super(stringValue, longValue);
		this.doubleValue = doubleValue;
	}

	public Double getDoubleValue() {
		return doubleValue;
	}

	public void setDoubleValue(Double doubleValue) {
		this.doubleValue = doubleValue;
	}
}
