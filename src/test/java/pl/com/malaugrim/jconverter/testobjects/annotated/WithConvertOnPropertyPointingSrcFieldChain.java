package pl.com.malaugrim.jconverter.testobjects.annotated;

import com.google.common.base.MoreObjects;
import pl.com.malaugrim.jconverter.core.annotations.Convert;
import pl.com.malaugrim.jconverter.testconverters.DummyConverter;

import java.util.Objects;

/**
 * @author Lukasz Tutka
 */
public class WithConvertOnPropertyPointingSrcFieldChain {

	private String field;

	public WithConvertOnPropertyPointingSrcFieldChain() {
	}

	public WithConvertOnPropertyPointingSrcFieldChain(String field) {
		this.field = field;
	}

	public String getField() {
		return field;
	}

	@Convert(value = DummyConverter.class, fromFieldChain = "testObject1.stringValue")
	public void setField(String field) {
		this.field = field;
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this)
				.add("field", field)
				.toString();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof WithConvertOnPropertyPointingSrcFieldChain)) return false;
		WithConvertOnPropertyPointingSrcFieldChain that = (WithConvertOnPropertyPointingSrcFieldChain) o;
		return Objects.equals(field, that.field);
	}

	@Override
	public int hashCode() {
		return Objects.hash(field);
	}
}
