package pl.com.malaugrim.jconverter.testobjects.annotated;

import com.google.common.base.MoreObjects;
import pl.com.malaugrim.jconverter.core.annotations.Ignore;

import java.util.Objects;

/**
 * @author Lukasz Tutka
 */
public class ObjectWithFieldNameDifferentFromMethodName {

	private String field;
	private Long longValue;

	public ObjectWithFieldNameDifferentFromMethodName() {
	}

	public ObjectWithFieldNameDifferentFromMethodName(String field, Long longValue) {
		this.field = field;
		this.longValue = longValue;
	}

	public void setTestField(String value) {
		this.field = value;
	}

	@Ignore("field")
	public String getTestField() {
		return field;
	}

	public Long getLongValue() {
		return longValue;
	}

	public void setLongValue(Long longValue) {
		this.longValue = longValue;
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this)
				.add("field", field)
				.add("longValue", longValue)
				.toString();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof ObjectWithFieldNameDifferentFromMethodName)) return false;
		ObjectWithFieldNameDifferentFromMethodName that = (ObjectWithFieldNameDifferentFromMethodName) o;
		return Objects.equals(field, that.field) &&
				Objects.equals(longValue, that.longValue);
	}

	@Override
	public int hashCode() {
		return Objects.hash(field, longValue);
	}
}
