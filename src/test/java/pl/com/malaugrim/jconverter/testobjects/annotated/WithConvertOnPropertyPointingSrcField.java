package pl.com.malaugrim.jconverter.testobjects.annotated;

import java.util.Objects;

import com.google.common.base.MoreObjects;

import pl.com.malaugrim.jconverter.core.annotations.Convert;
import pl.com.malaugrim.jconverter.testconverters.LongToStringConverter;

/**
 * @author Lukasz Tutka
 */
public class WithConvertOnPropertyPointingSrcField {

	private String field;

	public WithConvertOnPropertyPointingSrcField() {
	}

	public WithConvertOnPropertyPointingSrcField(String field) {
		this.field = field;
	}

	@Convert(fromFieldChain = "longValue", value = LongToStringConverter.class)
	public String getField() {
		return field;
	}

	public void setField(String field) {
		this.field = field;
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this)
				.add("field", field)
				.toString();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof WithConvertOnPropertyPointingSrcField)) return false;
		WithConvertOnPropertyPointingSrcField that = (WithConvertOnPropertyPointingSrcField) o;
		return Objects.equals(field, that.field);
	}

	@Override
	public int hashCode() {
		return Objects.hash(field);
	}
}
