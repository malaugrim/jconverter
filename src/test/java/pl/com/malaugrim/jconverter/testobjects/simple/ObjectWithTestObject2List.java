package pl.com.malaugrim.jconverter.testobjects.simple;

import java.util.Arrays;
import java.util.List;

/**
 * @author Lukasz Tutka
 */
public class ObjectWithTestObject2List {
	
	private List<TestObject2> listField;

	public ObjectWithTestObject2List() {
	}

	public ObjectWithTestObject2List(TestObject2... objects) {
		this(Arrays.asList(objects));
	}
	
	public ObjectWithTestObject2List(List<TestObject2> listField) {
		this.listField = listField;
	}

	public List<TestObject2> getListField() {
		return listField;
	}

	public void setListField(List<TestObject2> listField) {
		this.listField = listField;
	}
}
