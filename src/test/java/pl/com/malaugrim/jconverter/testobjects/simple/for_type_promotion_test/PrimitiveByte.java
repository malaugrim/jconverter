package pl.com.malaugrim.jconverter.testobjects.simple.for_type_promotion_test;

import com.google.common.base.MoreObjects;

import java.util.Objects;

/**
 * @author Lukasz Tutka
 */
public class PrimitiveByte {

	private byte field;

	public PrimitiveByte() {
	}

	@SuppressWarnings("SameParameterValue")
	public PrimitiveByte(byte field) {
		this.field = field;
	}

	public byte getField() {
		return field;
	}

	public void setField(byte field) {
		this.field = field;
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this)
				.add("field", field)
				.toString();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof PrimitiveByte)) return false;
		PrimitiveByte that = (PrimitiveByte) o;
		return field == that.field;
	}

	@Override
	public int hashCode() {
		return Objects.hash(field);
	}
}
