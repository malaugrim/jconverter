package pl.com.malaugrim.jconverter.testobjects.simple;

import com.google.common.base.Objects;

/**
 * @author Lukasz Tutka
 */
public class WithCloneableObject {

	private CloneableObject field;

	public WithCloneableObject() {
	}

	public WithCloneableObject(CloneableObject field) {
		this.field = field;
	}

	public CloneableObject getField() {
		return field;
	}

	public void setField(CloneableObject field) {
		this.field = field;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof WithCloneableObject)) return false;
		WithCloneableObject that = (WithCloneableObject) o;
		return Objects.equal(field, that.field);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(field);
	}
}
