package pl.com.malaugrim.jconverter.testobjects.simple.for_type_promotion_test;

import java.util.Objects;

import com.google.common.base.MoreObjects;

/**
 * @author Lukasz Tutka
 */
public class PrimitiveChar {

	private char field;

	public PrimitiveChar() {
	}

	@SuppressWarnings("SameParameterValue")
	public PrimitiveChar(char field) {
		this.field = field;
	}

	public char getField() {
		return field;
	}

	public void setField(char field) {
		this.field = field;
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this)
				.add("field", field)
				.toString();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof PrimitiveChar)) return false;
		PrimitiveChar that = (PrimitiveChar) o;
		return field == that.field;
	}

	@Override
	public int hashCode() {
		return Objects.hash(field);
	}
}
