package pl.com.malaugrim.jconverter.testobjects.simple.for_type_promotion_test;

import java.util.Objects;

import com.google.common.base.MoreObjects;

/**
 * @author Lukasz Tutka
 */
public class PrimitiveInt {

	private int field;

	public PrimitiveInt() {
	}

	public PrimitiveInt(int field) {
		this.field = field;
	}

	public int getField() {
		return field;
	}

	public void setField(int field) {
		this.field = field;
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this)
				.add("field", field)
				.toString();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof PrimitiveInt)) return false;
		PrimitiveInt that = (PrimitiveInt) o;
		return field == that.field;
	}

	@Override
	public int hashCode() {
		return Objects.hash(field);
	}
}
