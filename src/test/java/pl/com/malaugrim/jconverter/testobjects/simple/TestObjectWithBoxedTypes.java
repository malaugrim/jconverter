package pl.com.malaugrim.jconverter.testobjects.simple;

import java.util.Objects;

/**
 * @author Lukasz Tutka
 */
public class TestObjectWithBoxedTypes {

	private Integer intValue;
	private Long longValue;
	private Double doubleValue;
	private Boolean booleanValue;

	public TestObjectWithBoxedTypes() {
	}

	public TestObjectWithBoxedTypes(Integer intValue, Long longValue, Double doubleValue, Boolean booleanValue) {
		this.intValue = intValue;
		this.longValue = longValue;
		this.doubleValue = doubleValue;
		this.booleanValue = booleanValue;
	}

	public Integer getIntValue() {
		return intValue;
	}

	public void setIntValue(Integer intValue) {
		this.intValue = intValue;
	}

	public Long getLongValue() {
		return longValue;
	}

	public void setLongValue(Long longValue) {
		this.longValue = longValue;
	}

	public Double getDoubleValue() {
		return doubleValue;
	}

	public void setDoubleValue(Double doubleValue) {
		this.doubleValue = doubleValue;
	}

	public Boolean getBooleanValue() {
		return booleanValue;
	}

	public void setBooleanValue(Boolean booleanValue) {
		this.booleanValue = booleanValue;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof TestObjectWithBoxedTypes)) return false;
		TestObjectWithBoxedTypes that = (TestObjectWithBoxedTypes) o;
		return Objects.equals(intValue, that.intValue) &&
				Objects.equals(longValue, that.longValue) &&
				Objects.equals(doubleValue, that.doubleValue) &&
				Objects.equals(booleanValue, that.booleanValue);
	}

	@Override
	public int hashCode() {
		return Objects.hash(intValue, longValue, doubleValue, booleanValue);
	}
}
