package pl.com.malaugrim.jconverter.testobjects.simple;

import com.google.common.base.Objects;

/**
 * @author Lukasz Tutka
 */
public class TestObject5 {

	private WithoutDefaultConstructor field;


	public TestObject5() {
	}

	public TestObject5(WithoutDefaultConstructor field) {
		this.field = field;
	}

	public WithoutDefaultConstructor getField() {
		return field;
	}

	public void setField(WithoutDefaultConstructor field) {
		this.field = field;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof TestObject5)) return false;
		TestObject5 that = (TestObject5) o;
		return Objects.equal(field, that.field);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(field);
	}
}
