package pl.com.malaugrim.jconverter.testobjects.simple;

import com.google.common.base.Objects;

/**
 * @author Lukasz Tutka
 */
public class WithCloneableObjectWithoutCloneMethod {

	private CloneableObjectWithoutCloneMethod field;

	public WithCloneableObjectWithoutCloneMethod() {
	}

	public WithCloneableObjectWithoutCloneMethod(CloneableObjectWithoutCloneMethod field) {
		this.field = field;
	}

	public CloneableObjectWithoutCloneMethod getField() {
		return field;
	}

	public void setField(CloneableObjectWithoutCloneMethod field) {
		this.field = field;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof WithCloneableObjectWithoutCloneMethod)) return false;
		WithCloneableObjectWithoutCloneMethod that = (WithCloneableObjectWithoutCloneMethod) o;
		return Objects.equal(field, that.field);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(field);
	}
}
