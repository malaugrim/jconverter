package pl.com.malaugrim.jconverter.testobjects.simple.for_type_promotion_test;

import com.google.common.base.MoreObjects;

import java.util.Objects;

/**
 * @author Lukasz Tutka
 */
public class BoxedLong {

	private Long field;

	public BoxedLong() {
	}

	public BoxedLong(Long field) {
		this.field = field;
	}

	public Long getField() {
		return field;
	}

	public void setField(Long field) {
		this.field = field;
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this)
				.add("field", field)
				.toString();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof BoxedLong)) return false;
		BoxedLong boxedLong = (BoxedLong) o;
		return Objects.equals(field, boxedLong.field);
	}

	@Override
	public int hashCode() {
		return Objects.hash(field);
	}
}
