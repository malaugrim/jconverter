package pl.com.malaugrim.jconverter.testobjects.simple;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;

/**
 * @author Lukasz Tutka
 */
public class TestObject3 {
	
	private TestObject1 testObject1;
	private TestObject2 testObject2;

	public TestObject3() {
	}

	public TestObject3(TestObject1 testObject1, TestObject2 testObject2) {
		this.testObject1 = testObject1;
		this.testObject2 = testObject2;
	}

	public TestObject1 getTestObject1() {
		return testObject1;
	}

	public void setTestObject1(TestObject1 testObject1) {
		this.testObject1 = testObject1;
	}

	@SuppressWarnings("UnusedReturnValue")
	public TestObject2 getTestObject2() {
		return testObject2;
	}

	public void setTestObject2(TestObject2 testObject2) {
		this.testObject2 = testObject2;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof TestObject3)) return false;
		TestObject3 that = (TestObject3) o;
		return Objects.equal(testObject1, that.testObject1) &&
				Objects.equal(testObject2, that.testObject2);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(testObject1, testObject2);
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this)
				.add("testObject1", testObject1)
				.add("testObject2", testObject2)
				.toString();
	}
}
