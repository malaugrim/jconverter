package pl.com.malaugrim.jconverter.testobjects.simple;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;

/**
 * Created by Lukasz Tutka
 */
@SuppressWarnings("ALL")
public class TestObject2 {

	private int intValue;
	private double doubleValue;
	private String stringValue;

	public TestObject2() {
	}

	public TestObject2(int intValue, double doubleValue, String stringValue) {
		this.intValue = intValue;
		this.doubleValue = doubleValue;
		this.stringValue = stringValue;
	}

	public void setIntValue(int intValue) {
		this.intValue = intValue;
	}

	public int getIntValue() {
		return intValue;
	}

	public void setDoubleValue(double doubleValue) {
		this.doubleValue = doubleValue;
	}

	public double getDoubleValue() {
		return doubleValue;
	}

	public void setStringValue(String stringValue) {
		this.stringValue = stringValue;
	}

	public String getStringValue() {
		return stringValue;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof TestObject2)) return false;
		TestObject2 that = (TestObject2) o;
		return intValue == that.intValue &&
				Double.compare(that.doubleValue, doubleValue) == 0 &&
				Objects.equal(stringValue, that.stringValue);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(intValue, doubleValue, stringValue);
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this)
				.add("intValue", intValue)
				.add("doubleValue", doubleValue)
				.add("stringValue", stringValue)
				.toString();
	}
}
