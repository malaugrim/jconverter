package pl.com.malaugrim.jconverter.testobjects.annotated;

import pl.com.malaugrim.jconverter.core.annotations.Convert;
import pl.com.malaugrim.jconverter.testconverters.LongToStringConverter;

/**
 * @author Lukasz Tutka
 */
public class WithConvertPropertySetter {

	private String field1;

	public WithConvertPropertySetter() {
	}

	public WithConvertPropertySetter(String field) {
		this.field1 = field;
	}

	public String getTestField() {
		return field1;
	}

	@Convert(LongToStringConverter.class)
	public void setTestField(String value) {
		this.field1 = value;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof WithConvertPropertySetter)) return false;

		WithConvertPropertySetter that = (WithConvertPropertySetter) o;
		return !(field1 != null ? !field1.equals(that.field1) : that.field1 != null);

	}

	@Override
	public int hashCode() {
		return field1 != null ? field1.hashCode() : 0;
	}
}
