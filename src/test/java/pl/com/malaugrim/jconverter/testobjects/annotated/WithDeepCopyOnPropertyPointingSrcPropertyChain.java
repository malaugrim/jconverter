package pl.com.malaugrim.jconverter.testobjects.annotated;

import com.google.common.base.MoreObjects;
import pl.com.malaugrim.jconverter.core.annotations.DeepCopy;

import java.util.Objects;

/**
 * @author Lukasz Tutka
 */
public class WithDeepCopyOnPropertyPointingSrcPropertyChain {

	private String field;

	public WithDeepCopyOnPropertyPointingSrcPropertyChain() {
	}

	public WithDeepCopyOnPropertyPointingSrcPropertyChain(String field) {
		this.field = field;
	}

	public String getField() {
		return field;
	}

	@DeepCopy(fromPropertyChain = "testObject1.stringValue")
	public void setField(String field) {
		this.field = field;
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this)
				.add("field", field)
				.toString();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof WithDeepCopyOnPropertyPointingSrcPropertyChain)) return false;
		WithDeepCopyOnPropertyPointingSrcPropertyChain that = (WithDeepCopyOnPropertyPointingSrcPropertyChain) o;
		return Objects.equals(field, that.field);
	}

	@Override
	public int hashCode() {
		return Objects.hash(field);
	}
}
