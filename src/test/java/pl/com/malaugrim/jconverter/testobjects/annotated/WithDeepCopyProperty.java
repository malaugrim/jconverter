package pl.com.malaugrim.jconverter.testobjects.annotated;

import pl.com.malaugrim.jconverter.core.annotations.DeepCopy;
import pl.com.malaugrim.jconverter.testobjects.simple.TestObject1;
import pl.com.malaugrim.jconverter.testobjects.simple.TestObject2;

/**
 * @author Lukasz Tutka
 */
public class WithDeepCopyProperty {

	private TestObject1 obj1;
	private TestObject2 obj2;



	public WithDeepCopyProperty() {
	}

	public WithDeepCopyProperty(TestObject1 obj1) {
		this.obj1 = obj1;
	}

	public WithDeepCopyProperty(TestObject2 obj2) {
		this.obj2 = obj2;
	}

	public TestObject1 getTestObject1() {
		return obj1;
	}

	@DeepCopy
	public void setTestObject1(TestObject1 testObject1) {
		this.obj1 = testObject1;
	}

	@DeepCopy
	public TestObject2 getTestObject2() {
		return obj2;
	}

	public void setTestObject2(TestObject2 obj) {
		this.obj2 = obj;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof WithDeepCopyProperty)) return false;

		WithDeepCopyProperty that = (WithDeepCopyProperty) o;

		if (obj1 != null ? !obj1.equals(that.obj1) : that.obj1 != null) return false;
		return !(obj2 != null ? !obj2.equals(that.obj2) : that.obj2 != null);

	}

	@Override
	public int hashCode() {
		int result = obj1 != null ? obj1.hashCode() : 0;
		result = 31 * result + (obj2 != null ? obj2.hashCode() : 0);
		return result;
	}
}
