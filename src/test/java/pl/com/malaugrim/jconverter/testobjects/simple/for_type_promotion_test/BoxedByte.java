package pl.com.malaugrim.jconverter.testobjects.simple.for_type_promotion_test;

import com.google.common.base.MoreObjects;

import java.util.Objects;

/**
 * @author Lukasz Tutka
 */
public class BoxedByte {

	private Byte field;

	public BoxedByte() {
	}

	@SuppressWarnings("SameParameterValue")
	public BoxedByte(Byte field) {
		this.field = field;
	}

	public Byte getField() {
		return field;
	}

	public void setField(Byte field) {
		this.field = field;
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this)
				.add("field", field)
				.toString();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof BoxedByte)) return false;
		BoxedByte boxedByte = (BoxedByte) o;
		return Objects.equals(field, boxedByte.field);
	}

	@Override
	public int hashCode() {
		return Objects.hash(field);
	}
}
