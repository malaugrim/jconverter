package pl.com.malaugrim.jconverter.testobjects.simple.for_type_promotion_test;

import com.google.common.base.MoreObjects;

import java.util.Objects;

/**
 * @author Lukasz Tutka
 */
public class BoxedDouble {

	private Double field;

	public BoxedDouble() {
	}

	public BoxedDouble(Double field) {
		this.field = field;
	}

	public Double getField() {
		return field;
	}

	public void setField(Double field) {
		this.field = field;
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this)
				.add("field", field)
				.toString();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof BoxedDouble)) return false;
		BoxedDouble that = (BoxedDouble) o;
		return Objects.equals(field, that.field);
	}

	@Override
	public int hashCode() {
		return Objects.hash(field);
	}
}
