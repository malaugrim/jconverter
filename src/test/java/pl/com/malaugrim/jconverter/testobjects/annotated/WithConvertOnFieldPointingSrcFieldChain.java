package pl.com.malaugrim.jconverter.testobjects.annotated;

import com.google.common.base.MoreObjects;
import pl.com.malaugrim.jconverter.core.annotations.Convert;
import pl.com.malaugrim.jconverter.testconverters.DummyConverter;

import java.util.Objects;

/**
 * @author Lukasz Tutka
 */
public class WithConvertOnFieldPointingSrcFieldChain {

	@Convert(value = DummyConverter.class, fromFieldChain = "testObject1.stringValue")
	private String field;

	public WithConvertOnFieldPointingSrcFieldChain() {
	}

	public WithConvertOnFieldPointingSrcFieldChain(String field) {
		this.field = field;
	}

	public String getField() {
		return field;
	}

	public void setField(String field) {
		this.field = field;
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this)
				.add("field", field)
				.toString();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof WithConvertOnFieldPointingSrcFieldChain)) return false;
		WithConvertOnFieldPointingSrcFieldChain that = (WithConvertOnFieldPointingSrcFieldChain) o;
		return Objects.equals(field, that.field);
	}

	@Override
	public int hashCode() {
		return Objects.hash(field);
	}
}
