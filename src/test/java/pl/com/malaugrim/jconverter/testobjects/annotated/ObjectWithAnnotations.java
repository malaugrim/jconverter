package pl.com.malaugrim.jconverter.testobjects.annotated;

import pl.com.malaugrim.jconverter.core.TypeConverter;
import pl.com.malaugrim.jconverter.core.annotations.Convert;
import pl.com.malaugrim.jconverter.core.annotations.DeepCopy;
import pl.com.malaugrim.jconverter.core.annotations.Ignore;
import pl.com.malaugrim.jconverter.core.annotations.ShallowCopy;
import pl.com.malaugrim.jconverter.testconverters.LongToStringConverter;
import pl.com.malaugrim.jconverter.testobjects.simple.TestObject1;

/**
 * @author Lukasz Tutka
 */
@SuppressWarnings({"unused", "EmptyMethod", "SameReturnValue"})
public class ObjectWithAnnotations {

	@ShallowCopy
	private Short shortValue;
	
	@Ignore
	private Integer intValue;
	
	@DeepCopy
	private TestObject1 testObject1;
	
	@Convert(LongToStringConverter.class)
	private String stringValue;
	
	@DeepCopy
	public Long getLongValue() {
		return 1L;
	}
	
	public void setLongValue(Long value) {

	}

	@ShallowCopy
	public void setSomeObject(Object object) {

	}

	@Ignore
	public Boolean isBooleanValue() {
		return true;
	}
	
	public void setBooleanValue(Boolean value) {
		
	}

	@Convert(TypeConverter.class)
	public void setDoubleValue(Double value) {
		
	}
}
