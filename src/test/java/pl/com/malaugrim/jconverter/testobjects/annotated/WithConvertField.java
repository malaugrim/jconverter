package pl.com.malaugrim.jconverter.testobjects.annotated;

import pl.com.malaugrim.jconverter.core.annotations.Convert;
import pl.com.malaugrim.jconverter.testconverters.LongToStringConverter;

/**
 * @author Lukasz Tutka
 */
public class WithConvertField {

	@Convert(LongToStringConverter.class)
	private String testField;


	public WithConvertField() {
	}

	public WithConvertField(String testField) {
		this.testField = testField;
	}

	public String getTestField() {
		return testField;
	}

	public void setTestField(String testField) {
		this.testField = testField;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof WithConvertField)) return false;

		WithConvertField that = (WithConvertField) o;

		return !(testField != null ? !testField.equals(that.testField) : that.testField != null);

	}

	@Override
	public int hashCode() {
		return testField != null ? testField.hashCode() : 0;
	}
}
