package pl.com.malaugrim.jconverter.testobjects.annotated;

import java.util.Objects;

import com.google.common.base.MoreObjects;

import pl.com.malaugrim.jconverter.core.annotations.DeepCopy;
import pl.com.malaugrim.jconverter.testobjects.simple.TestObject2;

/**
 * @author Lukasz Tutka
 */
public class WithDeepCopyOnFieldPointingSrcProperty {

	@DeepCopy(fromPropertyChain = "testObject2")
	private TestObject2 field;

	public WithDeepCopyOnFieldPointingSrcProperty() {
	}

	public WithDeepCopyOnFieldPointingSrcProperty(TestObject2 field) {
		this.field = field;
	}

	public TestObject2 getField() {
		return field;
	}

	public void setField(TestObject2 field) {
		this.field = field;
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this)
				.add("field", field)
				.toString();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof WithDeepCopyOnFieldPointingSrcProperty)) return false;
		WithDeepCopyOnFieldPointingSrcProperty that = (WithDeepCopyOnFieldPointingSrcProperty) o;
		return Objects.equals(field, that.field);
	}

	@Override
	public int hashCode() {
		return Objects.hash(field);
	}
}
