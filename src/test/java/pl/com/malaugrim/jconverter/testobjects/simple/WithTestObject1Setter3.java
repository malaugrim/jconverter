package pl.com.malaugrim.jconverter.testobjects.simple;

import com.google.common.base.Objects;

/**
 * @author Lukasz Tutka
 */
public class WithTestObject1Setter3 {

	private TestObject1 testObject1;

	public WithTestObject1Setter3() {
	}

	public WithTestObject1Setter3(TestObject1 testObject1) {
		this.testObject1 = testObject1;
	}

	public TestObject1 getTestObject1() {
		return testObject1;
	}

	public void setTestObject1(TestObject1 testObject1) {
		this.testObject1 = testObject1;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof WithTestObject1Setter3)) return false;
		WithTestObject1Setter3 that = (WithTestObject1Setter3) o;
		return Objects.equal(testObject1, that.testObject1);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(testObject1);
	}
}
