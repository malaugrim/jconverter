package pl.com.malaugrim.jconverter.testobjects.simple;

/**
 * Created by Lukasz Tutka
 */
@SuppressWarnings("unused")
public class ObjectWithStringField {

	private String testField;

	public ObjectWithStringField() {
	}

	public ObjectWithStringField(String testField) {
		this.testField = testField;
	}

	public String getTestField() {
		return testField;
	}

	public void setTestField(String testField) {
		this.testField = testField;
	}
}
