package pl.com.malaugrim.jconverter.testobjects.simple;

import java.util.function.Supplier;

/**
 * @author Lukasz Tutka
 */
public class WithoutDefaultConstructorSupplier implements Supplier<WithoutDefaultConstructor> {

	private final Long longValue;

	public WithoutDefaultConstructorSupplier(Long longValue) {
		this.longValue = longValue;
	}

	@Override
	public WithoutDefaultConstructor get() {
		return new WithoutDefaultConstructor(longValue);
	}
}
