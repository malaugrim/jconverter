package pl.com.malaugrim.jconverter.testobjects.simple;

/**
 * Created by Lukasz Tutka
 */
public class SuperClass extends SuperSuperClass {

	private String stringValue;

	public SuperClass(String stringValue, Long longValue) {
		super(longValue);
		this.stringValue = stringValue;
	}

	public SuperClass() {
	}

	public String getStringValue() {
		return stringValue;
	}

	public void setStringValue(String stringValue) {
		this.stringValue = stringValue;
	}
}
