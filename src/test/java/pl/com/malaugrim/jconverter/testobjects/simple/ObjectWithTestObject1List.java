package pl.com.malaugrim.jconverter.testobjects.simple;

import java.util.Arrays;
import java.util.List;

/**
 * @author Lukasz Tutka
 */
public class ObjectWithTestObject1List {

	private List<TestObject1> listField;

	public ObjectWithTestObject1List() {
	}

	public ObjectWithTestObject1List(TestObject1... objects) {
		this(Arrays.asList(objects));
	}

	public ObjectWithTestObject1List(List<TestObject1> listField) {
		this.listField = listField;
	}

	public List<TestObject1> getListField() {
		return listField;
	}

	public void setListField(List<TestObject1> listField) {
		this.listField = listField;
	}
}
