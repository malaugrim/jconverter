package pl.com.malaugrim.jconverter.testobjects.annotated;

import com.google.common.base.MoreObjects;
import pl.com.malaugrim.jconverter.core.annotations.Ignore;

import java.util.Objects;

/**
 * @author Lukasz Tutka
 */
public class WithIgnoredPropertySetter {

	private String stringValue;
	private Long field2;

	public WithIgnoredPropertySetter() {
	}

	public WithIgnoredPropertySetter(String stringValue, Long field2) {
		this.stringValue = stringValue;
		this.field2 = field2;
	}

	public String getStringValue() {
		return stringValue;
	}

	@Ignore
	public void setStringValue(String value) {
		this.stringValue = value;
	}

	public Long getLongValue() {
		return field2;
	}

	public void setLongValue(Long value) {
		this.field2 = value;
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this)
				.add("stringValue", stringValue)
				.add("field2", field2)
				.toString();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof WithIgnoredPropertySetter)) return false;
		WithIgnoredPropertySetter that = (WithIgnoredPropertySetter) o;
		return Objects.equals(stringValue, that.stringValue) &&
				Objects.equals(field2, that.field2);
	}

	@Override
	public int hashCode() {
		return Objects.hash(stringValue, field2);
	}
}
