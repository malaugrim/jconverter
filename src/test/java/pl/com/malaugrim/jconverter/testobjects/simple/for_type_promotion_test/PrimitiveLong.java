package pl.com.malaugrim.jconverter.testobjects.simple.for_type_promotion_test;

import com.google.common.base.MoreObjects;

import java.util.Objects;

/**
 * @author Lukasz Tutka
 */
public class PrimitiveLong {

	private long field;

	public PrimitiveLong() {
	}

	public PrimitiveLong(long field) {
		this.field = field;
	}

	public long getField() {
		return field;
	}

	public void setField(long field) {
		this.field = field;
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this)
				.add("field", field)
				.toString();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof PrimitiveLong)) return false;
		PrimitiveLong that = (PrimitiveLong) o;
		return field == that.field;
	}

	@Override
	public int hashCode() {
		return Objects.hash(field);
	}
}
