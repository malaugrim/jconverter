package pl.com.malaugrim.jconverter.testobjects.simple;

import com.google.common.base.MoreObjects;

/**
 * Created by Lukasz Tutka
 */
@SuppressWarnings("SameParameterValue")
public class ObjectWithLongField {

	private Long testField;

	public ObjectWithLongField() {
	}

	public ObjectWithLongField(Long testField) {
		this.testField = testField;
	}

	public Long getTestField() {
		return testField;
	}

	public void setTestField(Long testField) {
		this.testField = testField;
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this)
				.add("testField", testField)
				.toString();
	}
}
