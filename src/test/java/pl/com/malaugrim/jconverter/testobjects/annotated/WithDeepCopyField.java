package pl.com.malaugrim.jconverter.testobjects.annotated;

import com.google.common.base.Objects;

import pl.com.malaugrim.jconverter.core.annotations.DeepCopy;
import pl.com.malaugrim.jconverter.testobjects.simple.TestObject1;

/**
 * @author Lukasz Tutka
 */
public class WithDeepCopyField {

	@DeepCopy
	private TestObject1 testObject1;
	
	

	public WithDeepCopyField() {
	}

	public WithDeepCopyField(TestObject1 testObject1) {
		this.testObject1 = testObject1;
	}

	public TestObject1 getTestObject1() {
		return testObject1;
	}

	public void setTestObject1(TestObject1 testObject1) {
		this.testObject1 = testObject1;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof WithDeepCopyField)) return false;
		WithDeepCopyField that = (WithDeepCopyField) o;
		return Objects.equal(testObject1, that.testObject1);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(testObject1);
	}
}
