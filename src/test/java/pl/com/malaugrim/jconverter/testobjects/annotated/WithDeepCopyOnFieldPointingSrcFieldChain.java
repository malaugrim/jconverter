package pl.com.malaugrim.jconverter.testobjects.annotated;

import com.google.common.base.MoreObjects;
import pl.com.malaugrim.jconverter.core.annotations.DeepCopy;

import java.util.Objects;

/**
 * @author Lukasz Tutka
 */
public class WithDeepCopyOnFieldPointingSrcFieldChain {

	@DeepCopy(fromFieldChain = "testObject1.stringValue")
	private String field;

	public WithDeepCopyOnFieldPointingSrcFieldChain() {
	}

	public WithDeepCopyOnFieldPointingSrcFieldChain(String field) {
		this.field = field;
	}

	public String getField() {
		return field;
	}

	public void setField(String field) {
		this.field = field;
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this)
				.add("field", field)
				.toString();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof WithDeepCopyOnFieldPointingSrcFieldChain)) return false;
		WithDeepCopyOnFieldPointingSrcFieldChain that = (WithDeepCopyOnFieldPointingSrcFieldChain) o;
		return Objects.equals(field, that.field);
	}

	@Override
	public int hashCode() {
		return Objects.hash(field);
	}
}
