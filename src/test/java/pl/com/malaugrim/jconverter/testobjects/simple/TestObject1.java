package pl.com.malaugrim.jconverter.testobjects.simple;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;

/**
 * Created by Lukasz Tutka
 */
@SuppressWarnings("ALL")
public class TestObject1 {

	private int intValue;
	private long longValue;
	private double doubleValue;
	private boolean booleanValue;
	private String stringValue;

	public TestObject1() {
	}

	public TestObject1(long longValue) {
		this.longValue = longValue;
	}

	public TestObject1(int intValue, long longValue, double doubleValue, boolean booleanValue, String stringValue) {
		this.intValue = intValue;
		this.longValue = longValue;
		this.doubleValue = doubleValue;
		this.booleanValue = booleanValue;
		this.stringValue = stringValue;
	}

	public void setIntValue(int intValue) {
		this.intValue = intValue;
	}

	public int getIntValue() {
		return intValue;
	}

	public void setLongValue(long longValue) {
		this.longValue = longValue;
	}

	public long getLongValue() {
		return longValue;
	}

	public void setDoubleValue(double doubleValue) {
		this.doubleValue = doubleValue;
	}

	public double getDoubleValue() {
		return doubleValue;
	}

	public void setBooleanValue(boolean booleanValue) {
		this.booleanValue = booleanValue;
	}

	public boolean getBooleanValue() {
		return booleanValue;
	}

	public void setStringValue(String stringValue) {
		this.stringValue = stringValue;
	}

	public String getStringValue() {
		return stringValue;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof TestObject1)) return false;
		TestObject1 that = (TestObject1) o;
		return intValue == that.intValue &&
				longValue == that.longValue &&
				Double.compare(that.doubleValue, doubleValue) == 0 &&
				booleanValue == that.booleanValue &&
				Objects.equal(stringValue, that.stringValue);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(intValue, longValue, doubleValue, booleanValue, stringValue);
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this)
				.add("intValue", intValue)
				.add("longValue", longValue)
				.add("doubleValue", doubleValue)
				.add("booleanValue", booleanValue)
				.add("stringValue", stringValue)
				.toString();
	}
}
