package pl.com.malaugrim.jconverter.testobjects.simple;

import com.google.common.base.Objects;

/**
 * @author Lukasz Tutka
 */
public class WithTestObject1Getter {

	private TestObject1 object;

	public WithTestObject1Getter() {
	}

	public WithTestObject1Getter(TestObject1 object) {
		this.object = object;
	}

	@SuppressWarnings("UnusedReturnValue")
	public TestObject1 getObject() {
		return object;
	}

	public void setObject(TestObject1 object) {
		this.object = object;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof WithTestObject1Getter)) return false;
		WithTestObject1Getter that = (WithTestObject1Getter) o;
		return Objects.equal(object, that.object);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(object);
	}
}
