package pl.com.malaugrim.jconverter.testobjects.simple;

/**
 * @author Lukasz Tutka
 */
public class WithTestObject1Setter2 {

	private TestObject1 object;

	public WithTestObject1Setter2() {
	}

	public WithTestObject1Setter2(TestObject1 testObject1) {
		object = testObject1;
	}

	public TestObject1 getObject() {
		return object;
	}

	public void setObject(TestObject1 object) {
		this.object = object;
	}
}
