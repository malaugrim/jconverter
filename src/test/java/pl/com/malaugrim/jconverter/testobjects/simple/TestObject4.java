package pl.com.malaugrim.jconverter.testobjects.simple;

import com.google.common.base.Objects;

/**
 * @author Lukasz Tutka
 */
public class TestObject4 {

	private TestObject3 testObject3;

	public TestObject4() {
	}

	public TestObject4(TestObject3 testObject3) {
		this.testObject3 = testObject3;
	}

	public TestObject3 getTestObject3() {
		return testObject3;
	}

	public void setTestObject3(TestObject3 testObject3) {
		this.testObject3 = testObject3;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof TestObject4)) return false;
		TestObject4 that = (TestObject4) o;
		return Objects.equal(testObject3, that.testObject3);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(testObject3);
	}
}
