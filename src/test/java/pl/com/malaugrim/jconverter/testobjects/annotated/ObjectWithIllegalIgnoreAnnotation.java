package pl.com.malaugrim.jconverter.testobjects.annotated;

import com.google.common.base.MoreObjects;
import pl.com.malaugrim.jconverter.core.annotations.Ignore;

import java.util.Objects;

/**
 * @author Lukasz Tutka
 */
public class ObjectWithIllegalIgnoreAnnotation {

	private String value;

	public ObjectWithIllegalIgnoreAnnotation() {
	}

	public ObjectWithIllegalIgnoreAnnotation(String value) {
		this.value = value;
	}

	@Ignore("wrongField")
	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this)
				.add("value", value)
				.toString();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof ObjectWithIllegalIgnoreAnnotation)) return false;
		ObjectWithIllegalIgnoreAnnotation that = (ObjectWithIllegalIgnoreAnnotation) o;
		return Objects.equals(value, that.value);
	}

	@Override
	public int hashCode() {
		return Objects.hash(value);
	}
}
