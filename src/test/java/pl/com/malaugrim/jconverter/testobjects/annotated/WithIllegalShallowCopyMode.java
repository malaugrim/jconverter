package pl.com.malaugrim.jconverter.testobjects.annotated;

import pl.com.malaugrim.jconverter.core.annotations.ShallowCopy;

/**
 * @author Lukasz Tutka
 */
public class WithIllegalShallowCopyMode {

	@SuppressWarnings("unused")
	@ShallowCopy(fromFieldChain = "field", fromPropertyChain = "property")
	private String field;
}
