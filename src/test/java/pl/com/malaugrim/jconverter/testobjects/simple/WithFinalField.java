package pl.com.malaugrim.jconverter.testobjects.simple;

import com.google.common.base.Objects;

/**
 * @author Lukasz Tutka
 */
@SuppressWarnings("SameParameterValue")
public class WithFinalField {

	private final Integer intValue;
	
	private Double doubleValue;

	public WithFinalField(Integer intValue) {
		this.intValue = intValue;
	}

	public WithFinalField(Double doubleValue, Integer intValue) {
		this.doubleValue = doubleValue;
		this.intValue = intValue;
	}

	public Double getDoubleValue() {
		return doubleValue;
	}

	public void setDoubleValue(Double doubleValue) {
		this.doubleValue = doubleValue;
	}

	public Integer getIntValue() {
		return intValue;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof WithFinalField)) return false;
		WithFinalField that = (WithFinalField) o;
		return Objects.equal(intValue, that.intValue) &&
				Objects.equal(doubleValue, that.doubleValue);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(intValue, doubleValue);
	}
}
