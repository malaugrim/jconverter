package pl.com.malaugrim.jconverter.testobjects.simple.for_type_promotion_test;

import com.google.common.base.MoreObjects;

import java.util.Objects;

/**
 * @author Lukasz Tutka
 */
public class PrimitiveFloat {

	private float field;

	public PrimitiveFloat() {
	}

	public PrimitiveFloat(float field) {
		this.field = field;
	}

	public float getField() {
		return field;
	}

	public void setField(float field) {
		this.field = field;
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this)
				.add("field", field)
				.toString();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof PrimitiveFloat)) return false;
		PrimitiveFloat that = (PrimitiveFloat) o;
		return Float.compare(that.field, field) == 0;
	}

	@Override
	public int hashCode() {
		return Objects.hash(field);
	}
}
