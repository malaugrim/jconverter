package pl.com.malaugrim.jconverter.testobjects.simple.for_type_promotion_test;

import com.google.common.base.MoreObjects;

import java.util.Objects;

/**
 * @author Lukasz Tutka
 */
public class PrimitiveShort {

	private short field;

	public PrimitiveShort() {
	}

	public PrimitiveShort(short field) {
		this.field = field;
	}

	public short getField() {
		return field;
	}

	public void setField(short field) {
		this.field = field;
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this)
				.add("field", field)
				.toString();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof PrimitiveShort)) return false;
		PrimitiveShort that = (PrimitiveShort) o;
		return field == that.field;
	}

	@Override
	public int hashCode() {
		return Objects.hash(field);
	}
}
