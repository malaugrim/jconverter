package pl.com.malaugrim.jconverter.testobjects.simple;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;

/**
 * @author Lukasz Tutka
 */
public class WithoutDefaultConstructor {

	private Long field;

	public WithoutDefaultConstructor(Long field) {
		this.field = field;
	}

	public Long getField() {
		return field;
	}

	public void setField(Long field) {
		this.field = field;
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this)
				.add("field", field)
				.toString();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof WithoutDefaultConstructor)) return false;
		WithoutDefaultConstructor that = (WithoutDefaultConstructor) o;
		return Objects.equal(field, that.field);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(field);
	}
}
