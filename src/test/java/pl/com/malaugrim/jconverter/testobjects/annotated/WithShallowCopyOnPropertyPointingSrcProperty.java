package pl.com.malaugrim.jconverter.testobjects.annotated;

import java.util.Objects;

import com.google.common.base.MoreObjects;

import pl.com.malaugrim.jconverter.core.annotations.ShallowCopy;

/**
 * @author Lukasz Tutka
 */
public class WithShallowCopyOnPropertyPointingSrcProperty {
	
	private String field;

	public WithShallowCopyOnPropertyPointingSrcProperty() {
	}

	public WithShallowCopyOnPropertyPointingSrcProperty(String field) {
		this.field = field;
	}

	public String getField() {
		return field;
	}

	@ShallowCopy(fromPropertyChain = "stringValue")
	public void setField(String field) {
		this.field = field;
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this)
				.add("field", field)
				.toString();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof WithShallowCopyOnPropertyPointingSrcProperty)) return false;
		WithShallowCopyOnPropertyPointingSrcProperty that = (WithShallowCopyOnPropertyPointingSrcProperty) o;
		return Objects.equals(field, that.field);
	}

	@Override
	public int hashCode() {
		return Objects.hash(field);
	}
}
