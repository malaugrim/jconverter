package pl.com.malaugrim.jconverter.testobjects.simple;

/**
 * Created by Lukasz Tutka
 */
public class SuperSuperClass {

	private Long longValue;

	public SuperSuperClass(Long longValue) {
		this.longValue = longValue;
	}

	public SuperSuperClass() {
	}

	public Long getLongValue() {
		return longValue;
	}

	public void setLongValue(Long longValue) {
		this.longValue = longValue;
	}
}
