package pl.com.malaugrim.jconverter.testobjects.annotated;

import com.google.common.base.MoreObjects;
import pl.com.malaugrim.jconverter.core.annotations.Ignore;

import java.util.Objects;

/**
 * @author Lukasz Tutka
 */
public class WithIgnoredField {

	@Ignore
	private String stringValue;
	private Long longValue;

	public WithIgnoredField() {
	}

	public WithIgnoredField(String stringValue, Long longValue) {
		this.stringValue = stringValue;
		this.longValue = longValue;
	}

	public String getStringValue() {
		return stringValue;
	}

	public void setStringValue(String stringValue) {
		this.stringValue = stringValue;
	}

	public Long getLongValue() {
		return longValue;
	}

	public void setLongValue(Long longValue) {
		this.longValue = longValue;
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this)
				.add("stringValue", stringValue)
				.add("longValue", longValue)
				.toString();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof WithIgnoredField)) return false;
		WithIgnoredField that = (WithIgnoredField) o;
		return Objects.equals(stringValue, that.stringValue) &&
				Objects.equals(longValue, that.longValue);
	}

	@Override
	public int hashCode() {
		return Objects.hash(stringValue, longValue);
	}
}
