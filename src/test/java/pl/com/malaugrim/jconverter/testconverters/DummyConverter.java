package pl.com.malaugrim.jconverter.testconverters;

import pl.com.malaugrim.jconverter.core.TypeConverter;

/**
 * @author Lukasz Tutka
 */
public class DummyConverter implements TypeConverter {

	@Override
	public boolean canConvert(Class srcClass, Class dstClass) {
		return true;
	}

	@Override
	public Object convert(Object srcObj, Object dstObj) {
		return srcObj;
	}

	@Override
	public Object convert(Object srcObj, Class dstClass) {
		return srcObj;
	}
}
