package pl.com.malaugrim.jconverter.testconverters;

import pl.com.malaugrim.jconverter.core.TypeConverter;

/**
 * @author Lukasz Tutka
 */
public class IntToIntConverter implements TypeConverter<Integer, Integer> {
	@Override
	public Integer convert(Integer srcObj, Integer dstObj) {
		return srcObj + 10;
	}

	@Override
	public Integer convert(Integer srcObj, Class<? extends Integer> dstClass) {
		return srcObj + 10;
	}

	@Override
	public boolean canConvert(Class<?> srcClass, Class<?> dstClass) {
		return true;
	}
}
