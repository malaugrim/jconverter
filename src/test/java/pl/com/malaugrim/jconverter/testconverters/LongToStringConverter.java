package pl.com.malaugrim.jconverter.testconverters;


import pl.com.malaugrim.jconverter.core.TypeConverter;

/**
 * Created by Lukasz Tutka
 */
public class LongToStringConverter implements TypeConverter<Long, String> {

	private static final Class<Long> SRC_CLASS = Long.class;
	private static final Class<String> DST_CLASS = String.class;

	@Override
	public String convert(Long srcObj, Class<? extends String> dstClass) {
		return convert(srcObj, "");
	}

	@Override
	public String convert(Long src, String dst) {
		return String.valueOf(src);
	}

	@Override
	public boolean canConvert(Class<?> srcClass, Class<?> dstClass) {
		return srcClass.equals(SRC_CLASS) && dstClass.equals(DST_CLASS);
	}

}
