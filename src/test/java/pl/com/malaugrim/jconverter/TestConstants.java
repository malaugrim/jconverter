package pl.com.malaugrim.jconverter;

/**
 * @author Lukasz Tutka
 */
public interface TestConstants {

	char CHAR_VALUE = 'c';
	byte BYTE_VALUE = 6;
	short SHORT_VALUE = 4;
	int INT_VALUE = 2;
	long LONG_VALUE = 1L;
	float FLOAT_VALUE = 2.2f;
	double DOUBLE_VALUE = 3.2;
	boolean BOOLEAN_VALUE = true;
	String STRING_VALUE = "str";
}
