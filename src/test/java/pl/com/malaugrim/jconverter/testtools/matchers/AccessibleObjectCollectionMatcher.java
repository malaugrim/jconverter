package pl.com.malaugrim.jconverter.testtools.matchers;

import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;

import pl.com.malaugrim.jconverter.core.accessor.AccessibleObjectWrapper;

/**
 * @author Lukasz Tutka
 */
public class AccessibleObjectCollectionMatcher extends TypeSafeMatcher<Collection<AccessibleObjectWrapper>> {

	private final Collection<ExpectedAccessible> expectedAccessibles;
	private List<String> messages = new ArrayList<>();

	public AccessibleObjectCollectionMatcher(Collection<ExpectedAccessible> expectedAccessibles) {
		this.expectedAccessibles = expectedAccessibles;
	}

	@Override
	public void describeTo(Description description) {
		expectedAccessibles.forEach(expected -> description.appendText(expected.toString()).appendText("\n"));
	}

	@Override
	protected void describeMismatchSafely(Collection<AccessibleObjectWrapper> item, Description description) {
		if(messages.isEmpty()) {
			description.appendValueList("", "\n\n", "", expectedAccessibles);
		}

		messages.forEach(msg -> description.appendText(msg).appendText("\n"));
	}

	@Override
	protected boolean matchesSafely(Collection<AccessibleObjectWrapper> actual) {
		messages = new ArrayList<>();
		if(actual.isEmpty()) {
			messages.add("Actual accessible list is empty");
		}

		messages.addAll(
				expectedAccessibles.stream()
						.filter(expected -> !contains(actual, expected))
						.map(expected -> "Actual accessible list does not contain: " + expected)
						.collect(Collectors.toList()));

		messages.addAll(
				actual.stream()
						.filter(actualAccessible -> !contains(expectedAccessibles, actualAccessible))
						.map(actualAccessible -> "Actual result contains redundant accessible: " + accessibleToString(actualAccessible))
						.collect(Collectors.toList()));
		
		return messages.isEmpty();
	}

	private boolean contains(Collection<ExpectedAccessible> expected, AccessibleObjectWrapper actual) {
		for (ExpectedAccessible expectedAccessible : expected) {
			if(areEqual(actual, expectedAccessible)) {
				return true;
			}
		}
		
		return false;
	}

	private String accessibleToString(AccessibleObjectWrapper<?> accessible) {
		return "{ Name: " +accessible.getAccessibleName()+ ", type: " +accessible.getType()+ ", annotations: " +accessible.getAnnotations()+ " }";
	}

	private boolean contains(Collection<AccessibleObjectWrapper> accessibles, ExpectedAccessible expected) {
		if(accessibles == null) {
			return false;
		}

		for (AccessibleObjectWrapper accessible : accessibles) {
			if(areEqual(accessible, expected)) {
				return true;
			}
		}

		return false;
	}

	@SuppressWarnings("unchecked")
	private boolean areEqual(AccessibleObjectWrapper actual, ExpectedAccessible expected) {
		return Objects.equals(actual.getAccessibleName(), expected.getName()) &&
				Objects.equals(actual.getType(), expected.getType()) &&
				annotationsEqual(actual.getAnnotations(), expected.getAnnotations());
	}

	private boolean annotationsEqual(List<Annotation> actualAnnotations, List<Class<? extends Annotation>> expectedAnnotations) {
		if(actualAnnotations.size() != expectedAnnotations.size()) {
			return false;
		}

		for (Annotation actualAnnotation : actualAnnotations) {
			if(!expectedAnnotations.contains(actualAnnotation.annotationType())) {
				return false;
			}
		}

		return true;
	}
}
