package pl.com.malaugrim.jconverter.testtools.matchers;

import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author Lukasz Tutka
 */
public class CollectionsEqualNoOrder extends TypeSafeMatcher<Collection> {

	private final Collection expectedObjects;
	private List<String> messages = new ArrayList<>();

	public CollectionsEqualNoOrder(Collection expectedObjects) {
		this.expectedObjects = expectedObjects;
	}

	@Override
	public void describeTo(Description description) {
		description.appendText(expectedObjects.toString());
	}

	@Override
	protected void describeMismatchSafely(Collection item, Description description) {
		messages.forEach(msg -> description.appendText(msg). appendText("\n"));
	}

	@SuppressWarnings("unchecked")
	@Override
	protected boolean matchesSafely(Collection actualObjects) {
		messages = new ArrayList<>();
		expectedObjects.stream()
				.filter(expected -> !actualObjects.contains(expected))
				.forEach(expected -> messages.add("Missing object: " + expected));

		actualObjects.stream()
				.filter(actual -> !expectedObjects.contains(actual))
				.forEach(actual -> messages.add("Redundant object: " + actual));
		
		return messages.isEmpty();
	}
}
