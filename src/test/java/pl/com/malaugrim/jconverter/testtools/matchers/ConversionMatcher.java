package pl.com.malaugrim.jconverter.testtools.matchers;

import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;

import java.lang.reflect.Field;
import java.util.*;

/**
 * Created by Lukasz Tutka
 */
public class ConversionMatcher<S, D> extends TypeSafeMatcher<D> {

	private final Map<String, Object> incorrectFieldValues = new HashMap<>();
	private final Map<String, Object> expectedFieldsToValueMap;
	private final S expected;

	public ConversionMatcher(S expected) {
		this.expected = expected;
		this.expectedFieldsToValueMap = createFieldNameToValueMap(expected);
	}

	@Override
	public void describeTo(Description description) {
		Set<String> incorrectFieldNames = incorrectFieldValues.keySet();
		incorrectFieldNames.forEach(fieldName -> {
			Object expectedValue = expectedFieldsToValueMap.get(fieldName);
			description.appendText("\n").appendText(fieldName).appendText(" = ").appendValue(expectedValue);
		});
	}

	@Override
	protected boolean matchesSafely(D actual) {
		if(isSimpleObject(actual)) {
			return areSimpleObjectsEqual(actual);
		} else {
			return allCorrespondingFieldsHaveSameValue(actual);
		}
	}

	private boolean areSimpleObjectsEqual(D actual) {
		boolean equals = Objects.equals(expected, actual);
		if(!equals) {
			expectedFieldsToValueMap.clear();
			expectedFieldsToValueMap.put("value", expected);
			incorrectFieldValues.clear();
			incorrectFieldValues.put("value", actual);
		}
		
		return equals;
	}

	private boolean isSimpleObject(D actual) {
		if(actual == null) {
			return true;
		}
		
		Class<?> cl = actual.getClass();
		return cl.isPrimitive() ||
				Number.class.isAssignableFrom(cl);
	}

	@Override
	protected void describeMismatchSafely(D actual, Description description) {
		incorrectFieldValues.forEach((field, value) -> description.appendText("\n").appendText(field).appendText(" = ").appendValue(value));
	}

	private boolean allCorrespondingFieldsHaveSameValue(D actual) {
		Map<String, Object> actualFieldsToValue = createFieldNameToValueMap(actual);
		expectedFieldsToValueMap.forEach((field, value) ->
				storeFieldAndValueIfActualObjContainsFieldAndHasIncorrectValue(actualFieldsToValue, field, value));

		return incorrectFieldValues.isEmpty();
	}

	private Map<String, Object> createFieldNameToValueMap(Object object) {
		if(object == null) {
			return new HashMap<>();
		} else {
			return createFieldMapping(object);
		}
	}

	private Map<String, Object> createFieldMapping(Object object) {
		Map<String, Object> map = new HashMap<>();
		Field[] fields = getFieldsFromClassHierarchy(object.getClass());

		for (Field field : fields) {
			field.setAccessible(true);
			try {
				map.put(field.getName(), field.get(object));
			} catch (IllegalAccessException e) {
				throw new RuntimeException(e);
			}
		}

		return map;
	}

	private Field[] getFieldsFromClassHierarchy(Class<?> cl) {
		List<Field> fields = new ArrayList<>();
		for(Class<?> clazz = cl ; clazz != null ; clazz = clazz.getSuperclass()) {
			Field[] declaredFields = clazz.getDeclaredFields();
			fields.addAll(Arrays.asList(declaredFields));
		}

		return fields.toArray(new Field[fields.size()]);
	}

	private void storeFieldAndValueIfActualObjContainsFieldAndHasIncorrectValue(Map<String, Object> actualFieldsToValueMap,
	                                                                            String expectedField, Object expectedValue) {
		if(actualFieldsToValueMap.containsKey(expectedField)) {
			Object actualValue = actualFieldsToValueMap.get(expectedField);
			boolean equals = Objects.equals(expectedValue, actualValue);
			if(!equals) {
				incorrectFieldValues.put(expectedField, actualValue);
			}
		}
	}
}
