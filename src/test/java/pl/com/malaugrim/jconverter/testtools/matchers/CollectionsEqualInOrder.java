package pl.com.malaugrim.jconverter.testtools.matchers;

import java.util.*;

import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;

/**
 * @author Lukasz Tutka
 */
public class CollectionsEqualInOrder extends TypeSafeMatcher<Collection> {

	private static final String INVALID_INDEX_EX = "Invalid index %d]. Expected collection has only %d elements.";
	private final Collection expected;
	private Map<Integer, String> indexToMismatchDescription = new HashMap<>(); 

	public CollectionsEqualInOrder(Collection expected) {
		this.expected = expected;
	}
	
	@Override
	public void describeTo(Description description) {
		description.appendText(expected.toString());
	}

	@Override
	protected void describeMismatchSafely(Collection item, Description description) {
		indexToMismatchDescription.forEach((i, msg) -> description.appendText(msg).appendText("\n"));
	}

	@Override
	protected boolean matchesSafely(Collection collection) {
		indexToMismatchDescription = new HashMap<>();

		int index = 0;
		for (Object object : collection) {
			Object expectedObj = getExpectedObjectAt(index);
			if(!Objects.equals(object, expectedObj)) {
				indexToMismatchDescription.put(index, "At index: " +index+ "\n\texpected: " +expectedObj+ ",\n\tbut actual was: " +object);
			}
			++index;
		}
		
		return indexToMismatchDescription.isEmpty();
	}
	
	private Object getExpectedObjectAt(int index) {
		int i = 0;
		for (Object object : expected) {
			if(i++ == index) {
				return object;
			}
		}

		throw new IndexOutOfBoundsException(String.format(INVALID_INDEX_EX, index, expected.size()));
	}
}
