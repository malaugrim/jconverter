package pl.com.malaugrim.jconverter.testtools.matchers;

import java.util.Collection;
import java.util.Map;

import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;

/**
 * @author Lukasz Tutka
 */
public class IsEmpty<T> extends TypeSafeMatcher<T> {

	private static final String ILLEGAL_OBJ_PASSED = "Object passed for matching must be either a map or collection";

	@Override
	public void describeTo(Description description) {
		description.appendText("Should be empty");
	}

	@Override
	protected void describeMismatchSafely(T item, Description mismatchDescription) {
		int size = 0;
		if (item instanceof Map)
			size = ((Map) item).size();
		else if(item instanceof Collection)
			size = ((Collection) item).size();
		
		mismatchDescription.appendText("Size = " +size);
	}

	@Override
	protected boolean matchesSafely(T object) {
		if (object instanceof Map) {
			Map map = (Map) object;
			return map.isEmpty();
		} else if(object instanceof Collection) {
			Collection collection = (Collection) object;
			return collection.isEmpty();
		}
		
		throw new IllegalArgumentException(ILLEGAL_OBJ_PASSED);
	}
}
