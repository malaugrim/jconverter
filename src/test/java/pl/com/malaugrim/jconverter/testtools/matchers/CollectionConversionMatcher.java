package pl.com.malaugrim.jconverter.testtools.matchers;

import java.util.*;

import org.hamcrest.Description;
import org.hamcrest.StringDescription;
import org.hamcrest.TypeSafeMatcher;

/**
 * @author Lukasz Tutka
 */
public class CollectionConversionMatcher<S extends Collection, D extends Collection> extends TypeSafeMatcher<D> {

	private static final String COLLECTION_TYPE_DESCRIPTION = "Collection type: ";
	private static final String COLLECTION_SIZE_DESCRIPTION = "Collection size: ";

	private final S expected;
	private final List<String> expectedValuesList = new ArrayList<>();
	private final List<String> errorList = new ArrayList<>();

	public CollectionConversionMatcher(S expected) {
		this.expected = expected;
	}

	@Override
	public void describeTo(Description description) {
		expectedValuesList.forEach(s -> description.appendText("\n").appendText(s));
	}

	@Override
	protected boolean matchesSafely(D actual) {
		boolean typesCorrect = checkCollectionClasses(actual);
		boolean sizeEqual = checkCollectionSize(actual);
		boolean elementsEqual = checkCollectionElements(actual);
		return typesCorrect && sizeEqual && elementsEqual;
	}

	@Override
	protected void describeMismatchSafely(D actual, Description description) {
		errorList.forEach(s -> description.appendText("\n").appendText(s));
	}

	private boolean checkCollectionClasses(D actual) {
		Class<? extends Collection> expectedClass = expected.getClass();
		Class<? extends Collection> actualClass = actual.getClass();
		if(!Objects.equals(expectedClass, actualClass)) {
			expectedValuesList.add(COLLECTION_TYPE_DESCRIPTION + expectedClass);
			errorList.add(COLLECTION_TYPE_DESCRIPTION + actualClass);
			return false;
		}

		return true;
	}

	private boolean checkCollectionSize(D actual) {
		if(actual.size() != expected.size()) {
			expectedValuesList.add(COLLECTION_SIZE_DESCRIPTION + expected.size());
			errorList.add(COLLECTION_SIZE_DESCRIPTION + actual.size());
			return false;
		}

		return true;
	}

	private boolean checkCollectionElements(D actual) {
		Iterator expectedIterator = expected.iterator();
		Iterator actualIterator = actual.iterator();
		while(expectedIterator.hasNext()) {
			if (!checkElementsAtNextIteration(expectedIterator, actualIterator)) {
				return false;
			}
		}

		return true;
	}

	@SuppressWarnings("unchecked")
	private boolean checkElementsAtNextIteration(Iterator expectedIterator, Iterator actualIterator) {
		Object expectedElement = expectedIterator.next();
		Object actualElement = actualIterator.next();

		return checkElement(actualElement, new ConversionMatcher(expectedElement));

	}

	@SuppressWarnings("unchecked")
	private boolean checkElement(Object actualElement, ConversionMatcher conversionMatcher) {
		if(!conversionMatcher.matchesSafely(actualElement)) {
			storeExpectedValueDescription(conversionMatcher);
			storeActualValueDescription(actualElement, conversionMatcher);
			return false;
		}

		return true;
	}

	private void storeExpectedValueDescription(ConversionMatcher conversionMatcher) {
		Description expectedDesc = new StringDescription();
		conversionMatcher.describeTo(expectedDesc);
		expectedValuesList.add(expectedDesc.toString());
	}

	@SuppressWarnings("unchecked")
	private void storeActualValueDescription(Object actualElement, ConversionMatcher conversionMatcher) {
		Description actualValuesDesc = new StringDescription();
		conversionMatcher.describeMismatchSafely(actualElement, actualValuesDesc);
		errorList.add(actualValuesDesc.toString());
	}
}
