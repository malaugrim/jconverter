package pl.com.malaugrim.jconverter.testtools.matchers;

import org.hamcrest.Matcher;

import java.util.Collection;

/**
 * Created by Lukasz Tutka
 */
public class TestMatchers {

	public static <T> Matcher<T> isProperlyCopied(T expected) {
		return new ConversionMatcher<>(expected);
	}

	public static <T extends Collection> Matcher<T> isCollectionProperlyConverted(T expected) {
		return new CollectionConversionMatcher<>(expected);
	}

	public static <T> Matcher<T> isEmpty() {
		return new IsEmpty<>();
	}

	public static Matcher<String> matchesPattern(String regex) {
		return new RegexMatcher(regex);
	}
	
	public static Matcher<Collection> collectionsEqualIsOrder(Collection expected) {
		return new CollectionsEqualInOrder(expected);
	}

	public static Matcher<Collection> collectionsEqualNoOrder(Collection expected) {
		return new CollectionsEqualNoOrder(expected);
	}
}
