package pl.com.malaugrim.jconverter.testtools;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Optional;

/**
 * @author Lukasz Tutka
 */
public class ValueExtractor {

	@SuppressWarnings("unchecked")
	public static <T> T getValue(Object object, String fieldName) {
		try {
			Field field = getField(object.getClass(), fieldName);
			field.setAccessible(true);
			return (T) field.get(object);
		} catch (ReflectiveOperationException e) {
			throw new RuntimeException(e);
		}
	}

	private static Field getField(Class<?> cl, String fieldName) throws NoSuchFieldException {
		Optional<Field> optField = Arrays.stream(cl.getDeclaredFields()).filter(f -> f.getName().equals(fieldName)).findAny();
		if(!optField.isPresent()) {
			return searchInSuperClass(cl, fieldName);
		}

		return optField.get();
	}

	private static Field searchInSuperClass(Class<?> cl, String fieldName) throws NoSuchFieldException {
		Class<?> superclass = cl.getSuperclass();
		if (superclass == null) {
			throw new NoSuchFieldException(fieldName);
		}

		return getField(superclass, fieldName);
	}
}
