package pl.com.malaugrim.jconverter.testtools.matchers;

import java.lang.annotation.Annotation;
import java.util.List;

import com.google.common.base.MoreObjects;
import com.google.common.collect.Lists;

/**
 * @author Lukasz Tutka
 */
public class ExpectedAccessible {

	private final String name;
	private final Class<?> type;
	private final List<Class<? extends Annotation>> annotations;

	public ExpectedAccessible(String name, Class<?> type, List<Class<? extends Annotation>> annotations) {
		this.name = name;
		this.type = type;
		this.annotations = annotations;
	}

	public List<Class<? extends Annotation>> getAnnotations() {
		return annotations;
	}

	public String getName() {
		return name;
	}

	public Class<?> getType() {
		return type;
	}

	@Override
	public String toString() {
		return "{ name: " +name+ ", type:" +type.getCanonicalName()+ ", annotations: " +annotations+ " }";
	}
}
