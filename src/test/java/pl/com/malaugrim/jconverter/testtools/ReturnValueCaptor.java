package pl.com.malaugrim.jconverter.testtools;

import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

/**
 * @author Lukasz Tutka
 */
public class ReturnValueCaptor<T> implements Answer<T> {
	
	private T returnValue;

	@SuppressWarnings("unchecked")
	@Override
	public T answer(InvocationOnMock invocationOnMock) throws Throwable {
		returnValue = (T) invocationOnMock.callRealMethod();
		return returnValue;
	}

	public T getReturnValue() {
		return returnValue;
	}
}
