package pl.com.malaugrim.jconverter.core.copymode;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import pl.com.malaugrim.jconverter.TestConstants;
import pl.com.malaugrim.jconverter.testobjects.simple.*;

import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.spy;
import static pl.com.malaugrim.jconverter.testtools.matchers.TestMatchers.isProperlyCopied;
import static pl.com.malaugrim.jconverter.testtools.matchers.TestMatchers.matchesPattern;

/**
 * @author Lukasz Tutka
 */
public abstract class AbstractCopyModeTest implements TestConstants {

	@Rule
	public final ExpectedException thrown = ExpectedException.none();

	@Test
	public void shouldThrowNPEWhenNullIsPassedAsSourceObject() {
		thrown.expect(NullPointerException.class);
		thrown.expectMessage("Source object is null");
		getCopyMode().setup(null, new TestObject1());
	}

	@Test
	public void shouldThrowNPEWhenNullIsPassedAsDestinationObject() {
		thrown.expect(NullPointerException.class);
		thrown.expectMessage("Destination object is null");
		getCopyMode().setup(new TestObject1(), null);
	}

	@Test
	public void shouldThrowExceptionWhenSrcAndDstObjectsAreNotSetup() {
		thrown.expect(IllegalStateException.class);
		thrown.expectMessage("Copy mode needs to be setup with source and destination objects");
		getCopyMode().copy("whatever", "whatever");
	}

	@Test
	public void shouldRetainNotCopiedAccessibles() {
		TestObject1 testObject1 = new TestObject1(INT_VALUE, LONG_VALUE, DOUBLE_VALUE, BOOLEAN_VALUE, STRING_VALUE);
		TestObject2 testObject2 = new TestObject2(INT_VALUE, DOUBLE_VALUE, STRING_VALUE);
		TestObject3 dst = new TestObject3(null, testObject2);
		TestObject3 expected = new TestObject3(testObject1, testObject2);
		getCopyMode().setup(new TestObject3(testObject1, null), dst);
		getCopyMode().copy("testObject1", "testObject1");

		assertThat(dst, isProperlyCopied(expected));
	}

	@Test
	public void shouldCopyAccessibleFromSuperClass() {
		SubClass src = new SubClass(STRING_VALUE, LONG_VALUE, DOUBLE_VALUE);
		SubClass dst = new SubClass();
		getCopyMode().setup(src, dst);
		getCopyMode().copy("longValue", "longValue");
		
		assertThat(dst, isProperlyCopied(new SubClass(null, LONG_VALUE, null)));
	}

	@Test
	public void shouldCopyPrimitivesToTheirBoxedCorrespondingTypes() {
		TestObject1 src = new TestObject1(INT_VALUE, LONG_VALUE, DOUBLE_VALUE, BOOLEAN_VALUE, STRING_VALUE);
		ObjectWithLongField dst = new ObjectWithLongField();
		getCopyMode().setup(src, dst);
		getCopyMode().copy("longValue", "testField");
		assertThat(dst, isProperlyCopied(new ObjectWithLongField(LONG_VALUE)));
	}

	@Test
	public void shouldCopyBoxedTypesToTheirCorrespondingPrimitives() {
		ObjectWithLongField src = new ObjectWithLongField(LONG_VALUE);
		TestObject1 dst = new TestObject1(INT_VALUE, 0L, DOUBLE_VALUE, BOOLEAN_VALUE, STRING_VALUE);
		getCopyMode().setup(src, dst);
		getCopyMode().copy("testField", "longValue");
		assertThat(dst, isProperlyCopied(new TestObject1(INT_VALUE, LONG_VALUE, DOUBLE_VALUE, BOOLEAN_VALUE, STRING_VALUE)));
	}

	@Test
	public void shouldSetPrimitiveDefaultWhenSrcBoxedTypeIsNull() {
		TestObjectWithBoxedTypes src = spy(new TestObjectWithBoxedTypes());
		TestObject1 dst = new TestObject1(INT_VALUE, LONG_VALUE, DOUBLE_VALUE, BOOLEAN_VALUE, STRING_VALUE);
		getCopyMode().setup(src, dst);
		getCopyMode().copy("intValue", "intValue");
		getCopyMode().copy("doubleValue", "doubleValue");
		getCopyMode().copy("booleanValue", "booleanValue");

		assertThat(dst, isProperlyCopied(new TestObject1(0, 1L, 0.0, false, STRING_VALUE)));
	}

	@Test
	public void shouldThrowExceptionWhenSrcAndDstAccessibleTypesAreDifferent() {
		thrown.expect(IllegalStateException.class);
		thrown.expectMessage(matchesPattern("Cannot assign (\\w+\\.?)+ to (\\w+\\.?)+"));
		
		TestObject3 src = new TestObject3(new TestObject1(INT_VALUE, LONG_VALUE, DOUBLE_VALUE, BOOLEAN_VALUE, STRING_VALUE), null);
		getCopyMode().setup(src, new TestObject3());
		getCopyMode().copy("testObject1", "testObject2");
	}

	@Test
	public void shouldUseSrcAccessibleChainToGetSrcValue() {
		TestObject3 src = new TestObject3(new TestObject1(INT_VALUE, LONG_VALUE, DOUBLE_VALUE, BOOLEAN_VALUE, STRING_VALUE), null);
		TestObject1 dst = new TestObject1();
		TestObject1 expected = new TestObject1(LONG_VALUE);

		getCopyMode().setup(src, dst);
		getCopyMode().copy("testObject1.longValue", "longValue");
		assertThat(dst, isProperlyCopied(expected));
	}

	@Test
	public void shouldUseDstAccessibleChainToSetValueInDstObject() {
		TestObject1 src = new TestObject1(INT_VALUE, LONG_VALUE, DOUBLE_VALUE, BOOLEAN_VALUE, STRING_VALUE);
		TestObject3 dst = new TestObject3(new TestObject1(), null);
		TestObject3 expected = new TestObject3(new TestObject1(LONG_VALUE), null);

		getCopyMode().setup(src, dst);
		getCopyMode().copy("longValue", "testObject1.longValue");
		assertThat(dst, isProperlyCopied(expected));
	}

	@Test
	public void shouldCreateDstInstanceWhenDstAccessibleChainIsUsed() {
		TestObject1 src = new TestObject1(INT_VALUE, LONG_VALUE, DOUBLE_VALUE, BOOLEAN_VALUE, STRING_VALUE);
		TestObject3 dst = new TestObject3();
		TestObject3 expected = new TestObject3(null, new TestObject2(INT_VALUE, 0.0, null));

		getCopyMode().setup(src, dst);
		getCopyMode().copy("intValue", "testObject2.intValue");
		assertThat(dst, isProperlyCopied(expected));
	}

	@Test
	public void shouldRetainNotCopiedDstAccessiblesWhenDstAccessibleChainIsUsed() {
		TestObject1 src = new TestObject1(INT_VALUE, LONG_VALUE, DOUBLE_VALUE, BOOLEAN_VALUE, STRING_VALUE);
		TestObject3 dst = new TestObject3(null, new TestObject2(0, DOUBLE_VALUE, STRING_VALUE));
		getCopyMode().setup(src, dst);
		getCopyMode().copy("intValue", "testObject2.intValue");

		TestObject3 expected = new TestObject3(null, new TestObject2(INT_VALUE, DOUBLE_VALUE, STRING_VALUE));
		assertThat(dst, isProperlyCopied(expected));
	}

	@Test
	public void shouldOmitFromCopyingWhenOneAccessibleFromChainIsNull() {
		TestObject3 src = new TestObject3(null, new TestObject2(INT_VALUE, DOUBLE_VALUE, STRING_VALUE));
		TestObject1 dst = new TestObject1();
		getCopyMode().setup(src, dst);
		getCopyMode().copy("testObject1.stringValue", "stringValue");
		getCopyMode().copy("testObject2.intValue", "longValue");

		assertThat(dst, isProperlyCopied(new TestObject1(INT_VALUE)));
	}

	protected void shouldThrowExceptionWhenThereIsNoSourceAccessible(String accessibleName) {
		thrown.expect(IllegalStateException.class);
		thrown.expectMessage("Source object does not contain " + accessibleName + " 'nonExisting'");

		getCopyMode().setup(new TestObject1(), new TestObject1());
		getCopyMode().copy("nonExisting", "longValue");
	}

	protected void shouldThrowExceptionWhenThereIsNoDestinationAccessible(String accessibleName) {
		thrown.expect(IllegalStateException.class);
		thrown.expectMessage("Destination object does not contain " + accessibleName + " 'nonExisting'");

		getCopyMode().setup(new TestObject1(), new TestObject1());
		getCopyMode().copy("longValue", "nonExisting");
	}

	protected abstract CopyMode getCopyMode();
}
