package pl.com.malaugrim.jconverter.core.copymode;

import org.junit.Before;
import org.junit.Test;

import pl.com.malaugrim.jconverter.core.copymode.copymodebuilder.CopyModeBuilder;
import pl.com.malaugrim.jconverter.testobjects.simple.ObjectWithStringField;
import pl.com.malaugrim.jconverter.testobjects.simple.TestObject1;
import pl.com.malaugrim.jconverter.testobjects.simple.TestObject2;
import pl.com.malaugrim.jconverter.core.copymode.copymodebuilder.ConversionModeVariant;
import pl.com.malaugrim.jconverter.testtools.matchers.TestMatchers;

import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;

/**
 * @author Lukasz Tutka
 */
@SuppressWarnings("unchecked")
public class FieldToFieldConversionModeTest extends AbstractConversionModeTest {
	
	private ConversionMode copyMode;


	@Override
	@Before
	public void setUp() {
		super.setUp();
		CopyModeBuilder builder = new CopyModeBuilder();
		copyMode = builder.convert(ConversionModeVariant.TAKE_DST_VALUE_OR_TYPE, typeConverter).fromField().toField().build();
	}

	@Test
	public void shouldSetFieldFromConvertedFieldOfTheSameNameInSameClassObject() {
		when(typeConverter.convert(any(), any())).thenReturn(STRING_VALUE);
		TestObject2 src = new TestObject2(INT_VALUE, DOUBLE_VALUE, STRING_VALUE);
		TestObject2 dst = spy(new TestObject2());
		TestObject2 expected = new TestObject2(0, 0.0, STRING_VALUE);
		copyMode.setup(src, dst);
		copyMode.copy("stringValue", "stringValue");

		assertThat(dst, TestMatchers.isProperlyCopied(expected));
		verify(dst, never()).setStringValue(any(String.class));
		verify(typeConverter).canConvert(String.class, String.class);
		verify(typeConverter).convert(STRING_VALUE, String.class);
	}

	@Test
	public void shouldSetFieldFromConvertedFieldOfTheSameNameInClassOfDifferentType() {
		when(typeConverter.convert(DOUBLE_VALUE, 0.0)).thenReturn(DOUBLE_VALUE);
		TestObject1 src = new TestObject1(INT_VALUE, LONG_VALUE, DOUBLE_VALUE, BOOLEAN_VALUE, STRING_VALUE);
		TestObject2 dst = spy(new TestObject2());
		TestObject2 expected = new TestObject2(0, DOUBLE_VALUE, null);
		copyMode.setup(src, dst);
		copyMode.copy("doubleValue", "doubleValue");

		assertThat(dst, TestMatchers.isProperlyCopied(expected));
		verify(dst, never()).setDoubleValue(any(Double.class));
		verify(typeConverter).canConvert(Double.class, double.class);
		verify(typeConverter).convert(DOUBLE_VALUE, 0.0);
	}

	@Test
	public void shouldSetFieldFromConvertedFieldOfDifferentNameInClassOfDifferentType() {
		when(typeConverter.convert(any(), any())).thenReturn(STRING_VALUE);
		TestObject2 src = new TestObject2(INT_VALUE, DOUBLE_VALUE, STRING_VALUE);
		ObjectWithStringField dst = spy(new ObjectWithStringField());
		ObjectWithStringField expected = new ObjectWithStringField(STRING_VALUE);
		copyMode.setup(src, dst);
		copyMode.copy("stringValue", "testField");

		assertThat(dst, TestMatchers.isProperlyCopied(expected));
		verify(typeConverter).canConvert(String.class, String.class);
		verify(typeConverter).convert(STRING_VALUE, String.class);
	}

	@Test
	public void shouldThrowExceptionWhenThereIsNoSourceField() {
		shouldThrowExceptionWhenThereIsNoSourceAccessible("field");
	}

	@Test
	public void shouldThrowExceptionWhenThereIsNoDestinationField() {
		shouldThrowExceptionWhenThereIsNoDestinationAccessible("field");
	}

	@Override
	protected ConversionMode getCopyMode() {
		return copyMode;
	}
}
