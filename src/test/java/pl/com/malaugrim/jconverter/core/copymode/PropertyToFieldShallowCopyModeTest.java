package pl.com.malaugrim.jconverter.core.copymode;

import pl.com.malaugrim.jconverter.core.copymode.copymodebuilder.CopyModeBuilder;
import pl.com.malaugrim.jconverter.testobjects.simple.TestObject1;
import pl.com.malaugrim.jconverter.testobjects.simple.TestObject3;
import pl.com.malaugrim.jconverter.testobjects.simple.WithTestObject1Getter;
import pl.com.malaugrim.jconverter.testobjects.simple.WithTestObject1Setter2;
import org.junit.Before;
import org.junit.Test;

import static pl.com.malaugrim.jconverter.testtools.matchers.TestMatchers.isProperlyCopied;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

/**
 * @author Lukasz Tutka
 */
public class PropertyToFieldShallowCopyModeTest extends AbstractCopyModeTest {

	private CopyMode copyMode;


	@Before
	public void setUp() {
		CopyModeBuilder builder = new CopyModeBuilder();
		copyMode = builder.shallowCopy().fromProperty().toField().build();
	}

	@Test
	public void shouldSetFieldFromPropertyOfTheSameNameInSameClassObject() {
		TestObject1 testObject1 = new TestObject1(INT_VALUE, LONG_VALUE, DOUBLE_VALUE, BOOLEAN_VALUE, STRING_VALUE);
		TestObject3 src = spy(new TestObject3(testObject1, null));
		TestObject3 dst = spy(new TestObject3());
		copyMode.setup(src, dst);
		copyMode.copy("testObject1", "testObject1");

		assertThat(dst, isProperlyCopied(new TestObject3(testObject1, null)));
		verify(src).getTestObject1();
		verify(dst, never()).setTestObject1(any());
	}

	@Test
	public void shouldSetFieldFromPropertyOfTheSameNameInClassOfDifferentType() {
		TestObject1 testObject1 = new TestObject1(INT_VALUE, LONG_VALUE, DOUBLE_VALUE, BOOLEAN_VALUE, STRING_VALUE);
		WithTestObject1Getter src = spy(new WithTestObject1Getter(testObject1));
		TestObject3 dst = spy(new TestObject3());
		copyMode.setup(src, dst);
		copyMode.copy("object", "testObject1");

		assertThat(dst, isProperlyCopied(new TestObject3(testObject1, null)));
		verify(src).getObject();
		verify(dst, never()).setTestObject1(any());
	}

	@SuppressWarnings("Duplicates")
	@Test
	public void shouldSetFieldFromPropertyOfDifferentNameInClassOfDifferentType() {
		TestObject1 testObject1 = new TestObject1(INT_VALUE, LONG_VALUE, DOUBLE_VALUE, BOOLEAN_VALUE, STRING_VALUE);
		TestObject3 src = spy(new TestObject3(testObject1, null));
		WithTestObject1Setter2 dst = spy(new WithTestObject1Setter2());
		copyMode.setup(src, dst);
		copyMode.copy("testObject1", "object");

		assertThat(dst, isProperlyCopied(new WithTestObject1Setter2(testObject1)));
		verify(src).getTestObject1();
		verify(dst, never()).setObject(any());
	}

	@Test
	public void shouldThrowExceptionWhenThereIsNoSourceProperty() {
		shouldThrowExceptionWhenThereIsNoSourceAccessible("property");
	}

	@Test
	public void shouldThrowExceptionWhenThereIsNoDestinationField() {
		shouldThrowExceptionWhenThereIsNoDestinationAccessible("field");
	}

	@Override
	protected CopyMode getCopyMode() {
		return copyMode;
	}

}
