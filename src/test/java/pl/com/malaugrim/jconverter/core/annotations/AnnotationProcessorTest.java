package pl.com.malaugrim.jconverter.core.annotations;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.google.common.collect.Lists;

import pl.com.malaugrim.jconverter.core.TypeConverter;
import pl.com.malaugrim.jconverter.core.accessor.AccessibleObjectWrapper;
import pl.com.malaugrim.jconverter.testconverters.LongToStringConverter;
import pl.com.malaugrim.jconverter.testobjects.annotated.ObjectWithAnnotations;
import pl.com.malaugrim.jconverter.testobjects.annotated.ObjectWithFieldNameDifferentFromMethodName;
import pl.com.malaugrim.jconverter.testobjects.annotated.ObjectWithIllegalIgnoreAnnotation;
import pl.com.malaugrim.jconverter.testobjects.annotated.WithIllegalShallowCopyMode;
import pl.com.malaugrim.jconverter.testobjects.simple.TestObject1;
import pl.com.malaugrim.jconverter.testtools.matchers.AccessibleObjectCollectionMatcher;
import pl.com.malaugrim.jconverter.testtools.matchers.ExpectedAccessible;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

/**
 * @author Lukasz Tutka
 */
@SuppressWarnings("unchecked")
public class AnnotationProcessorTest {

	private static final String IS_EMPTY_METHOD_MISSING = "ProcessingResult contains field of type, which does not have isEmpty() method. " +
			"Please update test.";


	@Rule
	public final ExpectedException thrown = ExpectedException.none();

	private final AnnotationProcessor processor = new AnnotationProcessor();

	@Test
	public void shouldGetAllAnnotatedAccessibles() {
		ProcessingResult result = processor.process(new ObjectWithAnnotations());
		List<AccessibleObjectWrapper> accessibles = result.getAnnotatedAccessibles();
		
		List<ExpectedAccessible> expectedAccessibles = Lists.newArrayList(
				new ExpectedAccessible("intValue", Integer.class, Lists.newArrayList(Ignore.class)),
				new ExpectedAccessible("testObject1", TestObject1.class, Lists.newArrayList(DeepCopy.class)),
				new ExpectedAccessible("longValue", Long.class, Lists.newArrayList(DeepCopy.class)),
				new ExpectedAccessible("stringValue", String.class, Lists.newArrayList(Convert.class)),
				new ExpectedAccessible("doubleValue", Double.class, Lists.newArrayList(Convert.class)),
				new ExpectedAccessible("shortValue", Short.class, Lists.newArrayList(ShallowCopy.class)),
				new ExpectedAccessible("someObject", Object.class, Lists.newArrayList(ShallowCopy.class)),
				new ExpectedAccessible("booleanValue", Boolean.class, Lists.newArrayList(Ignore.class))
		);
	
		assertThat(accessibles, new AccessibleObjectCollectionMatcher(expectedAccessibles));
	}

	@Test
	public void shouldGetAccessiblesToShallowCopy() {
		ProcessingResult result = processor.process(new ObjectWithAnnotations());
		List<AccessibleObjectWrapper> toShallowCopy = result.getToShallowCopy();
		List<ExpectedAccessible> expectedAccessibles = Lists.newArrayList(
				new ExpectedAccessible("shortValue", Short.class, Lists.newArrayList(ShallowCopy.class)),
				new ExpectedAccessible("someObject", Object.class, Lists.newArrayList(ShallowCopy.class))
		);

		assertThat(toShallowCopy, new AccessibleObjectCollectionMatcher(expectedAccessibles));
	}

	@Test
	public void shouldGetIgnoredAccessibles() {
		ProcessingResult result = processor.process(new ObjectWithAnnotations());
		List<AccessibleObjectWrapper> ignored = result.getIgnored();
		
		List<ExpectedAccessible> expected = Lists.newArrayList(
				new ExpectedAccessible("intValue", Integer.class, Lists.newArrayList(Ignore.class)),
				new ExpectedAccessible("booleanValue", Boolean.class, Lists.newArrayList(Ignore.class))
		);
		
		assertThat(ignored, new AccessibleObjectCollectionMatcher(expected));
	}

	@Test
	public void shouldGetAccessiblesToDeepCopy() {
		ProcessingResult result = processor.process(new ObjectWithAnnotations());
		List<AccessibleObjectWrapper> toDeepCopy = result.getToDeepCopy();

		List<ExpectedAccessible> expected = Lists.newArrayList(
				new ExpectedAccessible("testObject1", TestObject1.class, Lists.newArrayList(DeepCopy.class)),
				new ExpectedAccessible("longValue", Long.class, Lists.newArrayList(DeepCopy.class))
		);

		assertThat(toDeepCopy, new AccessibleObjectCollectionMatcher(expected));
	}

	@Test
	public void shouldGetAccessiblesToConvert() {
		ProcessingResult result = processor.process(new ObjectWithAnnotations());
		Map<AccessibleObjectWrapper, Class<? extends TypeConverter>> toConvert = result.getToConvert();

		List<ExpectedAccessible> expected = Lists.newArrayList(
				new ExpectedAccessible("stringValue", String.class, Lists.newArrayList(Convert.class)),
				new ExpectedAccessible("doubleValue", Double.class, Lists.newArrayList(Convert.class))
		);

		assertThat(toConvert.keySet(), new AccessibleObjectCollectionMatcher(expected));
		assertConverters(toConvert);
	}

	@Test
	public void shouldReturnEmptyProcessingResultWhenNullObjectIsProcessed() throws Exception {
		ProcessingResult result = processor.process(null);
		assertEmptyProcessingResult(result);
	}

	@Test
	public void shouldGetIgnoredAccessiblesBothPropertyAndCorrespondingFieldWItchDifferentName() {
		ProcessingResult result = processor.process(new ObjectWithFieldNameDifferentFromMethodName());
		List<AccessibleObjectWrapper> ignored = result.getIgnored();
		List<ExpectedAccessible> expected = Lists.newArrayList(
				new ExpectedAccessible("field", String.class, Lists.newArrayList()),
				new ExpectedAccessible("testField", String.class, Lists.newArrayList(Ignore.class))
		);

		assertThat(ignored, new AccessibleObjectCollectionMatcher(expected));
	}

	@Test
	public void shouldThrowExceptionWhenIgnorePropertyPointsToNonExistingField() {
		thrown.expect(IllegalStateException.class);
		thrown.expectMessage("Ignore annotation points to non existing field");
		processor.process(new ObjectWithIllegalIgnoreAnnotation());
	}

	@Test
	public void shouldThrowExceptionWhenShallowCopyHasPointedBothFieldAndProperty() {
		thrown.expect(IllegalStateException.class);
		thrown.expectMessage("ShallowCopy annotation cannot point source field and property and the same time");
		processor.process(new WithIllegalShallowCopyMode());
	}

	private void assertConverters(Map<AccessibleObjectWrapper, Class<? extends TypeConverter>> toConvert) {
		toConvert.forEach( (accessible, converter) -> {
			if(accessible.getAccessibleObjectName().equals("stringValue")) {
				assertThat(converter, equalTo(LongToStringConverter.class));
			} else if(accessible.getAccessibleObjectName().equals("doubleValue")) {
				assertThat(converter, equalTo(TypeConverter.class));
			}
		} );
	}

	private void assertEmptyProcessingResult(ProcessingResult result) throws InvocationTargetException, IllegalAccessException {
		Field[] fields = result.getClass().getDeclaredFields();
		for (Field field : fields) {
			String msg = field.getName() + ", is not empty";
			assertThat(msg, invokeIsEmptyMethod(field, result), equalTo(true));
		}
	}

	private boolean invokeIsEmptyMethod(Field field, ProcessingResult result) throws IllegalAccessException, InvocationTargetException {
		Class<?> fieldType = field.getType();
		Method isEmptyMethod = getIsEmptyMethod(fieldType);
		field.setAccessible(true);
		return (boolean) isEmptyMethod.invoke(field.get(result));
	}

	private Method getIsEmptyMethod(Class<?> fieldType) {
		return Arrays.stream(fieldType.getMethods()).filter(m -> m.getName().equals("isEmpty")).findAny()
				.orElseThrow(() -> new IllegalStateException(IS_EMPTY_METHOD_MISSING));
	}
}
