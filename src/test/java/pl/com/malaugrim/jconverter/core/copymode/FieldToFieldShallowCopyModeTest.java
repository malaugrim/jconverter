package pl.com.malaugrim.jconverter.core.copymode;

import pl.com.malaugrim.jconverter.core.copymode.copymodebuilder.CopyModeBuilder;
import pl.com.malaugrim.jconverter.testobjects.simple.ObjectWithStringField;
import pl.com.malaugrim.jconverter.testobjects.simple.TestObject1;
import pl.com.malaugrim.jconverter.testobjects.simple.TestObject2;
import org.junit.Before;
import org.junit.Test;

import static pl.com.malaugrim.jconverter.testtools.matchers.TestMatchers.isProperlyCopied;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;

/**
 * @author Lukasz Tutka
 */
public class FieldToFieldShallowCopyModeTest extends AbstractCopyModeTest {

	private CopyMode copyMode;

	@Before
	public void setUp() {
		CopyModeBuilder builder = new CopyModeBuilder();
		copyMode = builder.shallowCopy().fromField().toField().build();
	}

	@Test
	public void shouldSetFieldFromFieldOfTheSameNameInSameClassObject() {
		TestObject2 src = new TestObject2(INT_VALUE, DOUBLE_VALUE, STRING_VALUE);
		TestObject2 dst = spy(new TestObject2());
		TestObject2 expected = new TestObject2(0, 0.0, STRING_VALUE);
		copyMode.setup(src, dst);
		copyMode.copy("stringValue", "stringValue");

		assertThat(dst, isProperlyCopied(expected));
		verify(dst, never()).setStringValue(any());
	}

	@Test
	public void shouldSetFieldFromFieldOfTheSameNameInClassOfDifferentType() {
		TestObject1 src = new TestObject1(INT_VALUE, LONG_VALUE, DOUBLE_VALUE, BOOLEAN_VALUE, STRING_VALUE);
		TestObject2 dst = spy(new TestObject2());
		TestObject2 expected = new TestObject2(0, DOUBLE_VALUE, null);
		copyMode.setup(src, dst);
		copyMode.copy("doubleValue", "doubleValue");

		assertThat(dst, isProperlyCopied(expected));
		verify(dst, never()).setStringValue(any());
	}

	@Test
	public void shouldSetFieldFromFieldOfDifferentNameInClassOfDifferentType() {
		TestObject2 src = new TestObject2(INT_VALUE, DOUBLE_VALUE, STRING_VALUE);
		ObjectWithStringField dst = spy(new ObjectWithStringField());
		ObjectWithStringField expected = new ObjectWithStringField(STRING_VALUE);
		copyMode.setup(src, dst);
		copyMode.copy("stringValue", "testField");

		assertThat(dst, isProperlyCopied(expected));
		verify(dst, never()).setTestField(any());
	}

	@Test
	public void shouldThrowExceptionWhenThereIsNoSourceField() {
		shouldThrowExceptionWhenThereIsNoSourceAccessible("field");
	}

	@Test
	public void shouldThrowExceptionWhenThereIsNoDestinationField() {
		shouldThrowExceptionWhenThereIsNoDestinationAccessible("field");
	}

	@Override
	protected CopyMode getCopyMode() {
		return copyMode;
	}
}
