package pl.com.malaugrim.jconverter.core.copymode;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;
import static pl.com.malaugrim.jconverter.testtools.matchers.TestMatchers.isProperlyCopied;

import pl.com.malaugrim.jconverter.core.copymode.copymodebuilder.CopyModeBuilder;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import pl.com.malaugrim.jconverter.testobjects.simple.TestObject1;
import pl.com.malaugrim.jconverter.testobjects.simple.TestObject3;
import pl.com.malaugrim.jconverter.testobjects.simple.TestObject4;
import pl.com.malaugrim.jconverter.testobjects.simple.WithTestObject1Setter2;
import pl.com.malaugrim.jconverter.testobjects.simple.WithTestObject1Setter3;

/**
 * @author Lukasz Tutka
 */
@SuppressWarnings("Duplicates")
public class PropertyToFieldDeepCopyModeTest extends AbstractDeepCopyModeTest {

	private DeepCopyMode copyMode;

	@Before
	public void setUp() {
		CopyModeBuilder builder = new CopyModeBuilder();
		copyMode = builder.deepCopy().fromProperty().toField().build();
	}

	@Test
	public void shouldSetFieldFromPropertyOfTheSameNameInSameClassObject() {
		TestObject1 testObject1 = new TestObject1(INT_VALUE, LONG_VALUE, DOUBLE_VALUE, BOOLEAN_VALUE, STRING_VALUE);
		TestObject3 testObject3 = new TestObject3(testObject1, null);
		TestObject4 src = spy(new TestObject4(testObject3));
		TestObject4 dst = spy(new TestObject4());
		copyMode.setup(src, dst);
		copyMode.copy("testObject3", "testObject3");

		assertThat(dst, isProperlyCopied(new TestObject4(testObject3)));
		verify(src).getTestObject3();
		verify(dst, never()).setTestObject3(Mockito.any());
		assertThat(dst.getTestObject3(), not(sameInstance(testObject3)));
		assertThat(dst.getTestObject3().getTestObject1(), not(sameInstance(testObject1)));
	}

	@Test
	public void shouldSetFieldFromPropertyOfTheSameNameInClassOfDifferentType() {
		TestObject1 testObject1 = new TestObject1(INT_VALUE, LONG_VALUE, DOUBLE_VALUE, BOOLEAN_VALUE, STRING_VALUE);
		TestObject3 src = spy(new TestObject3(testObject1, null));
		WithTestObject1Setter3 dst = spy(new WithTestObject1Setter3());
		copyMode.setup(src, dst);
		copyMode.copy("testObject1", "testObject1");

		assertThat(dst, isProperlyCopied(new WithTestObject1Setter3(testObject1)));
		assertThat(dst.getTestObject1(), not(sameInstance(testObject1)));
		verify(src).getTestObject1();
		verify(dst, never()).setTestObject1(any(TestObject1.class));
	}

	@Test
	public void shouldSetFieldFromPropertyOfDifferentNameInClassOfDifferentType() {
		TestObject1 testObject1 = new TestObject1(INT_VALUE, LONG_VALUE, DOUBLE_VALUE, BOOLEAN_VALUE, STRING_VALUE);
		TestObject3 src = spy(new TestObject3(testObject1, null));
		WithTestObject1Setter2 dst = spy(new WithTestObject1Setter2());
		copyMode.setup(src, dst);
		copyMode.copy("testObject1", "object");

		assertThat(dst, isProperlyCopied(new WithTestObject1Setter2(testObject1)));
		verify(src).getTestObject1();
		verify(dst, never()).setObject(any());
	}

	@Test
	public void shouldThrowExceptionWhenThereIsNoSourceProperty() {
		shouldThrowExceptionWhenThereIsNoSourceAccessible("property");
	}

	@Test
	public void shouldThrowExceptionWhenThereIsNoDestinationField() {
		shouldThrowExceptionWhenThereIsNoDestinationAccessible("field");
	}

	@Override
	protected DeepCopyMode getCopyMode() {
		return copyMode;
	}
}
