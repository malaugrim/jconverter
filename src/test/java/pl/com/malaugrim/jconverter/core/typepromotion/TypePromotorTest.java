package pl.com.malaugrim.jconverter.core.typepromotion;

import com.google.common.collect.Lists;
import pl.com.malaugrim.jconverter.TestConstants;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import pl.com.malaugrim.jconverter.core.typepromotion.handlers.*;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

/**
 * @author Lukasz Tutka
 */
@RunWith(Parameterized.class)
public class TypePromotorTest implements TestConstants {

	private final Object src;
	private final Object expected;
	private TypePromotor typePromotor;


	@Parameterized.Parameters
	public static Object[] getParams() {
		return new Object[][] {
				{ CHAR_VALUE, (int) CHAR_VALUE },		//0
				{ CHAR_VALUE, (long) CHAR_VALUE },		//1
				{ CHAR_VALUE, (float) CHAR_VALUE },		//2
				{ CHAR_VALUE, (double) CHAR_VALUE },	//3
				{ CHAR_VALUE, String.valueOf(CHAR_VALUE) },	//4

				{ BYTE_VALUE, (short) BYTE_VALUE },		//5
				{ BYTE_VALUE, (int) BYTE_VALUE },		//6
				{ BYTE_VALUE, (long) BYTE_VALUE },		//7
				{ BYTE_VALUE, (float) BYTE_VALUE },		//8
				{ BYTE_VALUE, (double) BYTE_VALUE },	//9

				{ SHORT_VALUE, (int) SHORT_VALUE },		//10
				{ SHORT_VALUE, (long) SHORT_VALUE },	//11
				{ SHORT_VALUE, (float) SHORT_VALUE },	//12
				{ SHORT_VALUE, (double) SHORT_VALUE },	//13

				{ INT_VALUE, (long) INT_VALUE},			//14
				{ INT_VALUE, (float) INT_VALUE},		//15
				{ INT_VALUE, (double) INT_VALUE},		//16

				{ LONG_VALUE, (float) LONG_VALUE },		//17
				{ LONG_VALUE, (double) LONG_VALUE },	//18

				{ FLOAT_VALUE, (double) FLOAT_VALUE }	//19
		};
	}

	public TypePromotorTest(Object src, Object expected) {
		this.src = src;
		this.expected = expected;
	}

	@Before
	public void setUp() {
		typePromotor = new TypePromotor(Lists.newArrayList(
				new CharToIntPromotionHandler(),
				new CharToLongPromotionHandler(),
				new CharToFloatPromotionHandler(),
				new CharToDoublePromotionHandler(),
				new CharToStringPromotionHandler(),
				new ByteToShortPromotionHandler(),
				new ByteToIntPromotionHandler(),
				new ByteToLongPromotionHandler(),
				new ByteToFloatPromotionHandler(),
				new ByteToDoublePromotionHandler(),
				new ShortToIntPromotionHandler(),
				new ShortToLongPromotionHandler(),
				new ShortToFloatPromotionHandler(),
				new ShortToDoublePromotionHandler(),
				new IntToLongPromotionHandler(),
				new IntToFloatPromotionHandler(),
				new IntToDoublePromotionHandler(),
				new LongToFloatPromotionHandler(),
				new LongToDoublePromotionHandler(),
				new FloatToDoublePromotionHandler()
		));
	}

	@Test
	public void testPromotion() {
		Class<?> dstType = expected.getClass();
		assertThat(typePromotor.canPromote(src.getClass(), dstType), equalTo(true));
		assertThat(typePromotor.promote(src, dstType), equalTo(expected));
	}

}
