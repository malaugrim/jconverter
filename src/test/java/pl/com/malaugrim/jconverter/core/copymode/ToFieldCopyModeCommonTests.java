package pl.com.malaugrim.jconverter.core.copymode;

import pl.com.malaugrim.jconverter.core.TypeConverter;

import static org.mockito.Mockito.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.List;

import com.google.common.collect.Lists;
import pl.com.malaugrim.jconverter.core.copymode.copymodebuilder.CopyModeBuilder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.mockito.invocation.InvocationOnMock;

import pl.com.malaugrim.jconverter.testobjects.simple.WithFinalField;
import pl.com.malaugrim.jconverter.testtools.matchers.TestMatchers;
import pl.com.malaugrim.jconverter.core.copymode.copymodebuilder.ConversionModeVariant;

@RunWith(Parameterized.class)
public class ToFieldCopyModeCommonTests {
	
	@Rule
	public final ExpectedException thrown = ExpectedException.none();
	private final CopyMode copyMode;


	@Parameterized.Parameters
	public static List<CopyMode> getToFieldCopyModes() {
		CopyModeBuilder builder = new CopyModeBuilder();
		return Lists.newArrayList(
				builder.shallowCopy().fromField().toField().build(),
				builder.deepCopy().fromField().toField().build(),
				builder.convert(ConversionModeVariant.TAKE_DST_VALUE_ONLY, createConverter()).fromField().toField().build(),
				builder.shallowCopy().fromProperty().toField().build(),
				builder.deepCopy().fromProperty().toField().build(),
				builder.convert(ConversionModeVariant.TAKE_DST_VALUE_ONLY, createConverter()).fromProperty().toField().build()
		);
	}

	@SuppressWarnings("unchecked")
	private static TypeConverter createConverter() {
		TypeConverter typeConverter = mock(TypeConverter.class);
		when(typeConverter.canConvert(any(), any())).thenReturn(true);
		when(typeConverter.convert(any(), any())).thenAnswer(ToFieldCopyModeCommonTests::returnSrcObj);
		return typeConverter;
	}

	private static Object returnSrcObj(InvocationOnMock invocationOnMock) {
		return invocationOnMock.getArguments()[0];
	}

	public ToFieldCopyModeCommonTests(CopyMode copyMode) {
		this.copyMode = copyMode;
	}
	
	@Test
	public void shouldThrowExceptionWhenTryToSetFinalField() {
		thrown.expect(IllegalStateException.class);
		thrown.expectMessage(TestMatchers.matchesPattern("Cannot set final field \\w+ in object \\[.+\\]"));

		WithFinalField src = new WithFinalField(FieldToFieldShallowCopyModeTest.DOUBLE_VALUE, FieldToFieldShallowCopyModeTest.INT_VALUE);
		WithFinalField dst = new WithFinalField(null);
		copyMode.setup(src, dst);
		copyMode.copy("intValue", "intValue");
	}
}