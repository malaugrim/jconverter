package pl.com.malaugrim.jconverter.core.copymode;

import pl.com.malaugrim.jconverter.core.copymode.copymodebuilder.CopyModeBuilder;
import pl.com.malaugrim.jconverter.testobjects.simple.ObjectWithStringField;
import pl.com.malaugrim.jconverter.testobjects.simple.TestObject1;
import pl.com.malaugrim.jconverter.testobjects.simple.TestObject2;
import org.junit.Before;
import org.junit.Test;
import pl.com.malaugrim.jconverter.core.copymode.copymodebuilder.ConversionModeVariant;

import static pl.com.malaugrim.jconverter.testtools.matchers.TestMatchers.isProperlyCopied;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

/**
 * @author Lukasz Tutka
 */
@SuppressWarnings("unchecked")
public class PropertyToPropertyConversionModeTest extends AbstractConversionModeTest {

	private ConversionMode copyMode;

	@Override
	@Before
	public void setUp() {
		super.setUp();
		CopyModeBuilder builder = new CopyModeBuilder();
		copyMode = builder.convert(ConversionModeVariant.TAKE_DST_VALUE_OR_TYPE, typeConverter).fromProperty().toProperty().build();
	}

	@Test
	public void shouldSetPropertyFromConvertedPropertyOfTheSameNameInSameClassObject() {
		when(typeConverter.convert(any(), any())).thenReturn(STRING_VALUE);
		TestObject2 src = spy(new TestObject2(INT_VALUE, DOUBLE_VALUE, STRING_VALUE));
		TestObject2 dst = spy(new TestObject2());
		TestObject2 expected = new TestObject2(0, 0.0, STRING_VALUE);
		copyMode.setup(src, dst);
		copyMode.copy("stringValue", "stringValue");

		assertThat(dst, isProperlyCopied(expected));
		verify(src).getStringValue();
		verify(dst).setStringValue(STRING_VALUE);
		verify(typeConverter).canConvert(String.class, String.class);
		verify(typeConverter).convert(STRING_VALUE, String.class);
	}

	@Test
	public void shouldSetPropertyFromConvertedPropertyOfTheSameNameInClassOfDifferentType() {
		when(typeConverter.convert(DOUBLE_VALUE, 0.0)).thenReturn(DOUBLE_VALUE);
		TestObject1 src = spy(new TestObject1(INT_VALUE, LONG_VALUE, DOUBLE_VALUE, BOOLEAN_VALUE, STRING_VALUE));
		TestObject2 dst = spy(new TestObject2());
		TestObject2 expected = new TestObject2(0, DOUBLE_VALUE, null);
		copyMode.setup(src, dst);
		copyMode.copy("doubleValue", "doubleValue");

		assertThat(dst, isProperlyCopied(expected));
		verify(src).getDoubleValue();
		verify(dst).setDoubleValue(DOUBLE_VALUE);
		verify(typeConverter).canConvert(Double.class, double.class);
		verify(typeConverter).convert(DOUBLE_VALUE, 0.0);
	}

	@Test
	public void shouldSetPropertyFromConvertedPropertyOfDifferentNameInClassOfDifferentType() {
		when(typeConverter.convert(any(), any())).thenReturn(STRING_VALUE);
		TestObject2 src = spy(new TestObject2(INT_VALUE, DOUBLE_VALUE, STRING_VALUE));
		ObjectWithStringField dst = spy(new ObjectWithStringField());
		ObjectWithStringField expected = new ObjectWithStringField(STRING_VALUE);
		copyMode.setup(src, dst);
		copyMode.copy("stringValue", "testField");

		assertThat(dst, isProperlyCopied(expected));
		verify(src).getStringValue();
		verify(dst).setTestField(STRING_VALUE);
		verify(typeConverter).canConvert(String.class, String.class);
		verify(typeConverter).convert(STRING_VALUE, String.class);
	}

	@Test
	public void shouldThrowExceptionWhenThereIsNoSourceProperty() {
		shouldThrowExceptionWhenThereIsNoSourceAccessible("property");
	}

	@Test
	public void shouldThrowExceptionWhenThereIsNoDestinationProperty() {
		shouldThrowExceptionWhenThereIsNoDestinationAccessible("property");
	}


	@Override
	protected ConversionMode getCopyMode() {
		return copyMode;
	}
}
