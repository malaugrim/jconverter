package pl.com.malaugrim.jconverter.core.copymode;

import pl.com.malaugrim.jconverter.TestConstants;
import pl.com.malaugrim.jconverter.core.copymode.copymodebuilder.CopyModeBuilder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import pl.com.malaugrim.jconverter.testobjects.simple.for_type_promotion_test.*;

import java.io.PrintWriter;
import java.io.StringWriter;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

/**
 * @author Lukasz Tutka
 */
@RunWith(Parameterized.class)
public class CopyModeCompatibleTypesAssignmentTest implements TestConstants {

	private final CopyMode copyMode;

	@Parameterized.Parameters
	public static Object[] getCopyModesForTesting() {
		CopyModeBuilder builder = new CopyModeBuilder();
		return new Object[] {
				builder.shallowCopy().fromField().toField().build(),
				builder.shallowCopy().fromField().toProperty().build(),
				builder.shallowCopy().fromProperty().toField().build(),
				builder.shallowCopy().fromProperty().toProperty().build(),
				builder.deepCopy().fromField().toField().build(),
				builder.deepCopy().fromField().toProperty().build(),
				builder.deepCopy().fromProperty().toField().build(),
				builder.deepCopy().fromProperty().toProperty().build()
		};
	}

	public CopyModeCompatibleTypesAssignmentTest(CopyMode copyMode) {
		this.copyMode = copyMode;
	}

	public Object[][] getObjectsToCopy() {
		return new Object[][] {

				// char -> int
				{ new PrimitiveChar(CHAR_VALUE), new PrimitiveInt(), new PrimitiveInt((int) CHAR_VALUE) },
				{ new PrimitiveChar(CHAR_VALUE), new BoxedInt(), new BoxedInt((int) CHAR_VALUE) },
				{ new BoxedChar(CHAR_VALUE), new PrimitiveInt(), new PrimitiveInt((int) CHAR_VALUE) },
				{ new BoxedChar(CHAR_VALUE), new BoxedInt(), new BoxedInt((int) CHAR_VALUE) },

				// char -> long
				{ new PrimitiveChar(CHAR_VALUE), new PrimitiveLong(), new PrimitiveLong((long) CHAR_VALUE) },
				{ new PrimitiveChar(CHAR_VALUE), new BoxedLong(), new BoxedLong((long) CHAR_VALUE) },
				{ new BoxedChar(CHAR_VALUE), new PrimitiveLong(), new PrimitiveLong((long) CHAR_VALUE) },
				{ new BoxedChar(CHAR_VALUE), new BoxedLong(), new BoxedLong((long) CHAR_VALUE) },

				// char -> float
				{ new PrimitiveChar(CHAR_VALUE), new PrimitiveFloat(), new PrimitiveFloat((float) CHAR_VALUE) },
				{ new PrimitiveChar(CHAR_VALUE), new BoxedFloat(), new BoxedFloat((float) CHAR_VALUE) },
				{ new BoxedChar(CHAR_VALUE), new PrimitiveFloat(), new PrimitiveFloat((float) CHAR_VALUE) },
				{ new BoxedChar(CHAR_VALUE), new BoxedFloat(), new BoxedFloat((float) CHAR_VALUE) },

				// char -> double
				{ new PrimitiveChar(CHAR_VALUE), new PrimitiveDouble(), new PrimitiveDouble((double) CHAR_VALUE) },
				{ new PrimitiveChar(CHAR_VALUE), new BoxedDouble(), new BoxedDouble((double) CHAR_VALUE) },
				{ new BoxedChar(CHAR_VALUE), new PrimitiveDouble(), new PrimitiveDouble((double) CHAR_VALUE) },
				{ new BoxedChar(CHAR_VALUE), new BoxedDouble(), new BoxedDouble((double) CHAR_VALUE) },

				// char -> string
				{ new PrimitiveChar(CHAR_VALUE), new WithString(), new WithString(String.valueOf(CHAR_VALUE)) },
				{ new BoxedChar(CHAR_VALUE), new WithString(), new WithString(String.valueOf(CHAR_VALUE)) },

				// byte -> short
				{ new PrimitiveByte(BYTE_VALUE), new PrimitiveShort(), new PrimitiveShort((short) BYTE_VALUE) },
				{ new PrimitiveByte(BYTE_VALUE), new BoxedShort(), new BoxedShort((short) BYTE_VALUE) },
				{ new BoxedByte(BYTE_VALUE), new PrimitiveShort(), new PrimitiveShort((short) BYTE_VALUE) },
				{ new BoxedByte(BYTE_VALUE), new BoxedShort(), new BoxedShort((short) BYTE_VALUE) },

				// byte -> int
				{ new PrimitiveByte(BYTE_VALUE), new PrimitiveInt(), new PrimitiveInt((int) BYTE_VALUE) },
				{ new PrimitiveByte(BYTE_VALUE), new BoxedInt(), new BoxedInt((int) BYTE_VALUE) },
				{ new BoxedByte(BYTE_VALUE), new PrimitiveInt(), new PrimitiveInt((int) BYTE_VALUE) },
				{ new BoxedByte(BYTE_VALUE), new BoxedInt(), new BoxedInt((int) BYTE_VALUE) },

				// byte -> long
				{ new PrimitiveByte(BYTE_VALUE), new PrimitiveLong(), new PrimitiveLong((long) BYTE_VALUE) },
				{ new PrimitiveByte(BYTE_VALUE), new BoxedLong(), new BoxedLong((long) BYTE_VALUE) },
				{ new BoxedByte(BYTE_VALUE), new PrimitiveLong(), new PrimitiveLong((long) BYTE_VALUE) },
				{ new BoxedByte(BYTE_VALUE), new BoxedLong(), new BoxedLong((long) BYTE_VALUE) },

				// byte -> float
				{ new PrimitiveByte(BYTE_VALUE), new PrimitiveFloat(), new PrimitiveFloat((float) BYTE_VALUE) },
				{ new PrimitiveByte(BYTE_VALUE), new BoxedFloat(), new BoxedFloat((float) BYTE_VALUE) },
				{ new BoxedByte(BYTE_VALUE), new PrimitiveFloat(), new PrimitiveFloat((float) BYTE_VALUE) },
				{ new BoxedByte(BYTE_VALUE), new BoxedFloat(), new BoxedFloat((float) BYTE_VALUE) },

				// byte -> double
				{ new PrimitiveByte(BYTE_VALUE), new PrimitiveDouble(), new PrimitiveDouble((double) BYTE_VALUE) },
				{ new PrimitiveByte(BYTE_VALUE), new BoxedDouble(), new BoxedDouble((double) BYTE_VALUE) },
				{ new BoxedByte(BYTE_VALUE), new PrimitiveDouble(), new PrimitiveDouble((double) BYTE_VALUE) },
				{ new BoxedByte(BYTE_VALUE), new BoxedDouble(), new BoxedDouble((double) BYTE_VALUE) },

				// short -> int
				{ new PrimitiveShort(SHORT_VALUE), new PrimitiveInt(), new PrimitiveInt((int) SHORT_VALUE) },
				{ new PrimitiveShort(SHORT_VALUE), new BoxedInt(), new BoxedInt((int) SHORT_VALUE) },
				{ new BoxedShort(SHORT_VALUE), new PrimitiveInt(), new PrimitiveInt((int) SHORT_VALUE) },
				{ new BoxedShort(SHORT_VALUE), new BoxedInt(), new BoxedInt((int) SHORT_VALUE) },

				// short -> long
				{ new PrimitiveShort(SHORT_VALUE), new PrimitiveLong(), new PrimitiveLong((long) SHORT_VALUE) },
				{ new PrimitiveShort(SHORT_VALUE), new BoxedLong(), new BoxedLong((long) SHORT_VALUE) },
				{ new BoxedShort(SHORT_VALUE), new PrimitiveLong(), new PrimitiveLong((long) SHORT_VALUE) },
				{ new BoxedShort(SHORT_VALUE), new BoxedLong(), new BoxedLong((long) SHORT_VALUE) },

				// short -> float
				{ new PrimitiveShort(SHORT_VALUE), new PrimitiveFloat(), new PrimitiveFloat((float) SHORT_VALUE) },
				{ new PrimitiveShort(SHORT_VALUE), new BoxedFloat(), new BoxedFloat((float) SHORT_VALUE) },
				{ new BoxedShort(SHORT_VALUE), new PrimitiveFloat(), new PrimitiveFloat((float) SHORT_VALUE) },
				{ new BoxedShort(SHORT_VALUE), new BoxedFloat(), new BoxedFloat((float) SHORT_VALUE) },

				// short -> double
				{ new PrimitiveShort(SHORT_VALUE), new PrimitiveDouble(), new PrimitiveDouble((double) SHORT_VALUE) },
				{ new PrimitiveShort(SHORT_VALUE), new BoxedDouble(), new BoxedDouble((double) SHORT_VALUE) },
				{ new BoxedShort(SHORT_VALUE), new PrimitiveDouble(), new PrimitiveDouble((double) SHORT_VALUE) },
				{ new BoxedShort(SHORT_VALUE), new BoxedDouble(), new BoxedDouble((double) SHORT_VALUE) },

				// int -> long
				{ new PrimitiveInt(INT_VALUE), new PrimitiveLong(), new PrimitiveLong((long) INT_VALUE) },
				{ new PrimitiveInt(INT_VALUE), new BoxedLong(), new BoxedLong((long) INT_VALUE) },
				{ new BoxedInt(INT_VALUE), new PrimitiveLong(), new PrimitiveLong((long) INT_VALUE) },
				{ new BoxedInt(INT_VALUE), new BoxedLong(), new BoxedLong((long) INT_VALUE) },

				// int -> float
				{ new PrimitiveInt(INT_VALUE), new PrimitiveFloat(), new PrimitiveFloat((float) INT_VALUE) },
				{ new PrimitiveInt(INT_VALUE), new BoxedFloat(), new BoxedFloat((float) INT_VALUE) },
				{ new BoxedInt(INT_VALUE), new PrimitiveFloat(), new PrimitiveFloat((float) INT_VALUE) },
				{ new BoxedInt(INT_VALUE), new BoxedFloat(), new BoxedFloat((float) INT_VALUE) },

				// int -> double
				{ new PrimitiveInt(INT_VALUE), new PrimitiveDouble(), new PrimitiveDouble((double) INT_VALUE) },
				{ new PrimitiveInt(INT_VALUE), new BoxedDouble(), new BoxedDouble((double) INT_VALUE) },
				{ new BoxedInt(INT_VALUE), new PrimitiveDouble(), new PrimitiveDouble((double) INT_VALUE) },
				{ new BoxedInt(INT_VALUE), new BoxedDouble(), new BoxedDouble((double) INT_VALUE) },

				// long -> float
				{ new PrimitiveLong(LONG_VALUE), new PrimitiveFloat(), new PrimitiveFloat((float) LONG_VALUE) },
				{ new PrimitiveLong(LONG_VALUE), new BoxedFloat(), new BoxedFloat((float) LONG_VALUE) },
				{ new BoxedLong(LONG_VALUE), new PrimitiveFloat(), new PrimitiveFloat((float) LONG_VALUE) },
				{ new BoxedLong(LONG_VALUE), new BoxedFloat(), new BoxedFloat((float) LONG_VALUE) },

				// long -> double
				{ new PrimitiveLong(LONG_VALUE), new PrimitiveDouble(), new PrimitiveDouble((double) LONG_VALUE) },
				{ new PrimitiveLong(LONG_VALUE), new BoxedDouble(), new BoxedDouble((double) LONG_VALUE) },
				{ new BoxedLong(LONG_VALUE), new PrimitiveDouble(), new PrimitiveDouble((double) LONG_VALUE) },
				{ new BoxedLong(LONG_VALUE), new BoxedDouble(), new BoxedDouble((double) LONG_VALUE) },

				// float -> double
				{ new PrimitiveFloat(FLOAT_VALUE), new PrimitiveDouble(), new PrimitiveDouble((double) FLOAT_VALUE) },
				{ new PrimitiveFloat(FLOAT_VALUE), new BoxedDouble(), new BoxedDouble((double) FLOAT_VALUE) },
				{ new BoxedFloat(FLOAT_VALUE), new PrimitiveDouble(), new PrimitiveDouble((double) FLOAT_VALUE) },
				{ new BoxedFloat(FLOAT_VALUE), new BoxedDouble(), new BoxedDouble((double) FLOAT_VALUE) },
		};
	}

	@Test
	public void shouldPromoteAndAssignValue() {
		Object[][] objectsToCopy = getObjectsToCopy();
		for (Object[] toCopy : objectsToCopy) {
			assertCopyCorrectness(toCopy[0], toCopy[1], toCopy[2]);
		}
	}

	private void assertCopyCorrectness(Object src, Object dst, Object expected) {
		try {
			copyMode.setup(src, dst);
			copyMode.copy("field", "field");
			assertThat(getErrorMsg(dst, expected), dst, equalTo(expected));
		} catch (Exception ex) {
			fail(getErrorMsgWithStackTrace(dst, expected, ex));
		}
	}

	private String getErrorMsgWithStackTrace(Object actual, Object expected, Exception exception) {
		StringWriter stringWriter = new StringWriter();
		exception.printStackTrace(new PrintWriter(stringWriter));

		return getErrorMsg(actual, expected) + "\n\n" + stringWriter.toString();
	}

	private String getErrorMsg(Object actual, Object expected) {
		return String.format("Expected: %s,\n actual: %s", expected.toString(), actual.toString());
	}
}
