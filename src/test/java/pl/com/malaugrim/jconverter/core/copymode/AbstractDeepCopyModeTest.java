package pl.com.malaugrim.jconverter.core.copymode;

import pl.com.malaugrim.jconverter.core.ObjectInstantiator;
import org.junit.Test;

import java.util.function.Supplier;

import pl.com.malaugrim.jconverter.testtools.matchers.TestMatchers;
import pl.com.malaugrim.jconverter.TestConstants;
import pl.com.malaugrim.jconverter.testobjects.simple.*;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.verify;

/**
 * @author Lukasz Tutka
 */
public abstract class AbstractDeepCopyModeTest extends AbstractCopyModeTest {

	@Test
	public void shouldShallowCopyWhenDstAccessibleDoesNotHaveDefaultConstructorAndNoInstanceSupplierIsProvided() {
		WithoutDefaultConstructor expectedInstance = new WithoutDefaultConstructor(TestConstants.LONG_VALUE);
		TestObject5 src = new TestObject5(expectedInstance);
		TestObject5 dst = new TestObject5();
		getCopyMode().setup(src, dst);
		getCopyMode().copy("field", "field");

		assertThat(dst, TestMatchers.isProperlyCopied(src));
		assertThat(dst.getField(), sameInstance(expectedInstance));
	}

	@Test
	public void shouldShallowCopyNumbersAndStrings() {
		TestObject2 src = new TestObject2(TestConstants.INT_VALUE, TestConstants.DOUBLE_VALUE, TestConstants.STRING_VALUE);
		TestObject2 dst = new TestObject2();
		getCopyMode().setup(src, dst);
		getCopyMode().copy("doubleValue", "doubleValue");
		getCopyMode().copy("stringValue", "stringValue");

		assertThat(dst, TestMatchers.isProperlyCopied(new TestObject2(0, TestConstants.DOUBLE_VALUE, TestConstants.STRING_VALUE)));
		assertThat(dst.getStringValue(), sameInstance(TestConstants.STRING_VALUE));
	}

	@SuppressWarnings("unchecked")
	@Test
	public void shouldDeepCopyUsingProvidedInstanceSupplier() {
		Supplier<TestObject1> testObject1Supplier = mock(Supplier.class);
		when(testObject1Supplier.get()).thenReturn(new TestObject1(TestConstants.INT_VALUE + 1, TestConstants.LONG_VALUE + 1, TestConstants.DOUBLE_VALUE + 1, TestConstants.BOOLEAN_VALUE, null));

		TestObject3 dst = new TestObject3();
		ObjectInstantiator instantiator = new ObjectInstantiator();
		instantiator.addInstanceSupplier(TestObject1.class, testObject1Supplier);
		getCopyMode().setObjectInstantiator(instantiator);

		TestObject1 testObject1 = new TestObject1(TestConstants.INT_VALUE, TestConstants.LONG_VALUE, TestConstants.DOUBLE_VALUE, TestConstants.BOOLEAN_VALUE, TestConstants.STRING_VALUE);
		getCopyMode().setup(new TestObject3(testObject1, null), dst);
		getCopyMode().copy("testObject1", "testObject1");

		verify(testObject1Supplier).get();
		assertThat(dst, TestMatchers.isProperlyCopied(new TestObject3(testObject1, null)));
	}

	@Test
	public void shouldThrowExceptionWhenDstAccessibleDoesNotHaveDefaultConstructorAndNoInstanceSupplierIsProvided() {
		thrown.expect(IllegalStateException.class);
		thrown.expectMessage(TestMatchers.matchesPattern("Cannot find deep copy strategy for copying object \\[.+\\]\\. Either check if " +
				"provided strategies are sufficient, provide proper instance supplier via copyMode\\.setObjectInstantiator\\(\\) " +
				"method, or just set 'shallowCopyWhenCannotFindStrategy' flag to true\\."));

		WithoutDefaultConstructor expectedInstance = new WithoutDefaultConstructor(TestConstants.LONG_VALUE);
		TestObject5 src = new TestObject5(expectedInstance);
		TestObject5 dst = new TestObject5();
		getCopyMode().setup(src, dst);
		getCopyMode().shallowCopyWhenCannotClone(false);
		getCopyMode().copy("field", "field");
	}

	@Test
	public void shouldReturnNullWhenSrcIsNull() {
		TestObject3 src = new TestObject3(null, new TestObject2(TestConstants.INT_VALUE, TestConstants.DOUBLE_VALUE, TestConstants.STRING_VALUE));
		TestObject3 dst = new TestObject3(new TestObject1(TestConstants.INT_VALUE, TestConstants.INT_VALUE, TestConstants.DOUBLE_VALUE, TestConstants.BOOLEAN_VALUE, TestConstants.STRING_VALUE), null);
		getCopyMode().setup(src, dst);
		getCopyMode().copy("testObject1", "testObject1");

		assertThat(dst, TestMatchers.isProperlyCopied(new TestObject3()));
	}

	@Test
	public void shouldUseCloneMethodWhenDstAccessibleImplementsCloneableAndHasPublicCloneMethod() throws Exception {
		CloneableObject cloneable = spy(new CloneableObject(TestConstants.INT_VALUE, TestConstants.LONG_VALUE));
		WithCloneableObject src = new WithCloneableObject(cloneable);
		WithCloneableObject dst = new WithCloneableObject();
		getCopyMode().setup(src, dst);
		getCopyMode().copy("field", "field");

		verify(cloneable).clone();
		assertThat(dst.getField(), equalTo(src.getField()));
	}

	@Test
	public void shouldProceedWithStandardDeepCopyWhenFieldImplementsCloneableAndDoesNotHavePublicCloneMethod() {
		WithCloneableObjectWithoutCloneMethod src = new WithCloneableObjectWithoutCloneMethod(new CloneableObjectWithoutCloneMethod(TestConstants.INT_VALUE));
		WithCloneableObjectWithoutCloneMethod dst = new WithCloneableObjectWithoutCloneMethod();
		getCopyMode().setup(src, dst);
		getCopyMode().copy("field", "field");

		assertThat(dst, TestMatchers.isProperlyCopied(src));
	}

	@Override
	protected abstract DeepCopyMode getCopyMode();
}
