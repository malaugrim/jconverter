package pl.com.malaugrim.jconverter.core.copymode;

import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;

import pl.com.malaugrim.jconverter.core.copymode.copymodebuilder.CopyModeBuilder;
import org.junit.Before;
import org.junit.Test;

import static pl.com.malaugrim.jconverter.testtools.matchers.TestMatchers.isProperlyCopied;
import pl.com.malaugrim.jconverter.testobjects.simple.TestObject1;
import pl.com.malaugrim.jconverter.testobjects.simple.TestObject3;
import pl.com.malaugrim.jconverter.testobjects.simple.WithTestObject1Setter2;
import pl.com.malaugrim.jconverter.testobjects.simple.WithTestObject1Setter3;

/**
 * @author Lukasz Tutka
 */
public class FieldToPropertyShallowCopyModeTest extends AbstractCopyModeTest {

	private CopyMode copyMode;

	@Before
	public void setUp() {
		CopyModeBuilder builder = new CopyModeBuilder();
		copyMode = builder.shallowCopy().fromField().toProperty().build();
	}

	@Test
	public void shouldSetPropertyFromFieldOfTheSameNameInSameClassObject() {
		TestObject1 testObject1 = new TestObject1(INT_VALUE, LONG_VALUE, DOUBLE_VALUE, BOOLEAN_VALUE, STRING_VALUE);
		TestObject3 src = spy(new TestObject3(testObject1, null));
		TestObject3 dst = spy(new TestObject3());
		copyMode.setup(src, dst);
		copyMode.copy("testObject1", "testObject1");
		
		assertThat(dst, isProperlyCopied(new TestObject3(testObject1, null)));
		verify(src, never()).getTestObject1();
		verify(dst).setTestObject1(testObject1);
	}

	@Test
	public void shouldSetPropertyFromFieldOfTheSameNameInClassOfDifferentType() {
		TestObject1 testObject1 = new TestObject1(INT_VALUE, LONG_VALUE, DOUBLE_VALUE, BOOLEAN_VALUE, STRING_VALUE);
		TestObject3 src = spy(new TestObject3(testObject1, null));
		WithTestObject1Setter3 dst = spy(new WithTestObject1Setter3());
		copyMode.setup(src, dst);
		copyMode.copy("testObject1", "testObject1");
		
		assertThat(dst, isProperlyCopied(new WithTestObject1Setter3(testObject1)));
		verify(src, never()).getTestObject1();
		verify(dst).setTestObject1(testObject1);
	}

	@Test
	public void shouldSetPropertyFromFieldOfDifferentNameInClassOfDifferentType() {
		TestObject1 testObject1 = new TestObject1(INT_VALUE, LONG_VALUE, DOUBLE_VALUE, BOOLEAN_VALUE, STRING_VALUE);
		TestObject3 src = spy(new TestObject3(testObject1, null));
		WithTestObject1Setter2 dst = spy(new WithTestObject1Setter2());
		copyMode.setup(src, dst);
		copyMode.copy("testObject1", "object");

		assertThat(dst, isProperlyCopied(new WithTestObject1Setter2(testObject1)));
		verify(src, never()).getTestObject1();
		verify(dst).setObject(testObject1);
	}

	@Test
	public void shouldThrowExceptionWhenThereIsNoSourceField() {
		shouldThrowExceptionWhenThereIsNoSourceAccessible("field");
	}

	@Test
	public void shouldThrowExceptionWhenThereIsNoDestinationProperty() {
		shouldThrowExceptionWhenThereIsNoDestinationAccessible("property");
	}

	@Override
	protected CopyMode getCopyMode() {
		return copyMode;
	}
}
