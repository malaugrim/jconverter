package pl.com.malaugrim.jconverter.core.copymode.copymodebuilder;

import com.google.common.collect.Lists;
import pl.com.malaugrim.jconverter.core.TypeConverter;
import pl.com.malaugrim.jconverter.core.accessor.Accessor;
import pl.com.malaugrim.jconverter.core.accessor.FieldAccessor;
import pl.com.malaugrim.jconverter.core.accessor.PropertyAccessor;
import org.hamcrest.CoreMatchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import pl.com.malaugrim.jconverter.core.copymode.*;
import pl.com.malaugrim.jconverter.core.copymode.deepcopystrategy.*;
import pl.com.malaugrim.jconverter.core.copymode.validators.*;

import java.util.List;
import java.util.stream.Collectors;

import static pl.com.malaugrim.jconverter.testtools.ValueExtractor.getValue;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;

/**
 * @author Lukasz Tutka
 */
public class CopyModeBuilderTest {

	@Rule
	public final ExpectedException thrown = ExpectedException.none();
	
	private CopyModeBuilder builder;
	
	@Before
	public void setUp() {
		builder = new CopyModeBuilder();
	}

	@Test
	public void shouldCreateFieldToFieldCopyMode() {
		CopyMode copyMode = builder.shallowCopy().fromField().toField().build();
		
		testCreatedCopyMode(copyMode, ShallowCopyMode.class, FieldAccessor.class, FieldAccessor.class, SourceFieldValidator.class,
				DestinationFieldValidator.class);
	}

	@Test
	public void shouldCreateFieldToFieldDeepCopyMode() {
		CopyMode copyMode = builder.deepCopy().fromField().toField().build();

		testDeepCopyMode(copyMode, FieldAccessor.class, FieldAccessor.class, SourceFieldValidator.class, DestinationFieldValidator.class);
	}

	@Test
	public void shouldCreateFieldToFieldConversionMode() {
		TypeConverter typeConverter = mock(TypeConverter.class);
		CopyMode copyMode = builder.convert(ConversionModeVariant.TAKE_DST_VALUE_ONLY, typeConverter).fromField().toField().build();

		testCreatedCopyMode(copyMode, ConversionMode.class, FieldAccessor.class, FieldAccessor.class, SourceFieldValidator.class,
				DestinationFieldValidator.class);
	}

	@Test
	public void shouldCreateFieldToPropertyCopyMode() {
		CopyMode copyMode = builder.shallowCopy().fromField().toProperty().build();

		testCreatedCopyMode(copyMode, ShallowCopyMode.class, FieldAccessor.class, PropertyAccessor.class, SourceFieldValidator.class,
				DestinationPropertyValidator.class);
	}

	@Test
	public void shouldCreateFieldToPropertyDeepCopyMode() {
		CopyMode copyMode = builder.deepCopy().fromField().toProperty().build();

		testDeepCopyMode(copyMode, FieldAccessor.class, PropertyAccessor.class, SourceFieldValidator.class,
				DestinationPropertyValidator.class);
	}

	@Test
	public void shouldCreateFieldToPropertyConversionMode() {
		TypeConverter typeConverter = mock(TypeConverter.class);
		CopyMode copyMode = builder.convert(ConversionModeVariant.TAKE_DST_VALUE_ONLY, typeConverter).fromField().toProperty().build();

		testCreatedCopyMode(copyMode, ConversionMode.class, FieldAccessor.class, PropertyAccessor.class, SourceFieldValidator.class,
				DestinationPropertyValidator.class);
	}

	@Test
	public void shouldCreatePropertyToFieldCopyMode() {
		CopyMode copyMode = builder.shallowCopy().fromProperty().toField().build();

		testCreatedCopyMode(copyMode, ShallowCopyMode.class, PropertyAccessor.class, FieldAccessor.class, SourcePropertyValidator.class,
				DestinationFieldValidator.class);
	}

	@Test
	public void shouldCreatePropertyToFieldDeepCopyMode() {
		CopyMode copyMode = builder.deepCopy().fromProperty().toField().build();

		testDeepCopyMode(copyMode, PropertyAccessor.class, FieldAccessor.class, SourcePropertyValidator.class,
				DestinationFieldValidator.class);
	}

	@Test
	public void shouldCreatePropertyToFieldConversionMode() {
		TypeConverter typeConverter = mock(TypeConverter.class);
		CopyMode copyMode = builder.convert(ConversionModeVariant.TAKE_DST_VALUE_ONLY, typeConverter).fromProperty().toField().build();

		testCreatedCopyMode(copyMode, ConversionMode.class, PropertyAccessor.class, FieldAccessor.class, SourcePropertyValidator.class,
				DestinationFieldValidator.class);
	}

	@Test
	public void shouldCreatePropertyToPropertyCopyMode() {
		CopyMode copyMode = builder.shallowCopy().fromProperty().toProperty().build();

		testCreatedCopyMode(copyMode, ShallowCopyMode.class, PropertyAccessor.class, PropertyAccessor.class, SourcePropertyValidator.class,
				DestinationPropertyValidator.class);
	}

	@Test
	public void shouldCreatePropertyToPropertyDeepCopyMode() {
		CopyMode copyMode = builder.deepCopy().fromProperty().toProperty().build();

		testDeepCopyMode(copyMode, PropertyAccessor.class, PropertyAccessor.class, SourcePropertyValidator.class,
				DestinationPropertyValidator.class);
	}

	@Test
	public void shouldCreatePropertyToPropertyConversionMode() {
		TypeConverter typeConverter = mock(TypeConverter.class);
		CopyMode copyMode = builder.convert(ConversionModeVariant.TAKE_DST_VALUE_ONLY, typeConverter).fromProperty().toProperty().build();

		testCreatedCopyMode(copyMode, ConversionMode.class, PropertyAccessor.class, PropertyAccessor.class, SourcePropertyValidator.class,
				DestinationPropertyValidator.class);
	}

	@Test
	public void shouldThrowExceptionWhenSrcAccessorIsNull() {
		thrown.expect(IllegalStateException.class);
		thrown.expectMessage("Both source and destination must be selected");
		builder.build();
	}

	@Test
	public void shouldThrowExceptionWhenDstAccessorIsNull() {
		builder.shallowCopy().fromField();
		thrown.expect(IllegalStateException.class);
		thrown.expectMessage("Both source and destination must be selected");
		builder.build();
	}

	@Test
	public void shouldBeInInitialStateAfterBuild() {
		builder.deepCopy().fromField().toProperty().build();
		assertTrue(isInInitialState(builder));
		
		builder.convert(ConversionModeVariant.TAKE_DST_VALUE_ONLY, mock(TypeConverter.class)).fromProperty().toField().build();
		assertTrue(isInInitialState(builder));
	}

	@SuppressWarnings("unchecked")
	private <T extends AbstractCopyMode> void testCreatedCopyMode(CopyMode result, Class<T> expected, Class<? extends Accessor> srcAccessor,
																  Class<? extends Accessor> dstAccessor,
																  Class<? extends AccessibleValidator> srcValidator,
																  Class<? extends AccessibleValidator> dstValidator) {

		assertThat(result, instanceOf(expected));

		T copyMode = (T) result;
		assertThat(getValue(copyMode, "srcAccessorType"), equalTo(srcAccessor));
		assertThat(getValue(copyMode, "dstAccessorType"), equalTo(dstAccessor));
		assertThat(getValue(copyMode, "srcValidator"), instanceOf(srcValidator));
		assertThat(getValue(copyMode, "dstValidator"), instanceOf(dstValidator));
	}

	private void testDeepCopyMode(CopyMode result, Class<? extends Accessor> srcAccessor, Class<? extends Accessor> dstAccessor,
								  Class<? extends AccessibleValidator> srcValidator, Class<? extends AccessibleValidator> dstValidator) {
		testCreatedCopyMode(result, DeepCopyMode.class, srcAccessor, dstAccessor, srcValidator, dstValidator);
		DeepCopyMode copyMode = (DeepCopyMode) result;
		assertThat(getValue(copyMode, "strategyPicker"), instanceOf(SequentialStrategyPicker.class));
		assertCopyStrategies(copyMode);
	}

	@SuppressWarnings("unchecked")
	private void assertCopyStrategies(DeepCopyMode copyMode) {
		List<DeepCopyStrategy> list = getValue(copyMode, "deepCopyStrategies");
		List<Class<?>> strategies = list.stream().map(DeepCopyStrategy::getClass).collect(Collectors.toList());
		List<Class<?>> expected = Lists.newArrayList(NullObjectStrategy.class, CloneableMethodStrategy.class,
				SimpleObjectShallowCopyStrategy.class, FieldByFieldCopyStrategy.class);
		assertThat(strategies, CoreMatchers.equalTo(expected));
	}

	private boolean isInInitialState(CopyModeBuilder builder) {
		return builder.srcAccessorType == null &&
				builder.dstAccessorType == null &&
				builder.typeConverter == null && 
				!builder.deepCopy &&
				!builder.buildStarted;
	}

}
