package pl.com.malaugrim.jconverter.core.copymode;

import pl.com.malaugrim.jconverter.core.copymode.copymodebuilder.CopyModeBuilder;
import org.junit.Before;
import org.junit.Test;
import pl.com.malaugrim.jconverter.testobjects.simple.TestObject1;
import pl.com.malaugrim.jconverter.testobjects.simple.TestObject3;
import pl.com.malaugrim.jconverter.testobjects.simple.WithTestObject1Setter;
import pl.com.malaugrim.jconverter.testobjects.simple.WithTestObject1Setter2;
import pl.com.malaugrim.jconverter.testtools.matchers.TestMatchers;

import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;

/**
 * @author Lukasz Tutka
 */
public class PropertyToPropertyShallowCopyModeTest extends AbstractCopyModeTest {
	
	private CopyMode copyMode;

	@Before
	public void setUp() {
		CopyModeBuilder builder = new CopyModeBuilder();
		copyMode = builder.shallowCopy().fromProperty().toProperty().build();
	}

	@Test
	public void shouldSetPropertyFromPropertyOfTheSameNameInSameClassObject() {
		TestObject1 testObject1 = new TestObject1(INT_VALUE, LONG_VALUE, DOUBLE_VALUE, BOOLEAN_VALUE, STRING_VALUE);
		TestObject3 src = spy(new TestObject3(testObject1, null));
		TestObject3 dst = spy(new TestObject3());
		TestObject3 expected = new TestObject3(testObject1, null);
		copyMode.setup(src, dst);
		copyMode.copy("testObject1", "testObject1");

		assertThat(dst, TestMatchers.isProperlyCopied(expected));
		verify(dst).setTestObject1(testObject1);
		verify(src).getTestObject1();
		verifyNoMoreInteractions(src, dst);
	}

	@Test
	public void shouldSetPropertyFromPropertyOfTheSameNameInClassOfDifferentType() {
		TestObject1 testObject1 = new TestObject1(INT_VALUE, LONG_VALUE, DOUBLE_VALUE, BOOLEAN_VALUE, STRING_VALUE);
		TestObject3 src = spy(new TestObject3(testObject1, null));
		WithTestObject1Setter dst = spy(new WithTestObject1Setter());
		WithTestObject1Setter expected = new WithTestObject1Setter(testObject1);
		copyMode.setup(src, dst);
		copyMode.copy("testObject1", "testObject1");

		assertThat(dst, TestMatchers.isProperlyCopied(expected));
		verify(dst).setTestObject1(testObject1);
		verify(src).getTestObject1();
		verifyNoMoreInteractions(src, dst);
	}

	@Test
	public void shouldSetPropertyFromPropertyOfDifferentNameInClassOfDifferentType() {
		TestObject1 testObject1 = new TestObject1(INT_VALUE, LONG_VALUE, DOUBLE_VALUE, BOOLEAN_VALUE, STRING_VALUE);
		TestObject3 src = spy(new TestObject3(testObject1, null));
		WithTestObject1Setter2 dst = spy(new WithTestObject1Setter2());
		WithTestObject1Setter2 expected = new WithTestObject1Setter2(testObject1);
		copyMode.setup(src, dst);
		copyMode.copy("testObject1", "object");

		assertThat(dst, TestMatchers.isProperlyCopied(expected));
		verify(dst).setObject(testObject1);
		verify(src).getTestObject1();
		verifyNoMoreInteractions(src, dst);
	}

	@Test
	public void shouldThrowExceptionWhenThereIsNoSourceField() {
		shouldThrowExceptionWhenThereIsNoSourceAccessible("property");
	}

	@Test
	public void shouldThrowExceptionWhenThereIsNoDestinationProperty() {
		shouldThrowExceptionWhenThereIsNoDestinationAccessible("property");
	}

	@Override
	protected CopyMode getCopyMode() {
		return copyMode;
	}
}
