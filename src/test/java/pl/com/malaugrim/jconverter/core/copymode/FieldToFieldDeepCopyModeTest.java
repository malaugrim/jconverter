package pl.com.malaugrim.jconverter.core.copymode;

import pl.com.malaugrim.jconverter.core.copymode.copymodebuilder.CopyModeBuilder;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import pl.com.malaugrim.jconverter.testobjects.simple.*;

import static pl.com.malaugrim.jconverter.testtools.matchers.TestMatchers.isProperlyCopied;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

/**
 * @author Lukasz Tutka
 */
public class FieldToFieldDeepCopyModeTest extends AbstractDeepCopyModeTest {

	private DeepCopyMode copyMode;

	@Before
	public void setUp() {
		CopyModeBuilder builder = new CopyModeBuilder();
		copyMode = builder.deepCopy().fromField().toField().build();
	}

	@Test
	public void shouldSetFieldFromFieldOfTheSameNameInSameClassObject() {
		TestObject1 testObject1 = new TestObject1(INT_VALUE, LONG_VALUE, DOUBLE_VALUE, BOOLEAN_VALUE, STRING_VALUE);
		TestObject3 testObject3 = new TestObject3(testObject1, null);
		TestObject4 src = spy(new TestObject4(testObject3));
		TestObject4 dst = spy(new TestObject4());
		copyMode.setup(src, dst);
		copyMode.copy("testObject3", "testObject3");

		assertThat(dst, isProperlyCopied(new TestObject4(testObject3)));
		verify(src, never()).getTestObject3();
		verify(dst, never()).setTestObject3(Mockito.any());
		assertThat(dst.getTestObject3(), not(sameInstance(testObject3)));
		assertThat(dst.getTestObject3().getTestObject1(), not(sameInstance(testObject1)));
	}

	@Test
	public void shouldSetFieldFromFieldOfTheSameNameInClassOfDifferentType() {
		TestObject1 src = new TestObject1(INT_VALUE, LONG_VALUE, DOUBLE_VALUE, BOOLEAN_VALUE, STRING_VALUE);
		TestObject2 dst = spy(new TestObject2());
		TestObject2 expected = new TestObject2(0, DOUBLE_VALUE, null);
		copyMode.setup(src, dst);
		copyMode.copy("doubleValue", "doubleValue");

		assertThat(dst, isProperlyCopied(expected));
		verify(dst, never()).setDoubleValue(DOUBLE_VALUE);
	}

	@Test
	public void shouldSetFieldFromFieldOfDifferentNameInClassOfDifferentType() {
		TestObject1 testObject1 = new TestObject1(INT_VALUE, LONG_VALUE, DOUBLE_VALUE, BOOLEAN_VALUE, STRING_VALUE);
		TestObject3 src = spy(new TestObject3(testObject1, null));
		WithTestObject1Setter2 dst = spy(new WithTestObject1Setter2());
		copyMode.setup(src, dst);
		copyMode.copy("testObject1", "object");

		assertThat(dst, isProperlyCopied(new WithTestObject1Setter2(testObject1)));
		verify(src, never()).getTestObject1();
		verify(dst, never()).setObject(any());
	}

	@Test
	public void shouldThrowExceptionWhenThereIsNoSourceField() {
		shouldThrowExceptionWhenThereIsNoSourceAccessible("field");
	}

	@Test
	public void shouldThrowExceptionWhenThereIsNoDestinationField() {
		shouldThrowExceptionWhenThereIsNoDestinationAccessible("field");
	}

	@Override
	protected DeepCopyMode getCopyMode() {
		return copyMode;
	}
}
