package pl.com.malaugrim.jconverter.core.copymode;

import org.junit.Ignore;
import org.junit.Test;
import org.mockito.Mockito;
import pl.com.malaugrim.jconverter.core.TypeConverter;
import pl.com.malaugrim.jconverter.testconverters.DummyConverter;
import pl.com.malaugrim.jconverter.testobjects.simple.TestObject1;
import pl.com.malaugrim.jconverter.testobjects.simple.TestObject2;
import pl.com.malaugrim.jconverter.testobjects.simple.TestObject3;

import static org.hamcrest.CoreMatchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static pl.com.malaugrim.jconverter.testtools.matchers.TestMatchers.isProperlyCopied;

/**
 * @author Lukasz Tutka
 */
@SuppressWarnings("unchecked")
public abstract class AbstractConversionModeTest extends AbstractCopyModeTest {

	protected TypeConverter typeConverter;

	public void setUp() {
		typeConverter = Mockito.spy(new DummyConverter());
	}

	/**
	 * N/A
	 */
	@Ignore
	@Test
	@Override
	public void shouldCopyPrimitivesToTheirBoxedCorrespondingTypes() {
	}

	/**
	 * N/A
	 */
	@Ignore
	@Test
	@Override
	public void shouldCopyBoxedTypesToTheirCorrespondingPrimitives() {
	}

	@Test
	@Override
	public void shouldRetainNotCopiedAccessibles() {
		TestObject1 testObject1 = new TestObject1(INT_VALUE, LONG_VALUE, DOUBLE_VALUE, BOOLEAN_VALUE, STRING_VALUE);
		when(typeConverter.convert(any(), any())).thenReturn(testObject1);

		TestObject2 testObject2 = new TestObject2(INT_VALUE, DOUBLE_VALUE, STRING_VALUE);
		TestObject3 dst = new TestObject3(null, testObject2);
		TestObject3 expected = new TestObject3(testObject1, testObject2);
		getCopyMode().setup(new TestObject3(testObject1, null), dst);
		getCopyMode().copy("testObject1", "testObject1");

		assertThat(dst, isProperlyCopied(expected));
	}

	@Test
	public void shouldPassDstFieldInstanceToConverterWhenValueIsNotNull() {
		TestObject1 testObject1 = new TestObject1(INT_VALUE, LONG_VALUE, DOUBLE_VALUE, BOOLEAN_VALUE, STRING_VALUE);
		TestObject3 src = new TestObject3(null, null);
		TestObject3 dst = new TestObject3(testObject1, null);
		getCopyMode().setup(src, dst);
		getCopyMode().copy("testObject1", "testObject1");

		verify(typeConverter).convert(eq(null), argThat(sameInstance(testObject1)));
	}

	@Test
	public void shouldPassDstClassToConverterWhenValueIsNull() {
		TestObject3 src = new TestObject3(null, null);
		TestObject3 dst = new TestObject3(null, null);
		getCopyMode().setup(src, dst);
		getCopyMode().copy("testObject1", "testObject1");

		verify(typeConverter).convert(null, TestObject1.class);
	}

	@Override
	protected abstract ConversionMode getCopyMode();
}
