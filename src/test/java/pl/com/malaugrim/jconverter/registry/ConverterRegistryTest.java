package pl.com.malaugrim.jconverter.registry;

import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.google.common.cache.Cache;
import com.google.common.collect.Lists;

import pl.com.malaugrim.jconverter.core.ObjectConverter;
import pl.com.malaugrim.jconverter.testobjects.simple.TestObject1;
import pl.com.malaugrim.jconverter.testobjects.simple.TestObject2;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.hasItem;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @author Lukasz Tutka
 */
@SuppressWarnings("unchecked")
public class ConverterRegistryTest {

	private ConverterRegistry registry;
	
	@Mock
	private ObjectConverter converter1;
	@Mock
	private ObjectConverter converter2;
	@Mock
	private ObjectConverter converter3;


	@Before
	public void setUp() throws Exception {
		registry = new ConverterRegistry();
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void shouldContainAddedConverters() {
		registry.register(converter1);
		registry.register(Lists.newArrayList(converter2, converter3));
		
		List<ObjectConverter> converters = registry.getAllConverters();
		assertThat(converters.size(), equalTo(3));
		assertHasInstance(converters, converter1);
		assertHasInstance(converters, converter2);
		assertHasInstance(converters, converter3);
	}

	@SuppressWarnings("OptionalGetWithoutIsPresent")
	@Test
	public void shouldFindProperConverter() {
		onCanConvertCompareClasses(converter1, TestObject1.class, TestObject2.class);
		onCanConvertCompareClasses(converter2, Integer.class, Long.class);
		registry.register(converter1);
		Optional<ObjectConverter> opt = registry.find(TestObject1.class, TestObject2.class);
		assertThat(opt.isPresent(), equalTo(true));
		assertThat(opt.get().canConvert(TestObject1.class, TestObject2.class), equalTo(true));
	}

	@Test
	public void shouldCacheFoundObjectConverters() {
		onCanConvertCompareClasses(converter1, TestObject1.class, TestObject2.class);
		onCanConvertCompareClasses(converter2, Integer.class, Long.class);
		Cache cache = mock(Cache.class);
		registry.setCache(cache);
		registry.register(Lists.newArrayList(converter1, converter2));
		registry.find(TestObject1.class, TestObject2.class);
		registry.find(TestObject1.class, TestObject2.class);

		ConversionClasses conversionClasses = ConversionClasses.of(TestObject1.class, TestObject2.class);
		InOrder inOrder = Mockito.inOrder(cache);
		inOrder.verify(cache).getIfPresent(conversionClasses);
		inOrder.verify(cache).put(conversionClasses, converter1);
		inOrder.verify(cache).getIfPresent(conversionClasses);
	}

	private void onCanConvertCompareClasses(ObjectConverter converter, Class expectedSrcClass, Class expectedDstClass) {
		when(converter.canConvert(any(), any())).thenAnswer(invocation -> {
			Class srcClass = invocation.getArgumentAt(0, Class.class);
			Class dstClass = invocation.getArgumentAt(1, Class.class);
			return expectedSrcClass.equals(srcClass) && expectedDstClass.equals(dstClass);
		});
	}

	private void assertHasInstance(List<ObjectConverter> converters, ObjectConverter converter) {
		assertThat(converters, hasItem(converter));
		assertTrue(registry.contains(converter));
	}
}
